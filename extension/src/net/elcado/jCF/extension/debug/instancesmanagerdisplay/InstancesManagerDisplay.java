/*
 * Copyright (c). Frédéric Cadier (2014-2018)
 *
 * This software is a computer program whose purpose is to allow component programming in pure java.
 *
 * This software is governed by the CeCILL-C license under French law and abiding by the rules of distribution of free software. You can use, modify and/or redistribute the software under the terms of the CeCILL-C license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy, modify and redistribute granted by the license, users are provided only with a limited warranty and the software's author, the holder of the economic rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, that may mean that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured and, more generally, to use and operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had knowledge of the CeCILL-C license and that you accept its terms.
 */
package net.elcado.jCF.extension.debug.instancesmanagerdisplay;

import com.sun.javafx.application.PlatformImpl;
import javafx.application.Platform;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeTableColumn;
import javafx.scene.control.TreeTableView;
import javafx.stage.Stage;
import net.elcado.jCF.core.annotations.Component;
import net.elcado.jCF.core.annotations.EventsSink;
import net.elcado.jCF.core.annotations.Id;
import net.elcado.jCF.core.annotations.UsedService;
import net.elcado.jCF.core.container.Container;
import net.elcado.jCF.core.container.InstancesServices;
import net.elcado.jCF.core.container.instancesManager.InstancesManagerDebugServices;
import net.elcado.jCF.core.container.instancesManager.InstancesManagerEvent;
import net.elcado.jCF.core.lang.component.Initializable;
import net.elcado.jCF.core.lang.component.Startable;
import net.elcado.jCF.core.lang.instance.Instance;
import net.elcado.jCF.core.lang.instance.InstanceId;
import net.elcado.jCF.core.lang.instance.events.Sink;
import net.elcado.jCF.utils.tree.Tree;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * @author fred
 */
@Component
public class InstancesManagerDisplay implements Initializable, Startable {

    @FXML
    private TreeTableView<Instance> instancesTreeTableView;

    @Id
    private final transient InstanceId thisId = null;

    @UsedService
    private final transient InstancesServices instancesServices = null;

    @EventsSink
    private final transient Sink<InstancesManagerEvent> instancesManagerEventsSink = instancesManagerEvent -> this.onStarted();

    @UsedService
	private final transient InstancesManagerDebugServices instancesManagerDebugServices = null;

    private Stage stage;

    @Override
	public boolean onInitialized() {
		PlatformImpl.startup(() -> {
            try {
                URL fxmlFileURL = getClass().getResource("debug_main_window.fxml");

                // build FXMLLoader and set controller
                FXMLLoader fxmlLoader = new FXMLLoader(fxmlFileURL);
                fxmlLoader.setController(this);

                // load FXML
                Parent node = fxmlLoader.load();
                assert node != null;

                // build scene
                Scene mainScene = new Scene(node);

                stage = new Stage();
                stage.setOnCloseRequest(event -> {
                    instancesServices.stopInstance(thisId);
                    instancesServices.undeployInstance(thisId);
                });
                stage.setScene(mainScene);
                stage.show();

                // type column
                TreeTableColumn<Instance, String> instanceTypeColumn = (TreeTableColumn<Instance, String>) instancesTreeTableView.getColumns().get(0);
                instanceTypeColumn.setCellValueFactory((TreeTableColumn.CellDataFeatures<Instance, String> param) ->
                        new ReadOnlyStringWrapper(param.getValue().getValue().getComponent().getClazz().getSimpleName())
                );

                // id column
                TreeTableColumn<Instance, String> idTypeColumn = (TreeTableColumn<Instance, String>) instancesTreeTableView.getColumns().get(1);
                idTypeColumn.setCellValueFactory((TreeTableColumn.CellDataFeatures<Instance, String> param) ->
                        new ReadOnlyStringWrapper(param.getValue().getValue().getInstanceId().toString())
                );

                // state column
                TreeTableColumn<Instance, String> stateTypeColumn = (TreeTableColumn<Instance, String>) instancesTreeTableView.getColumns().get(2);
                stateTypeColumn.setCellValueFactory((TreeTableColumn.CellDataFeatures<Instance, String> param) ->
                        new ReadOnlyStringWrapper(param.getValue().getValue().getCurrentInstanceState().toString())
                );

                instancesTreeTableView.setShowRoot(false);

            } catch (IOException IOe) {
                IOe.printStackTrace();
            }
        });

		return true;
	}

	@Override
	public boolean onStarted() {
        Platform.runLater(() -> {
            // populate tree table view
            Tree<InstanceId, Instance> instancesTree = instancesManagerDebugServices.getInstancesTree();

            // get root instances
            TreeItem<Instance> rootInstanceTreeItem = new TreeItem<>();
            rootInstanceTreeItem.setExpanded(true);
            instancesTree.getRoots().forEach(rootId -> rootInstanceTreeItem.getChildren().add(this.buildInstanceTreeItem(rootId, instancesTree)));

            instancesTreeTableView.setRoot(rootInstanceTreeItem);
        });

		return true;
	}

	private TreeItem<Instance> buildInstanceTreeItem(InstanceId instanceId, Tree<InstanceId, Instance> instancesTree) {
        // first get children recursively
        List<TreeItem<Instance>> childrenTreeItem = new ArrayList<>();
        instancesTree.getChildren(instanceId).forEach(instanceChildId -> childrenTreeItem.add(this.buildInstanceTreeItem(instanceChildId, instancesTree)));

        // then build tree item for current instance
        TreeItem<Instance> instanceTreeItem = new TreeItem<>(instancesTree.getRef(instanceId));
        instanceTreeItem.setExpanded(true);
        instanceTreeItem.getChildren().addAll(childrenTreeItem);

        return instanceTreeItem;
    }

	@Override
	public boolean onPaused() {
		return true;
	}

	@Override
	public boolean onResumed() {
		return true;
	}

	@Override
	public boolean onStopped() {
		return true;
	}

	@Override
	public void onDisposed() {

	}

	@Override
	public boolean onFailed() {
		return true;
	}
}
