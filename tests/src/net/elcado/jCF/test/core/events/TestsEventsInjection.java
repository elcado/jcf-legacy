/*
 * Copyright (c). Frédéric Cadier (2014-2017)
 *
 * This software is a computer program whose purpose is to allow component programming in pure java.
 *
 * This software is governed by the CeCILL-C license under French law and abiding by the rules of distribution of free software. You can use, modify and/or redistribute the software under the terms of the CeCILL-C license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy, modify and redistribute granted by the license, users are provided only with a limited warranty and the software's author, the holder of the economic rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, that may mean that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured and, more generally, to use and operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had knowledge of the CeCILL-C license and that you accept its terms.
 */
package net.elcado.jCF.test.core.events;

import junit.utils.ExtendedRunner;
import junit.utils.Order;
import net.elcado.jCF.core.annotations.Component;
import net.elcado.jCF.core.annotations.Id;
import net.elcado.jCF.core.annotations.UsedService;
import net.elcado.jCF.core.container.ComponentsServices;
import net.elcado.jCF.core.container.Container;
import net.elcado.jCF.core.container.InstancesServices;
import net.elcado.jCF.core.lang.instance.InstanceId;
import net.elcado.jCF.log.LoggingServices;
import net.elcado.jCF.test.core.components.MessagesSink;
import net.elcado.jCF.test.core.components.MessagesSource;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(ExtendedRunner.class)
@Component
public class TestsEventsInjection {

	@UsedService
	private final transient ComponentsServices componentsServices = null;

	@UsedService
	private final transient InstancesServices instancesServices = null;

    @UsedService
    private final transient LoggingServices loggingServices = null;

    @Id
	private final transient InstanceId thisId = null;

	@Before
	public void setup() {
		Container.initialize(this);

		this.componentsServices.registerComponent(MessagesSource.class);
		this.componentsServices.registerComponent(MessagesSink.class);

		this.loggingServices.info(
				  "######################\n"
				+ "### LAUNCHING TEST ###\n"
				+ "######################");
	}

	@After
	public void tearDown() {
		this.loggingServices.info(
				  "###################\n"
				+ "### ENDING TEST ###\n"
				+ "###################");

		Container.terminate();
	}

	private void pause(long millis) {
		try {
			Thread.sleep(millis);
		} catch (InterruptedException e) {}
	}

	/**
	 * Deploys	1/ messages source
	 * 			2/ messages sink
	 * Starts messages source
	 */
	@Test
	@Order(1)
	public void testEventsInjection() {
		// deploy source
		InstanceId messagesSourceId = this.instancesServices.deployInstance(this.thisId, MessagesSource.class);

		// deploy sink
		this.instancesServices.deployInstance(this.thisId, MessagesSink.class);

		// start sources
		this.instancesServices.startInstance(messagesSourceId);

		this.pause(10000);

		// stop source
		this.instancesServices.stopInstance(messagesSourceId);
	}

	/**
	 * Deploys	1/ many messages source
	 * 			2/ many messages sink
	 * Starts messages sources
	 */
	@Test
	@Order(2)
	public void testManyEventsInjection() {
		// deploy source
		InstanceId messagesSource1Id = this.instancesServices.deployInstance(this.thisId, MessagesSource.class);
		InstanceId messagesSource2Id = this.instancesServices.deployInstance(this.thisId, MessagesSource.class);
		InstanceId messagesSource3Id = this.instancesServices.deployInstance(this.thisId, MessagesSource.class);

		// deploy sink
		this.instancesServices.deployInstance(this.thisId, MessagesSink.class);
		this.instancesServices.deployInstance(this.thisId, MessagesSink.class);

		// start sources
		this.instancesServices.startInstance(messagesSource1Id);
		this.instancesServices.startInstance(messagesSource2Id);
		this.instancesServices.startInstance(messagesSource3Id);

		this.pause(10000);

		// stop source
		this.instancesServices.stopInstance(messagesSource1Id);
		this.instancesServices.stopInstance(messagesSource2Id);
		this.instancesServices.stopInstance(messagesSource3Id);
	}

	/**
	 * Deploys	1/ messages source
	 * 			2/ messages sink
	 * Starts messages source
	 * Undeploys some sources and sinks
	 */
	@Test
	@Order(3)
	public void testManyEventsInjectionAndUndeploySome() {
		// deploy source
		InstanceId messagesSourceId_0 = this.instancesServices.deployInstance(this.thisId, MessagesSource.class);
		InstanceId messagesSourceId_1 = this.instancesServices.deployInstance(this.thisId, MessagesSource.class);
		InstanceId messagesSourceId_2 = this.instancesServices.deployInstance(this.thisId, MessagesSource.class);

		// deploy sink
		this.instancesServices.deployInstance(this.thisId, MessagesSink.class);
		InstanceId messageSinkId_1 = this.instancesServices.deployInstance(this.thisId, MessagesSink.class);

		// start sources
		this.instancesServices.startInstance(messagesSourceId_0);
		this.instancesServices.startInstance(messagesSourceId_1);
		this.instancesServices.startInstance(messagesSourceId_2);

		this.pause(5000);

		// undeploy some sources and sinks
		this.instancesServices.undeployInstance(messageSinkId_1);

		this.instancesServices.stopInstance(messagesSourceId_2);
		this.instancesServices.undeployInstance(messagesSourceId_2);

		this.pause(5000);

		// stop source
		this.instancesServices.stopInstance(messagesSourceId_0);
		this.instancesServices.stopInstance(messagesSourceId_1);
	}

	// TODO other tests with tree of instances
}
