/*
 * Copyright (c). Frédéric Cadier (2014-2017)
 *
 * This software is a computer program whose purpose is to allow component programming in pure java.
 *
 * This software is governed by the CeCILL-C license under French law and abiding by the rules of distribution of free software. You can use, modify and/or redistribute the software under the terms of the CeCILL-C license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy, modify and redistribute granted by the license, users are provided only with a limited warranty and the software's author, the holder of the economic rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, that may mean that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured and, more generally, to use and operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had knowledge of the CeCILL-C license and that you accept its terms.
 */
package net.elcado.jCF.test.core.parameters;

import junit.utils.ExtendedRunner;
import junit.utils.Order;
import net.elcado.jCF.core.annotations.Component;
import net.elcado.jCF.core.annotations.Id;
import net.elcado.jCF.core.annotations.ParametersService;
import net.elcado.jCF.core.annotations.UsedService;
import net.elcado.jCF.core.container.ComponentsServices;
import net.elcado.jCF.core.container.Container;
import net.elcado.jCF.core.container.InstancesServices;
import net.elcado.jCF.core.lang.instance.InstanceId;
import net.elcado.jCF.core.lang.instance.parameters.Parameters;
import net.elcado.jCF.log.LoggingServices;
import net.elcado.jCF.test.core.components.ServiceMultiUser;
import net.elcado.jCF.test.core.components.ServiceProviderParameter;
import net.elcado.jCF.test.core.components.ServiceProviderWithParameter;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(ExtendedRunner.class)
@Component
public class TestsMultiInjectionWithParameters {

	@UsedService
	private final transient ComponentsServices componentsServices = null;

	@UsedService
	private final transient InstancesServices instancesServices = null;

    @UsedService
    private final transient LoggingServices loggingServices = null;

    @Id
	private final transient InstanceId thisId = null;

	@ParametersService
	private final transient Parameters<ServiceProviderParameter> serviceProviderParameter = null;

	@Before
	public void setup() {
		Container.initialize(this);

		this.componentsServices.registerComponent(ServiceProviderWithParameter.class);
		this.componentsServices.registerComponent(ServiceMultiUser.class);

		this.loggingServices.info(
				  "######################\n"
				+ "### LAUNCHING TEST ###\n"
				+ "######################");
	}

	@After
	public void tearDown() {
		this.loggingServices.info(
				  "###################\n"
				+ "### ENDING TEST ###\n"
				+ "###################");

		Container.terminate();
	}

	private void pause(long millis) {
		try {
			Thread.sleep(millis);
		} catch (InterruptedException e) {}
	}

	/**
	 * Deploys	1/ service providers
	 * 			2/ service user
	 */
	@Test
	@Order(1)
	public void deployMultiUserTest() {
		// deploy providers
		InstanceId providerOne = this.instancesServices.deployInstance(this.thisId, ServiceProviderWithParameter.class);
		InstanceId providerTwo = this.instancesServices.deployInstance(this.thisId, ServiceProviderWithParameter.class);
		InstanceId providerThree = this.instancesServices.deployInstance(this.thisId, ServiceProviderWithParameter.class);

		this.serviceProviderParameter.set(providerOne, ServiceProviderParameter.NAME, "one");
		this.serviceProviderParameter.set(providerTwo, ServiceProviderParameter.NAME, "two");
		this.serviceProviderParameter.set(providerThree, ServiceProviderParameter.NAME, "three");

		// deploy user
		InstanceId userId = this.instancesServices.deployInstance(this.thisId, ServiceMultiUser.class);

		// start user
		this.instancesServices.startInstance(userId);

		this.pause(5000);

		// stop user
		this.instancesServices.stopInstance(userId);
	}
}
