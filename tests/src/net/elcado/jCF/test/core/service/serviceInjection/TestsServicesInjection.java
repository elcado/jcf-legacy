/*
 * Copyright (c). Frédéric Cadier (2014-2018)
 *
 * This software is a computer program whose purpose is to allow component programming in pure java.
 *
 * This software is governed by the CeCILL-C license under French law and abiding by the rules of distribution of free software. You can use, modify and/or redistribute the software under the terms of the CeCILL-C license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy, modify and redistribute granted by the license, users are provided only with a limited warranty and the software's author, the holder of the economic rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, that may mean that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured and, more generally, to use and operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had knowledge of the CeCILL-C license and that you accept its terms.
 */
package net.elcado.jCF.test.core.service.serviceInjection;

import junit.utils.ExtendedRunner;
import junit.utils.Order;
import net.elcado.jCF.core.annotations.Component;
import net.elcado.jCF.core.annotations.Id;
import net.elcado.jCF.core.annotations.UsedService;
import net.elcado.jCF.core.container.ComponentsServices;
import net.elcado.jCF.core.container.Container;
import net.elcado.jCF.core.container.InstancesServices;
import net.elcado.jCF.core.lang.instance.InstanceId;
import net.elcado.jCF.log.LoggingServices;
import net.elcado.jCF.test.core.components.ServiceProvider;
import net.elcado.jCF.test.core.components.ServiceUser;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(ExtendedRunner.class)
@Component
public class TestsServicesInjection {

	@UsedService
	private final transient ComponentsServices componentsServices = null;

	@UsedService
	private final transient InstancesServices instancesServices = null;

    @UsedService
    private final transient LoggingServices loggingServices = null;

    @Id
	private final transient InstanceId thisId = null;

	private static final int SMALL_PAUSE_TIME_IN_MILLIS = 751;
	private static final int BIG_PAUSE_TIME_IN_MILLIS = 2384;

	@Before
	public void setup() {
        System.setProperty("org.slf4j.simpleLogger.defaultLogLevel", "info");

        Container.initialize(this);

		assert this.thisId != null;

		this.componentsServices.registerComponent(ServiceProvider.class);
		this.componentsServices.registerComponent(ServiceUser.class);

		this.loggingServices.info(
				  "######################\n"
				+ "### LAUNCHING TEST ###\n"
				+ "######################");
	}

	@After
	public void tearDown() {
		this.loggingServices.info(
				  "###################\n"
				+ "### ENDING TEST ###\n"
				+ "###################");

		Container.terminate();
	}

	private void pause(long millis) {
		try {
			Thread.sleep(millis);
		} catch (InterruptedException e) {}
	}

	private InstanceId deployInstance(InstanceId parentId, Class<?> componentType, boolean autoStart) {
		this.loggingServices.info("### Deploying " + componentType.getSimpleName() + " instance");
		InstanceId deployedInstanceId = this.instancesServices.deployInstance(parentId, componentType, autoStart);

		this.pause(TestsServicesInjection.SMALL_PAUSE_TIME_IN_MILLIS);

		return deployedInstanceId;
	}

	private boolean startInstance(InstanceId instanceId, Class<?> componentType) {
		this.loggingServices.info("### Starting " + componentType.getSimpleName() + " instance");
		boolean isInstanceStarted = this.instancesServices.startInstance(instanceId);

		this.pause(TestsServicesInjection.BIG_PAUSE_TIME_IN_MILLIS);

		return isInstanceStarted;
	}

	private boolean stopInstance(InstanceId instanceId, Class<?> componentType) {
		this.loggingServices.info("### Stoping " + componentType.getSimpleName() + " instance");
		boolean isInstanceStopped = this.instancesServices.stopInstance(instanceId);

		this.pause(TestsServicesInjection.SMALL_PAUSE_TIME_IN_MILLIS);

		return isInstanceStopped;
	}

	private boolean undeployInstance(InstanceId instanceId, Class<?> componentType) {
		this.loggingServices.info("### Undeploying " + componentType.getSimpleName() + " instance");
		boolean isInstanceUndeployed = this.instancesServices.undeployInstance(instanceId);

		this.pause(TestsServicesInjection.SMALL_PAUSE_TIME_IN_MILLIS);

		return isInstanceUndeployed;
	}

	/**
	 * Deploys	1/ service provider
	 * 			2/ service user
	 */
	@Test
	@Order(1)
	public void deployUserAfterProviderTest() {
		// deploy provider
		this.deployInstance(this.thisId, ServiceProvider.class, true);

		// deploy and start user
		InstanceId userId = this.deployInstance(this.thisId, ServiceUser.class, false);
		this.startInstance(userId, ServiceUser.class);

		// stop user
		this.stopInstance(userId, ServiceUser.class);
	}

	/**
	 * Deploys	1/ service user
	 * 			2/ service provider
	 */
	@Test
	@Order(2)
	public void deployUserBeforeProviderTest() {
		// deploy and start user
		InstanceId userId = this.deployInstance(this.thisId, ServiceUser.class, false);
		this.startInstance(userId, ServiceUser.class);

		// deploy and start provider
		this.deployInstance(this.thisId, ServiceProvider.class, true);

		// stop user
		this.stopInstance(userId, ServiceUser.class);
	}

	@Test
	@Order(3)
	public void deployUndeployTest() {
		// deploy provider
		InstanceId providerId = this.deployInstance(this.thisId, ServiceProvider.class, true);

		// deploy and start user
		InstanceId userId = this.deployInstance(this.thisId, ServiceUser.class, false);
		this.startInstance(userId, ServiceUser.class);

		// undeploy and re-deploy and start user
		this.stopInstance(userId, ServiceUser.class);
		this.undeployInstance(userId, ServiceUser.class);
		userId = this.deployInstance(this.thisId, ServiceUser.class, false);
        this.startInstance(userId, ServiceUser.class);
        this.stopInstance(userId, ServiceUser.class);
        this.undeployInstance(userId, ServiceUser.class);

		// undeploy and re-deploy provider
		this.undeployInstance(providerId, ServiceProvider.class);
		this.deployInstance(this.thisId, ServiceProvider.class, true);
	}
}
