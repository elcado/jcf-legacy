/*
 * Copyright (c). Frédéric Cadier (2014-2017)
 *
 * This software is a computer program whose purpose is to allow component programming in pure java.
 *
 * This software is governed by the CeCILL-C license under French law and abiding by the rules of distribution of free software. You can use, modify and/or redistribute the software under the terms of the CeCILL-C license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy, modify and redistribute granted by the license, users are provided only with a limited warranty and the software's author, the holder of the economic rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, that may mean that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured and, more generally, to use and operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had knowledge of the CeCILL-C license and that you accept its terms.
 */
package junit.utils;

import org.junit.Ignore;
import org.junit.runner.Description;
import org.junit.runner.notification.RunNotifier;
import org.junit.runners.BlockJUnit4ClassRunner;
import org.junit.runners.model.FrameworkMethod;
import org.junit.runners.model.InitializationError;
import org.junit.runners.model.Statement;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Execute tests with respect of the eventual {@link Order @Order} and
 * {@link Repeat @Repeat} annotations.
 * <p>
 * {@link Repeat @Repeat} annotation allows to repeatedly execute a given test.
 * <p>
 * {@link Order @Order} annotation allows to execute tests in a given order.
 * Take care, when executing tests in a constrained order to not introduce
 * dependencies between tests. This is to be use, for example, to execute tests
 * in increasing complexity. The not ordered tests will be executed after the
 * ordered ones, in random order.
 * 
 * 
 * @author Frédéric Cadier
 */
public class ExtendedRunner extends BlockJUnit4ClassRunner {

	public ExtendedRunner(Class<?> klass) throws InitializationError {
		super(klass);
	}

	/**
	 * In case the method is annotated with Repeat, create a "suite"
	 * description. Otherwise, pass the handle to the super.
	 */
	@Override
	protected Description describeChild(FrameworkMethod method) {
		if (method.getAnnotation(Repeat.class) != null && method.getAnnotation(Ignore.class) == null) {
			return describeRepeatTest(method);
		}
		return super.describeChild(method);
	}

	private Description describeRepeatTest(FrameworkMethod method) {
		int times = method.getAnnotation(Repeat.class).value();

		Description description = Description.createSuiteDescription(testName(method) + " [" + times + " times]", method.getAnnotations());

		for (int i = 1; i <= times; i++) {
			description.addChild(Description.createTestDescription(getTestClass().getJavaClass(), "[" + i + "] " + testName(method)));
		}

		return description;
	}

	/**
	 * Order test methods through the {@link Order @Order} annotation.
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	protected List computeTestMethods() {
		List list = super.computeTestMethods();
		List copy = new ArrayList(list);
		Collections.sort(copy, new Comparator<FrameworkMethod>() {
			public int compare(FrameworkMethod method1, FrameworkMethod method2) {
				Integer orderMethod1 = null;
				Integer orderMethod2 = null;

				if (method1.getAnnotation(Order.class) != null)
					orderMethod1 = method1.getAnnotation(Order.class).value();

				if (method2.getAnnotation(Order.class) != null)
					orderMethod2 = method2.getAnnotation(Order.class).value();

				int compareResult = 0;
				if (orderMethod1 == null && orderMethod2 == null)
					compareResult = 0;
				else if (orderMethod1 == null && orderMethod2 != null)
					compareResult = Integer.MAX_VALUE;
				else if (orderMethod1 != null && orderMethod2 == null)
					compareResult = Integer.MIN_VALUE;
				else
					compareResult = orderMethod1.compareTo(orderMethod2);

				return compareResult;
			}
		});
		return copy;
	}

	/**
	 * In case the method is annotated with {@link Repeat} do specific handle.
	 * Otherwise, pass the handle to the super.
	 */
	@Override
	protected void runChild(final FrameworkMethod method, RunNotifier notifier) {
		Description description = describeChild(method);

		if (method.getAnnotation(Repeat.class) != null && method.getAnnotation(Ignore.class) == null) {
			runRepeatedly(methodBlock(method), description, notifier);
		}

		super.runChild(method, notifier);
	}

	private void runRepeatedly(Statement statement, Description description, RunNotifier notifier) {
		for (Description desc : description.getChildren()) {
			runLeaf(statement, desc, notifier);
		}
	}

}
