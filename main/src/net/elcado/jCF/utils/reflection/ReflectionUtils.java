/*
 * Copyright (c). Frédéric Cadier (2014-2018)
 *
 * This software is a computer program whose purpose is to allow component programming in pure java.
 *
 * This software is governed by the CeCILL-C license under French law and abiding by the rules of distribution of free software. You can use, modify and/or redistribute the software under the terms of the CeCILL-C license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy, modify and redistribute granted by the license, users are provided only with a limited warranty and the software's author, the holder of the economic rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, that may mean that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured and, more generally, to use and operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had knowledge of the CeCILL-C license and that you accept its terms.
 */

package net.elcado.jCF.utils.reflection;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;

public class ReflectionUtils {
    private ReflectionUtils() {
//begin of modifiable zone(JavaSuper).....C/ccd2d9ee-9d68-4b5e-849a-05789cc96104

//end of modifiable zone(JavaSuper).......E/ccd2d9ee-9d68-4b5e-849a-05789cc96104
//begin of modifiable zone(JavaCode)......C/ccd2d9ee-9d68-4b5e-849a-05789cc96104

//end of modifiable zone(JavaCode)........E/ccd2d9ee-9d68-4b5e-849a-05789cc96104
    }

    public static ReflectionUtils getReflectionUtils() {
//begin of modifiable zone(JavaCode)......C/96a67d95-514d-4d77-bda5-877d723a9947

//end of modifiable zone(JavaCode)........E/96a67d95-514d-4d77-bda5-877d723a9947
//begin of modifiable zone................T/eeaaf235-671c-45b7-893f-edada7d81c38
        return SingletonHolder.instance;
//end of modifiable zone..................E/eeaaf235-671c-45b7-893f-edada7d81c38
    }

    public <T> T buildObject(final Class<T> clazz, final Object... params) {
//begin of modifiable zone................T/8aec88a8-124d-4f7e-9d45-c0fa6b8fabec
        // get constructor
        Constructor<T> constructor = this.getConstructor(clazz, params);
        T object = null;
        try {
            constructor.setAccessible(true);
            object = constructor.newInstance(params);
        }
        catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
            e.printStackTrace();
        }
//end of modifiable zone..................E/8aec88a8-124d-4f7e-9d45-c0fa6b8fabec
//begin of modifiable zone................T/50faefcb-1d9b-4b8f-9c60-3bc5bdb5dfdc
        return object;
//end of modifiable zone..................E/50faefcb-1d9b-4b8f-9c60-3bc5bdb5dfdc
    }

    private <T> Constructor<T> getConstructor(final Class<T> clazz, final Object... params) {
//begin of modifiable zone................T/7541b9b2-2791-4e78-a225-2bc2723bacf3
        // get args types list
        Class<?>[] argsTypes = new Class<?>[params.length];
        for (int i = 0; i < params.length; i++) {
            argsTypes[i] = params[i].getClass();
        }
        
        // get constructor that corresponds to the list of args
        Constructor<T> constructor = null;
        try {
            constructor = clazz.getDeclaredConstructor(argsTypes);
        }
        catch (NoSuchMethodException|SecurityException e) {
            e.printStackTrace();
        }
//end of modifiable zone..................E/7541b9b2-2791-4e78-a225-2bc2723bacf3
//begin of modifiable zone................T/75c59e59-bf66-4418-a793-1bf265550bb1
        return constructor;
//end of modifiable zone..................E/75c59e59-bf66-4418-a793-1bf265550bb1
    }

    public Object getFieldValue(final Object object, final String fieldName) {
//begin of modifiable zone................T/c7d4c2f2-e840-4983-a766-9ee41110ec11
        Object fieldValue = null;
        try {
            Field componentsManagerServicesField = object.getClass().getDeclaredField(fieldName);
            componentsManagerServicesField.setAccessible(true);
            fieldValue = componentsManagerServicesField.get(object);
        } catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException e) {
            e.printStackTrace();
        }
//end of modifiable zone..................E/c7d4c2f2-e840-4983-a766-9ee41110ec11
//begin of modifiable zone................T/9957f2ba-e1eb-4681-aea6-f6e11f2b3558
        return fieldValue;
//end of modifiable zone..................E/9957f2ba-e1eb-4681-aea6-f6e11f2b3558
    }

    public void setFieldValue(final Object object, final String fieldName, final Object fieldValue) {
//begin of modifiable zone................T/fb9c39d6-0f75-475d-a3c4-58a1249cd1a9
        try {
            Field componentsManagerServicesField = object.getClass().getDeclaredField(fieldName);
            componentsManagerServicesField.setAccessible(true);
            componentsManagerServicesField.set(object, fieldValue);
        } catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException e) {
            e.printStackTrace();
        }
//end of modifiable zone..................E/fb9c39d6-0f75-475d-a3c4-58a1249cd1a9
    }

    private static class SingletonHolder {
        private static final ReflectionUtils instance = new ReflectionUtils();

    }

}
