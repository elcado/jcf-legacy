/*
 * Copyright (c). Frédéric Cadier (2014-2018)
 *
 * This software is a computer program whose purpose is to allow component programming in pure java.
 *
 * This software is governed by the CeCILL-C license under French law and abiding by the rules of distribution of free software. You can use, modify and/or redistribute the software under the terms of the CeCILL-C license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy, modify and redistribute granted by the license, users are provided only with a limited warranty and the software's author, the holder of the economic rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, that may mean that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured and, more generally, to use and operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had knowledge of the CeCILL-C license and that you accept its terms.
 */

package net.elcado.jCF.core.container;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import net.elcado.jCF.core.annotations.EventsSink;
import net.elcado.jCF.core.annotations.ImplementedService;
import net.elcado.jCF.core.annotations.UsedService;
import net.elcado.jCF.core.container.componentsManager.ComponentsManager;
import net.elcado.jCF.core.container.componentsManager.ComponentsManagerServices;
import net.elcado.jCF.core.container.instancesManager.InstancesManager;
import net.elcado.jCF.core.container.instancesManager.InstancesManagerDeploymentServices;
import net.elcado.jCF.core.container.instancesManager.InstancesManagerEvent;
import net.elcado.jCF.core.container.instancesManager.InstancesManagerLifeCycleServices;
import net.elcado.jCF.core.container.instancesManager.dependenciesResolver.DependenciesResolver;
import net.elcado.jCF.core.container.instancesManager.instanceStateManager.InstanceStateManager;
import net.elcado.jCF.core.container.instancesManager.parametersManager.ParametersManager;
import net.elcado.jCF.core.container.instancesManager.remoteManager.RemoteManager;
import net.elcado.jCF.core.container.instancesManager.remoteManager.remoteRegistry.RemoteRegistry;
import net.elcado.jCF.core.lang.component.Component;
import net.elcado.jCF.core.lang.instance.Instance;
import net.elcado.jCF.core.lang.instance.InstanceId;
import net.elcado.jCF.core.lang.instance.events.Sink;
import net.elcado.jCF.extension.debug.DebugServices;
import net.elcado.jCF.log.LoggingServices;
import net.elcado.jCF.log.SLF4jLogging;
import net.elcado.jCF.utils.reflection.ReflectionUtils;

@net.elcado.jCF.core.annotations.Component
public class Container implements ComponentsServices, InstancesServices {
    @ImplementedService
    public final transient ComponentsServices componentsServices = this;

    @ImplementedService
    public final transient InstancesServices instancesServices = this;

    @UsedService
    private final transient LoggingServices loggingServices = null;

    @UsedService
    private final transient ComponentsManagerServices componentsManagerServices = null;

    @UsedService
    private final transient InstancesManagerDeploymentServices instancesManagerDeployementServices = null;

    @UsedService
    private final transient InstancesManagerLifeCycleServices instancesManagerLifeCycleServices = null;

    @UsedService
    private final transient DebugServices debugServices = null;

    @EventsSink
    private final transient Sink<InstancesManagerEvent> instancesManagerEventsSink = new InstancesManagerEventsHandler();

    private final List<InstanceId> rootInstanceIds = new ArrayList<> ();

    private final List<InstanceId> manuallyStartedInstanceIds = new ArrayList<> ();

    private InstanceId containerId;

    private InstanceId instancesManagerId;

    /**
     * Setup a jCF {@link Container} and inject the supplied object as the root component of the deployed application. This means that all other deployed components are children of this root component.
     * @param rootInstance an existing component instance to be injected into the new jCF application.
     */
    public static void initialize(Object rootInstance) {
//begin of modifiable zone................T/8ef27d87-1c3a-453e-a142-93a5d46a7a67
        if (System.getProperty("org.slf4j.simpleLogger.defaultLogLevel") == null) {
            System.setProperty("org.slf4j.simpleLogger.defaultLogLevel", "info");// -Dorg.slf4j.simpleLogger.defaultLogLevel=debug
        }
        
        if (rootInstance == null) {
            throw new NullPointerException("Root instance cannot be null");
        }
        
        final ReflectionUtils reflectionUtils = ReflectionUtils.getReflectionUtils();
        
        // build container and deploy it as a component
        Container container = SingletonHolder.instance;
        ComponentsManager componentsManager = reflectionUtils.buildObject(ComponentsManager.class);
        InstancesManager instancesManager = reflectionUtils.buildObject(InstancesManager.class);
        DependenciesResolver dependenciesResolver = reflectionUtils.buildObject(DependenciesResolver.class);
        ParametersManager parametersManager = reflectionUtils.buildObject(ParametersManager.class);
        RemoteRegistry remoteRegistry = reflectionUtils.buildObject(RemoteRegistry.class);
        RemoteManager remoteManager = reflectionUtils.buildObject(RemoteManager.class);
        InstanceStateManager instanceStateManager = reflectionUtils.buildObject(InstanceStateManager.class);
        SLF4jLogging slf4jLogging = reflectionUtils.buildObject(SLF4jLogging.class);
        
        reflectionUtils.setFieldValue(container, "componentsManagerServices", componentsManager);
        reflectionUtils.setFieldValue(container, "instancesManagerDeployementServices", instancesManager);
        reflectionUtils.setFieldValue(container, "instancesManagerLifeCycleServices", instancesManager);
        reflectionUtils.setFieldValue(container, "loggingServices", slf4jLogging);
        
        reflectionUtils.setFieldValue(remoteManager, "loggingServices", slf4jLogging);
        
        reflectionUtils.setFieldValue(componentsManager, "loggingServices", slf4jLogging);
        
        reflectionUtils.setFieldValue(dependenciesResolver, "loggingServices", slf4jLogging);
        reflectionUtils.setFieldValue(dependenciesResolver, "remoteManagerServices", remoteManager);
        
        reflectionUtils.setFieldValue(parametersManager, "loggingServices", slf4jLogging);
        
        reflectionUtils.setFieldValue(remoteRegistry, "loggingServices", slf4jLogging);
        
        reflectionUtils.setFieldValue(remoteManager, "loggingServices", slf4jLogging);
        reflectionUtils.setFieldValue(remoteManager, "remoteRegistryServices", remoteRegistry);
        
        reflectionUtils.setFieldValue(instancesManager, "dependenciesResolverServices", dependenciesResolver);
        reflectionUtils.setFieldValue(instancesManager, "parametersManagerService", parametersManager);
        reflectionUtils.setFieldValue(instancesManager, "instanceStateManagerServices", instanceStateManager);
        reflectionUtils.setFieldValue(instancesManager, "remoteManagerServices", remoteManager);
        reflectionUtils.setFieldValue(instancesManager, "loggingServices", slf4jLogging);
        
        container.registerComponent(Container.class);
        container.registerComponent(ComponentsManager.class);
        container.registerComponent(InstancesManager.class);
        container.registerComponent(DependenciesResolver.class);
        container.registerComponent(ParametersManager.class);
        container.registerComponent(RemoteRegistry.class);
        container.registerComponent(RemoteManager.class);
        container.registerComponent(InstanceStateManager.class);
        container.registerComponent(SLF4jLogging.class);
        
        // deploy and starts logger
        InstanceId slf4jLoggingId = container.deployInstance(null, SLF4jLogging.class, slf4jLogging, true);
        assert slf4jLoggingId != null : "Cannot deploy logging manager";
        
        // deploy container
        container.containerId = container.deployInstance(null, Container.class, container, true);
        assert container.containerId != null : "Cannot deploy container";
        
        InstanceId componentsManagerId = container.deployInstance(container.containerId, ComponentsManager.class, componentsManager, true);
        assert componentsManagerId != null : "Cannot deploy components manager";
        
        container.instancesManagerId = container.deployInstance(container.containerId, InstancesManager.class, instancesManager, true);
        assert container.instancesManagerId != null : "Cannot deploy instances manager";
        
        InstanceId dependenciesResolverId = container.deployInstance(container.instancesManagerId, DependenciesResolver.class, dependenciesResolver, true);
        assert dependenciesResolverId != null : "Cannot deploy dependencies manager";
        
        InstanceId parametersManagerId = container.deployInstance(container.instancesManagerId, ParametersManager.class, parametersManager, true);
        assert parametersManagerId != null : "Cannot deploy parameters manager";
        
        InstanceId instanceStateManagerId = container.deployInstance(container.instancesManagerId, InstanceStateManager.class, instanceStateManager, true);
        assert instanceStateManagerId != null : "Cannot deploy instance state manager";
        
        InstanceId remoteManagerId = container.deployInstance(container.instancesManagerId, RemoteManager.class, remoteManager, true);
        assert remoteManagerId != null : "Cannot deploy remote manager";
        
        InstanceId remoteRegistryId = container.deployInstance(remoteManagerId, RemoteRegistry.class, remoteRegistry, true);
        assert remoteRegistryId != null : "Cannot deploy remote registry";
        
        // deploy supplied root instance
        container.registerComponent(rootInstance.getClass());
        container.deployInstance(null, rootInstance.getClass(), rootInstance, true);
//end of modifiable zone..................E/8ef27d87-1c3a-453e-a142-93a5d46a7a67
    }

    public static void terminate() {
//begin of modifiable zone................T/29787edd-4b65-4c46-b91c-c3e0c000a658
        // block until manuallyStartedInstanceIds list is empty
        synchronized (SingletonHolder.instance.manuallyStartedInstanceIds) {
            while (!SingletonHolder.instance.manuallyStartedInstanceIds.isEmpty()) {
                try {
                    SingletonHolder.instance.manuallyStartedInstanceIds.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
        
        // stop all roots but container
        List<InstanceId> rootIdsCopy = new ArrayList<>(SingletonHolder.instance.rootInstanceIds);
        rootIdsCopy.stream()
            .filter(rootId -> !SingletonHolder.instance.containerId.equals(rootId))
            .forEach(SingletonHolder.instance::stopInstance);
        
        // undeploy all roots but container
        rootIdsCopy.stream()
            .filter(rootId -> !SingletonHolder.instance.containerId.equals(rootId))
            .forEach(SingletonHolder.instance::undeployInstance);
        
        // assert that all instances but container have been undeployed
        assert (SingletonHolder.instance.rootInstanceIds.size() == 1 && SingletonHolder.instance.rootInstanceIds.contains(SingletonHolder.instance.containerId)) : "Not all root instances have been undeployed";
        
        // stop container
        SingletonHolder.instance.stopInstance(SingletonHolder.instance.containerId);
        
        // unregister all components (including Container)
        SingletonHolder.instance.componentsManagerServices.getComponents()
            .forEach(SingletonHolder.instance.componentsManagerServices::unregisterComponent);
        
        // assert that all components (including Container) have been unregistered
        assert SingletonHolder.instance.componentsManagerServices.getComponents().isEmpty() : "Not all components have been unregistered";
        
        // remove container
        SingletonHolder.instance.rootInstanceIds.remove(SingletonHolder.instance.containerId);
        
        // assert that all instances including container have been undeployed
        assert SingletonHolder.instance.rootInstanceIds.isEmpty() : "Cannot undeploy container";
//end of modifiable zone..................E/29787edd-4b65-4c46-b91c-c3e0c000a658
    }

    /**
     * Private constructor: a jCF component is not supposed to be built directly from user code.
     */
    private Container() {
//begin of modifiable zone(JavaSuper).....C/d7251bbd-ee07-4d5a-9d18-2d282b502512

//end of modifiable zone(JavaSuper).......E/d7251bbd-ee07-4d5a-9d18-2d282b502512
//begin of modifiable zone(JavaCode)......C/d7251bbd-ee07-4d5a-9d18-2d282b502512

//end of modifiable zone(JavaCode)........E/d7251bbd-ee07-4d5a-9d18-2d282b502512
    }

    /**
     * Register a new component.
     * @param componentType The class of the component.
     * @return true if component has been correctly registered, false otherwise.
     */
    public boolean registerComponent(Class<?> componentType) {
//begin of modifiable zone................T/df88a839-aa14-4569-8799-a6b92caf3876
        Component component = this.componentsManagerServices.registerComponent(componentType);
//end of modifiable zone..................E/df88a839-aa14-4569-8799-a6b92caf3876
//begin of modifiable zone................T/d88d80ea-b02c-4f87-9385-cc295c143fe0
        return component != null;
//end of modifiable zone..................E/d88d80ea-b02c-4f87-9385-cc295c143fe0
    }

    /**
     * Deploy a new instance of a registered component.
     * @param parentId The instance id of the parent of the deployed instance.
     * @param componentType The class of the component.
     * @return the instance id of the deployed instance. Null if the component is not registered.
     */
    public InstanceId deployInstance(final InstanceId parentId, Class<?> componentType) {
//begin of modifiable zone................T/a5daba74-970e-4214-a37f-beec958737b6
        InstanceId instanceId = this.deployInstance(parentId, componentType, true);
//end of modifiable zone..................E/a5daba74-970e-4214-a37f-beec958737b6
//begin of modifiable zone................T/81b2cb79-1b66-4f00-bd7f-8c6a80e8932e
        return instanceId;
//end of modifiable zone..................E/81b2cb79-1b66-4f00-bd7f-8c6a80e8932e
    }

    /**
     * Deploy a new instance of a registered component.
     * @param parentId The instance id of the parent of the deployed instance.
     * @param componentType The class of the component.
     * @param autoStart When set to true, which is the default value, the deployed instance will be automatically undeployed when calling {@link Container#terminate()}
     * @return the instance id of the deployed instance. Null if the component is not registered.
     */
    public InstanceId deployInstance(final InstanceId parentId, Class<?> componentType, final boolean autoStart) {
//begin of modifiable zone................T/a2cdf2b6-2539-4fe5-8607-0a741e8c25ee
        InstanceId instanceId = this.deployInstance(parentId, componentType, null, autoStart);
//end of modifiable zone..................E/a2cdf2b6-2539-4fe5-8607-0a741e8c25ee
//begin of modifiable zone................T/08634a56-fefc-4db0-9933-3b3e218570a1
        return instanceId;
//end of modifiable zone..................E/08634a56-fefc-4db0-9933-3b3e218570a1
    }

    /**
     * Deploy a new instance of a registered component.
     * @param parentId The instance id of the parent of the deployed instance.
     * @param componentType The class of the component.
     * @param object An existing instance of the component.
     * @param autoStart When set to true, which is the default value, the deployed instance will be automatically undeployed when calling {@link Container#terminate()}
     * @return the instance id of the deployed instance. Null if the component is not registered.
     */
    private InstanceId deployInstance(final InstanceId parentId, Class<?> componentType, Object object, final boolean autoStart) {
//begin of modifiable zone................T/8ffba2da-91bd-48e7-9dec-9be485f18fdc
        InstanceId instanceId = null;
        Component component = this.componentsManagerServices.getComponent(componentType);
        if (component != null) {
            // deploy and eventually add instance id in root list
            instanceId = this.instancesManagerDeployementServices.deployInstance(parentId, component, object);
            if (parentId == null) {
                this.rootInstanceIds.add(instanceId);
            }
        
            // initialize instance
            this.instancesManagerLifeCycleServices.initInstance(instanceId);
        
            // eventually start instance,
            // otherwise add instance id in list of instances that won't be automatically undeployed on terminate
            if (autoStart) {
                this.startInstance(instanceId);
            }
            else {
                synchronized (this.manuallyStartedInstanceIds) {
                    this.manuallyStartedInstanceIds.add(instanceId);
                }
            }
        }
        else {
            if (this.loggingServices != null) {
                this.loggingServices.error("Cannot find a registered component for " + componentType);
            }
        }
//end of modifiable zone..................E/8ffba2da-91bd-48e7-9dec-9be485f18fdc
//begin of modifiable zone................T/3bf06274-2907-406f-ac77-3a20e802587a
        return instanceId;
//end of modifiable zone..................E/3bf06274-2907-406f-ac77-3a20e802587a
    }

    /**
     * Start all instances that implements {@link net.elcado.jCF.core.lang.component.Startable Startable}.
     * @return true if all instances have been started, false otherwise
     */
    public boolean startInstances() {
//begin of modifiable zone................T/99457dac-0f90-47f0-b232-da8257fc4521
        boolean started = this.instancesManagerLifeCycleServices.startInstances();
//end of modifiable zone..................E/99457dac-0f90-47f0-b232-da8257fc4521
//begin of modifiable zone................T/11efaf46-7a31-4338-8e69-5f30e44ab3b3
        return started;
//end of modifiable zone..................E/11efaf46-7a31-4338-8e69-5f30e44ab3b3
    }

    /**
     * Start supplied instance if it is deployed and it implements {@link net.elcado.jCF.core.lang.component.Startable Startable}. Do nothing otherwise.
     * @param instanceId The instance id of the instance to remove.
     * @return true if instance has been started, false otherwise
     */
    public boolean startInstance(final InstanceId instanceId) {
//begin of modifiable zone................T/233accdb-a533-4c62-816a-1dee49d649c5
        boolean started = this.instancesManagerLifeCycleServices.startInstance(instanceId);
//end of modifiable zone..................E/233accdb-a533-4c62-816a-1dee49d649c5
//begin of modifiable zone................T/6d0802ec-19f4-424b-acd0-eba990c01457
        return started;
//end of modifiable zone..................E/6d0802ec-19f4-424b-acd0-eba990c01457
    }

    /**
     * Pauses all deployed instances that implements {@link net.elcado.jCF.core.lang.component.Startable Startable}.
     * @return true if all instances have been paused, false otherwise
     */
    public boolean pauseInstances() {
//begin of modifiable zone................T/2dad060e-19b6-4a82-b820-e06afbe5e61a
        boolean paused = this.instancesManagerLifeCycleServices.pauseInstances();
//end of modifiable zone..................E/2dad060e-19b6-4a82-b820-e06afbe5e61a
//begin of modifiable zone................T/030d3bf8-fe20-4b0c-bb0d-d886a70d977e
        return paused;
//end of modifiable zone..................E/030d3bf8-fe20-4b0c-bb0d-d886a70d977e
    }

    /**
     * Pauses supplied instance if it is deployed and it implements {@link net.elcado.jCF.core.lang.component.Startable Startable}. Do nothing otherwise.
     * @param instanceId The instance id of the instance to remove.
     * @return true if instance has been paused, false otherwise
     */
    public boolean pauseInstance(final InstanceId instanceId) {
//begin of modifiable zone................T/13bfb7f7-73b2-49e2-b10e-f2bfd73ca8d6
        boolean paused = this.instancesManagerLifeCycleServices.pauseInstance(instanceId);
//end of modifiable zone..................E/13bfb7f7-73b2-49e2-b10e-f2bfd73ca8d6
//begin of modifiable zone................T/1e68124a-6848-4d98-883a-83f9b30583a4
        return paused;
//end of modifiable zone..................E/1e68124a-6848-4d98-883a-83f9b30583a4
    }

    /**
     * Resumes all deployed instances that implements {@link net.elcado.jCF.core.lang.component.Startable Startable}.
     * @return true if all instances have been paused, false otherwise
     */
    public boolean resumeInstances() {
//begin of modifiable zone................T/e2ce575d-28af-4523-bd13-44fbc10e018b
        boolean resumed = this.instancesManagerLifeCycleServices.resumeInstances();
//end of modifiable zone..................E/e2ce575d-28af-4523-bd13-44fbc10e018b
//begin of modifiable zone................T/10886ba3-7230-4c79-b833-ae96a90f9ceb
        return resumed;
//end of modifiable zone..................E/10886ba3-7230-4c79-b833-ae96a90f9ceb
    }

    /**
     * Resumes supplied instance if it is deployed and it implements {@link net.elcado.jCF.core.lang.component.Startable Startable}. Do nothing otherwise.
     * @param instanceId The instance id of the instance to remove.
     * @return true if instance has been resumed, false otherwise
     */
    public boolean resumeInstance(final InstanceId instanceId) {
//begin of modifiable zone................T/038bc67f-f650-4731-b325-9767022236e3
        boolean resumed = this.instancesManagerLifeCycleServices.resumeInstance(instanceId);
//end of modifiable zone..................E/038bc67f-f650-4731-b325-9767022236e3
//begin of modifiable zone................T/067e2edb-fd21-427d-88fa-7625b62823c9
        return resumed;
//end of modifiable zone..................E/067e2edb-fd21-427d-88fa-7625b62823c9
    }

    /**
     * Stop all deployed instances that implements {@link net.elcado.jCF.core.lang.component.Startable Startable}.
     * @return true if all instances have been stopped, false otherwise
     */
    public boolean stopInstances() {
//begin of modifiable zone................T/146bb0d2-7925-4263-8e2d-538dca11fa6d
        boolean stopped = this.instancesManagerLifeCycleServices.stopInstances();
//end of modifiable zone..................E/146bb0d2-7925-4263-8e2d-538dca11fa6d
//begin of modifiable zone................T/5f8fd0ef-1818-47fd-bc51-4fcb4065c90a
        return stopped;
//end of modifiable zone..................E/5f8fd0ef-1818-47fd-bc51-4fcb4065c90a
    }

    /**
     * Stop supplied instance if it is deployed and it implements {@link net.elcado.jCF.core.lang.component.Startable Startable}. Do nothing otherwise.
     * @param instanceId The instance id of the instance to remove.
     * @return true if instance has been stopped, false otherwise
     */
    public boolean stopInstance(final InstanceId instanceId) {
//begin of modifiable zone................T/f38f85be-63ba-4847-a321-2a47d21c8bbf
        boolean stopped = this.instancesManagerLifeCycleServices.stopInstance(instanceId);
        
        // eventually remove from manuallyStartedInstanceIds
        synchronized (this.manuallyStartedInstanceIds) {
            if (stopped && this.manuallyStartedInstanceIds.contains(instanceId)) {
                this.manuallyStartedInstanceIds.remove(instanceId);
        
                // notify terminate()
                this.manuallyStartedInstanceIds.notify();
            }
        }
//end of modifiable zone..................E/f38f85be-63ba-4847-a321-2a47d21c8bbf
//begin of modifiable zone................T/5a23b88e-0d07-492d-9c72-78af904ac59b
        return stopped;
//end of modifiable zone..................E/5a23b88e-0d07-492d-9c72-78af904ac59b
    }

    /**
     * Undeploy an instance.
     * @param instanceId The instance id of the instance to remove.
     * @return true if instance has been undeployed, false otherwise
     */
    public boolean undeployInstance(final InstanceId instanceId) {
//begin of modifiable zone................T/b3e3ee1d-0343-442b-8176-79689390a55f
        boolean stopped = false;
        
        // stop instance if not manually started
        if (!manuallyStartedInstanceIds.contains(instanceId)) {
            stopped = this.stopInstance(instanceId);
        }
        
        // only undeploy stopped instance
        boolean isInstanceUndeployed = false;
        if (stopped) {
            List<InstanceId> undeployInstanceIds = this.instancesManagerDeployementServices.undeployInstance(instanceId);
        
            // instance is undeployed if the list of previously undeployed instance contains it
            isInstanceUndeployed = undeployInstanceIds.contains(instanceId);
        
            // eventually remove from rootInstanceIds
            if (isInstanceUndeployed) {
                this.rootInstanceIds.remove(instanceId);
            }
        }
        else {
            loggingServices.error("Cannot undeploy an instance before it has been stopped");
        }
//end of modifiable zone..................E/b3e3ee1d-0343-442b-8176-79689390a55f
//begin of modifiable zone................T/d63014f9-47f9-425f-bbdb-472baed7373f
        return isInstanceUndeployed;
//end of modifiable zone..................E/d63014f9-47f9-425f-bbdb-472baed7373f
    }

    /**
     * Unregister a component.
     * @param componentType The class of the component.
     * @return true if component has been correctly registered, false otherwise.
     */
    public boolean unregisterComponent(Class<?> componentType) {
//begin of modifiable zone................T/c60d7ac7-7325-43da-9c70-4e30be11e0f7
        Component component = this.componentsManagerServices.getComponent(componentType);
        if (component != null) {
            if (this.loggingServices != null) {
                this.loggingServices.debug(Thread.currentThread() + ": Removing component [" + componentType.getSimpleName() + "]");
            }
        
            // stop instances
            component.getInstances().stream()
                .map(Instance::getInstanceId)
                .forEach(this::stopInstance);
        
            // remove instances
            component.getInstances().stream()
                .map(Instance::getInstanceId)
                .forEach(this::undeployInstance);
        
            // remove component
            this.componentsManagerServices.unregisterComponent(component);
        
            if (this.loggingServices != null) {
                this.loggingServices.debug(Thread.currentThread() + ": [" + componentType.getSimpleName() + "] removed");
            }
        }
//end of modifiable zone..................E/c60d7ac7-7325-43da-9c70-4e30be11e0f7
//begin of modifiable zone................T/024ce64d-4535-4062-9c92-b7a72dcd2833
        return false;
//end of modifiable zone..................E/024ce64d-4535-4062-9c92-b7a72dcd2833
    }

    private static class SingletonHolder {
        private static final Container instance = new Container();

    }

    class InstancesManagerEventsHandler implements Sink<InstancesManagerEvent> {
        private final AtomicBoolean debugServicesDeployed = new AtomicBoolean(false);

        /**
         * Implements this to process event.
         * @param event The received event to be processed.
         */
        @Override
        public void processEvent(final InstancesManagerEvent event) {
//begin of modifiable zone................T/7798515a-6dac-4b02-929d-6d95384e38b1
            //System.out.println(event);
            // detect deployment of DebugServices
            if (event.getEventType() == InstancesManagerEvent.INSTANCE_INITIALIZED
                    && Container.this.debugServices != null
                    && this.debugServicesDeployed.compareAndSet(false, true)) {
                Container.this.debugServices.deployDisplay(Container.this.instancesManagerId);
            }
//end of modifiable zone..................E/7798515a-6dac-4b02-929d-6d95384e38b1
        }

    }

}
