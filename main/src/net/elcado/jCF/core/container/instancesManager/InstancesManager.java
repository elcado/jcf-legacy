/*
 * Copyright (c). Frédéric Cadier (2014-2018)
 *
 * This software is a computer program whose purpose is to allow component programming in pure java.
 *
 * This software is governed by the CeCILL-C license under French law and abiding by the rules of distribution of free software. You can use, modify and/or redistribute the software under the terms of the CeCILL-C license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy, modify and redistribute granted by the license, users are provided only with a limited warranty and the software's author, the holder of the economic rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, that may mean that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured and, more generally, to use and operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had knowledge of the CeCILL-C license and that you accept its terms.
 */

package net.elcado.jCF.core.container.instancesManager;

import java.util.ArrayList;
import java.util.List;
import net.elcado.jCF.core.container.instancesManager.dependenciesResolver.DependenciesResolverServices;
import net.elcado.jCF.core.container.instancesManager.instanceStateManager.InstanceStateManagerServices;
import net.elcado.jCF.core.container.instancesManager.parametersManager.ParametersManagerService;
import net.elcado.jCF.core.container.instancesManager.remoteManager.RemoteManagerServices;
import net.elcado.jCF.core.lang.component.Component;
import net.elcado.jCF.core.lang.component.events.EventsSinkPort;
import net.elcado.jCF.core.lang.component.events.EventsSourcePort;
import net.elcado.jCF.core.lang.component.parameters.ParametersPort;
import net.elcado.jCF.core.lang.component.services.ImplementedServicePort;
import net.elcado.jCF.core.lang.component.services.UsedServicePort;
import net.elcado.jCF.core.lang.instance.Instance;
import net.elcado.jCF.core.lang.instance.InstanceId;
import net.elcado.jCF.core.lang.instance.events.EventsSink;
import net.elcado.jCF.core.lang.instance.events.EventsSource;
import net.elcado.jCF.core.lang.instance.events.Sink;
import net.elcado.jCF.core.lang.instance.events.SinkQueue;
import net.elcado.jCF.core.lang.instance.events.Source;
import net.elcado.jCF.core.lang.instance.events.SourceDispatcher;
import net.elcado.jCF.core.lang.instance.parameters.Parameters;
import net.elcado.jCF.core.lang.instance.services.ImplementedService;
import net.elcado.jCF.core.lang.instance.services.UsedService;
import net.elcado.jCF.log.LoggingServices;
import net.elcado.jCF.utils.reflection.ReflectionUtils;
import net.elcado.jCF.utils.tree.Tree;

@net.elcado.jCF.core.annotations.Component
public class InstancesManager implements InstancesManagerDeploymentServices, InstancesManagerLifeCycleServices, InstancesManagerRemoteServices, InstancesManagerDebugServices {
    private final Tree<InstanceId,Instance> instancesTree = new Tree<>();

    @net.elcado.jCF.core.annotations.ImplementedService
    public final transient InstancesManagerDeploymentServices instancesManagerDeployementServices = this;

    @net.elcado.jCF.core.annotations.ImplementedService
    public final transient InstancesManagerLifeCycleServices instancesManagerLifeCycleServices = this;

    @net.elcado.jCF.core.annotations.ImplementedService
    private final transient InstancesManagerRemoteServices instancesManagerRemoteServices = this;

    @net.elcado.jCF.core.annotations.ImplementedService
    public final transient InstancesManagerDebugServices instancesManagerDebugServices = this;

    @net.elcado.jCF.core.annotations.UsedService
    private final transient LoggingServices loggingServices = null;

    @net.elcado.jCF.core.annotations.UsedService
    private final transient DependenciesResolverServices dependenciesResolverServices = null;

    @net.elcado.jCF.core.annotations.UsedService
    private final transient ParametersManagerService parametersManagerService = null;

    @net.elcado.jCF.core.annotations.UsedService
    private final transient InstanceStateManagerServices instanceStateManagerServices = null;

    @net.elcado.jCF.core.annotations.UsedService
    private final transient RemoteManagerServices remoteManagerServices = null;

    @net.elcado.jCF.core.annotations.EventsSource
    public final transient Source<InstancesManagerEvent> instancesManagerEventsSource = null;

    private InstancesManager() {
//begin of modifiable zone(JavaSuper).....C/00cc2616-a6b1-435b-9511-beb9f6bf8687

//end of modifiable zone(JavaSuper).......E/00cc2616-a6b1-435b-9511-beb9f6bf8687
//begin of modifiable zone(JavaCode)......C/00cc2616-a6b1-435b-9511-beb9f6bf8687

//end of modifiable zone(JavaCode)........E/00cc2616-a6b1-435b-9511-beb9f6bf8687
    }

    public Tree<InstanceId,Instance> getInstancesTree() {
//begin of modifiable zone(JavaCode)......C/da24fe76-5a21-49c8-9d7e-86a119edab60

//end of modifiable zone(JavaCode)........E/da24fe76-5a21-49c8-9d7e-86a119edab60
//begin of modifiable zone................T/b835de7c-1225-480e-b628-e8c2ef06b905
        return this.instancesTree;
//end of modifiable zone..................E/b835de7c-1225-480e-b628-e8c2ef06b905
    }

    /**
     * Deploy an existing instance of a registered component.
     * @param parentId The instance id of the parent of the deployed instance.
     * @param component The component to deploy.
     * @param object An existing instance of the component.
     * @return the instance id of the deployed instance. Null if the component is not registered or if the component is an alreday deployed singleton.
     */
    @SuppressWarnings("rawtypes")
    public InstanceId deployInstance(final InstanceId parentId, Component component, Object object) {
//begin of modifiable zone................T/7281814a-0a48-4ab8-960b-a361ef6bd407
        InstanceId instanceId = null;
        
        // check deployment rules before doing anything
        if (this.checkDeploymentRules(parentId, component)) {
            // build component instance's object if null provided
            if (object == null) {
                object = this.buildComponentInstanceObject(component);
            }
        
            // deploy component instance's object
            if (object != null) {
                if (this.loggingServices != null) {
                    this.loggingServices.debug("Deploying object " + object + " of " + component.getClazz() + (parentId == null ? " as a singleton" : " under " + parentId));
                }
        
                // build instance
                Instance instance = this.buildInstance(component, object);
                instanceId = instance.getInstanceId();
        
                // deploy instance
                this.deployInstance(parentId, component, instance);
            } else {
                if (this.loggingServices != null) {
                    this.loggingServices.error("Cannot build object instance of " + component.getClazz());
                }
            }
        }
//end of modifiable zone..................E/7281814a-0a48-4ab8-960b-a361ef6bd407
//begin of modifiable zone................T/136579da-3418-4d70-990b-db97f4487c99
        return instanceId;
//end of modifiable zone..................E/136579da-3418-4d70-990b-db97f4487c99
    }

    /**
     * Deploy an existing instance of a registered component.
     */
    public void deployRemoteInstance(final Instance instance) {
//begin of modifiable zone................T/c090d592-2d7f-4971-b0a4-c51e0423441f
        if (this.loggingServices != null) {
            this.loggingServices.debug("Deploying remote instance '" + instance.getInstanceId() + "'  as a singleton");
        }
        
        // deploy remote instance as root
        this.deployInstance(null, instance.getComponent(), instance);
//end of modifiable zone..................E/c090d592-2d7f-4971-b0a4-c51e0423441f
    }

    /**
     * Recursively initializes supplied instance.
     * @param instanceId The instance id of the instance to remove.
     * @return true if instance has been initialized, false otherwise
     */
    public boolean initInstance(final InstanceId instanceId) {
//begin of modifiable zone................T/41c1efa9-1bc2-48a2-8470-988953c3d304
        Tree<InstanceId, Instance> instancesTree = this.instancesTree;
        
        // first init children
        List<InstanceId> childrenIds = instancesTree.getChildren(instanceId);
        boolean isChildrenTransitionsOK = true;
        for (InstanceId childId : childrenIds) {
            isChildrenTransitionsOK &= this.initInstance(childId);
        }
        
        boolean isTransitionOK = false;
        if (isChildrenTransitionsOK) {
            Instance instance = instancesTree.getRef(instanceId);
        
            // initialize instance
            isTransitionOK = this.instanceStateManagerServices.initInstance(instance);
        
            // notify
            if (isTransitionOK && this.instancesManagerEventsSource != null) {
                InstancesManagerEvent instancesManagerEvent = new InstancesManagerEvent(InstancesManagerEvent.INSTANCE_INITIALIZED, instanceId);
                this.instancesManagerEventsSource.postEvent(instancesManagerEvent);
            }
        } else {
            if (this.loggingServices != null) {
                this.loggingServices.debug("Cannot initialize " + instanceId + " because one of its children cannot initialize.");
            }
        }
//end of modifiable zone..................E/41c1efa9-1bc2-48a2-8470-988953c3d304
//begin of modifiable zone................T/2889fb33-ad3d-4583-abcb-0bd090326b46
        return isTransitionOK;
//end of modifiable zone..................E/2889fb33-ad3d-4583-abcb-0bd090326b46
    }

    /**
     * Starts all instances that implements {@link net.elcado.jCF.core.lang.component.Startable Startable}.
     * @return true if all instances have been started, false otherwise
     */
    public boolean startInstances() {
//begin of modifiable zone................T/c31fbe4c-e01c-47fd-a65f-24ea419fe20e
        boolean areTransitionsOK = true;
        
        // start roots
        for (InstanceId instanceId : this.instancesTree.getRoots()) {
            areTransitionsOK &= this.startInstance(instanceId);
        }
//end of modifiable zone..................E/c31fbe4c-e01c-47fd-a65f-24ea419fe20e
//begin of modifiable zone................T/230c4b49-76f2-4316-932f-f115cbbf7da9
        return areTransitionsOK;
//end of modifiable zone..................E/230c4b49-76f2-4316-932f-f115cbbf7da9
    }

    /**
     * Starts supplied instance if it is deployed and it implements {@link net.elcado.jCF.core.lang.component.Startable Startable}. Do nothing otherwise.
     * @param instanceId The instance id of the instance to remove.
     * @return true if instance has been started, false otherwise
     */
    public boolean startInstance(final InstanceId instanceId) {
//begin of modifiable zone................T/1cbb7e51-7bf3-4fa2-b907-27eae289dcf4
        Tree<InstanceId, Instance> instancesTree = this.instancesTree;
        
        // first start children
        List<InstanceId> childrenIds = instancesTree.getChildren(instanceId);
        boolean isChildrenTransitionsOK = true;
        for (InstanceId childId : childrenIds) {
            isChildrenTransitionsOK &= this.startInstance(childId);
        }
        
        boolean isTransitionOK = false;
        if (isChildrenTransitionsOK) {
            Instance instance = instancesTree.getRef(instanceId);
        
            // do nothing if instance is already started
            if (this.instanceStateManagerServices.isStarted(instance)) {
                isTransitionOK = true;
            }
            else {
                // start sinks and remote services
                this.startSinksAndRemoteServices(instance);
        
                // start instance
                isTransitionOK = this.instanceStateManagerServices.startInstance(instance);
                if (isTransitionOK) {
                    if (instance.getComponent().isRemotable()) {
                        this.remoteManagerServices.publishRemoteImplementedServices(instance);
                    }
        
                    // notify
                    if (this.instancesManagerEventsSource != null) {
                        InstancesManagerEvent instancesManagerEvent = new InstancesManagerEvent(InstancesManagerEvent.INSTANCE_STARTED, instanceId);
                        this.instancesManagerEventsSource.postEvent(instancesManagerEvent);
                    }
                }
            }
        } else {
            if (this.loggingServices != null) {
                this.loggingServices.debug("Cannot start " + instanceId + " because one of its children cannot start.");
            }
        }
//end of modifiable zone..................E/1cbb7e51-7bf3-4fa2-b907-27eae289dcf4
//begin of modifiable zone................T/738b63c6-a8b0-4c97-b233-b288dae4ca66
        return isTransitionOK;
//end of modifiable zone..................E/738b63c6-a8b0-4c97-b233-b288dae4ca66
    }

    /**
     * Pauses all deployed instances that implements {@link net.elcado.jCF.core.lang.component.Startable Startable}.
     * @return true if all instances have been paused, false otherwise
     */
    public boolean pauseInstances() {
//begin of modifiable zone................T/a2b95eaa-3b2b-4372-b6dd-a706b7ac0f75
        boolean areTransitionsOK = true;
        
        // pause roots
        for (InstanceId instanceId : this.instancesTree.getRoots()) {
            areTransitionsOK &= this.pauseInstance(instanceId);
        }
//end of modifiable zone..................E/a2b95eaa-3b2b-4372-b6dd-a706b7ac0f75
//begin of modifiable zone................T/cda472c6-e442-4e12-b1c8-9a162008a8b4
        return areTransitionsOK;
//end of modifiable zone..................E/cda472c6-e442-4e12-b1c8-9a162008a8b4
    }

    /**
     * Pauses supplied instance if it is deployed and it implements {@link net.elcado.jCF.core.lang.component.Startable Startable}. Do nothing otherwise.
     * @param instanceId The instance id of the instance to remove.
     * @return true if all instances have been paused, false otherwise
     */
    public boolean pauseInstance(final InstanceId instanceId) {
//begin of modifiable zone................T/528695b4-e1bb-404a-af28-83d719b450fe
        Tree<InstanceId, Instance> instancesTree = this.instancesTree;
        
        // first pause children
        List<InstanceId> childrenIds = instancesTree.getChildren(instanceId);
        boolean isChildrenTransitionsOK = true;
        for (InstanceId childId : childrenIds) {
            isChildrenTransitionsOK &= this.pauseInstance(childId);
        }
        
        boolean isTransitionOK = false;
        if (isChildrenTransitionsOK) {
            Instance instance = instancesTree.getRef(instanceId);
        
            // do nothing if instance is already paused
            if (this.instanceStateManagerServices.isPaused(instance)) {
                isTransitionOK = true;
            }
            else {
                // pause instance
                isTransitionOK = this.instanceStateManagerServices.pauseInstance(instance);
        
                // notify
                if (isTransitionOK && this.instancesManagerEventsSource != null) {
                    InstancesManagerEvent instancesManagerEvent = new InstancesManagerEvent(InstancesManagerEvent.INSTANCE_PAUSED, instanceId);
                    this.instancesManagerEventsSource.postEvent(instancesManagerEvent);
                }
            }
        } else {
            if (this.loggingServices != null) {
                this.loggingServices.debug("Cannot pause " + instanceId + " because one of its children cannot pause.");
            }
        }
//end of modifiable zone..................E/528695b4-e1bb-404a-af28-83d719b450fe
//begin of modifiable zone................T/25e84834-16b1-47f6-bcdb-25fa34375e65
        return isTransitionOK;
//end of modifiable zone..................E/25e84834-16b1-47f6-bcdb-25fa34375e65
    }

    /**
     * Resumes all deployed instances that implements {@link net.elcado.jCF.core.lang.component.Startable Startable}.
     * @return true if all instances have been paused, false otherwise
     */
    public boolean resumeInstances() {
//begin of modifiable zone................T/43d0c71d-8e22-447b-9f49-ba70ed4d2778
        boolean areTransitionsOK = true;
        
        // resume roots
        for (InstanceId instanceId : this.instancesTree.getRoots()) {
            areTransitionsOK &= this.resumeInstance(instanceId);
        }
//end of modifiable zone..................E/43d0c71d-8e22-447b-9f49-ba70ed4d2778
//begin of modifiable zone................T/5f0390e3-f6c2-4aa4-94de-523e7fe9f6a0
        return areTransitionsOK;
//end of modifiable zone..................E/5f0390e3-f6c2-4aa4-94de-523e7fe9f6a0
    }

    /**
     * Resumes supplied instance if it is deployed and it implements {@link net.elcado.jCF.core.lang.component.Startable Startable}. Do nothing otherwise.
     * @param instanceId The instance id of the instance to remove.
     * @return true if all instances have been paused, false otherwise
     */
    public boolean resumeInstance(final InstanceId instanceId) {
//begin of modifiable zone................T/97e78381-c596-4d47-939e-583c045fdcbd
        Tree<InstanceId, Instance> instancesTree = this.instancesTree;
        
        // first resume children
        List<InstanceId> childrenIds = instancesTree.getChildren(instanceId);
        boolean isChildrenTransitionsOK = true;
        for (InstanceId childId : childrenIds) {
            isChildrenTransitionsOK &= this.resumeInstance(childId);
        }
        
        boolean isTransitionOK = false;
        if (isChildrenTransitionsOK) {
            Instance instance = instancesTree.getRef(instanceId);
        
            // do nothing if instance is already resumed
            if (this.instanceStateManagerServices.isResumed(instance)) {
                isTransitionOK = true;
            }
            else {
                // resume instance
                isTransitionOK = this.instanceStateManagerServices.resumeInstance(instance);
        
                // notify
                if (isTransitionOK && this.instancesManagerEventsSource != null) {
                    InstancesManagerEvent instancesManagerEvent = new InstancesManagerEvent(InstancesManagerEvent.INSTANCE_RESUMED, instanceId);
                    this.instancesManagerEventsSource.postEvent(instancesManagerEvent);
                }
            }
        } else {
            if (this.loggingServices != null) {
                this.loggingServices.debug("Cannot resume " + instanceId + " because one of its children cannot resume.");
            }
        }
//end of modifiable zone..................E/97e78381-c596-4d47-939e-583c045fdcbd
//begin of modifiable zone................T/12efd8ee-8d7e-4e03-b2e8-1cc89e1613af
        return isTransitionOK;
//end of modifiable zone..................E/12efd8ee-8d7e-4e03-b2e8-1cc89e1613af
    }

    /**
     * Stops all deployed instances that implements {@link net.elcado.jCF.core.lang.component.Startable Startable}.
     * @return true if all instances have been paused, false otherwise
     */
    public boolean stopInstances() {
//begin of modifiable zone................T/e7c06bef-e7a0-467c-a2ea-a6203321f442
        boolean areTransitionsOK = true;
        
        // stop roots
        for (InstanceId instanceId : this.instancesTree.getRoots()) {
            areTransitionsOK &= this.stopInstance(instanceId);
        }
//end of modifiable zone..................E/e7c06bef-e7a0-467c-a2ea-a6203321f442
//begin of modifiable zone................T/d1106f47-3e14-42de-8d39-6fe9e1395ea4
        return areTransitionsOK;
//end of modifiable zone..................E/d1106f47-3e14-42de-8d39-6fe9e1395ea4
    }

    /**
     * Stops supplied instance if it is deployed and it implements {@link net.elcado.jCF.core.lang.component.Startable Startable}. Do nothing otherwise.
     * @param instanceId The instance id of the instance to remove.
     * @return true if all instances have been paused, false otherwise
     */
    public boolean stopInstance(final InstanceId instanceId) {
//begin of modifiable zone................T/90156111-e4bd-40f5-a14e-f7ddaa3b4acb
        Tree<InstanceId, Instance> instancesTree = this.instancesTree;
        
        // first stop children
        List<InstanceId> childrenIds = instancesTree.getChildren(instanceId);
        boolean isChildrenTransitionsOK = true;
        for (InstanceId childId : childrenIds) {
            isChildrenTransitionsOK &= this.stopInstance(childId);
        }
        
        boolean isTransitionOK = false;
        if (isChildrenTransitionsOK) {
            Instance instance = instancesTree.getRef(instanceId);
        
            // do nothing if instance is already stopped
            if (this.instanceStateManagerServices.isStopped(instance)) {
                isTransitionOK = true;
            }
            else {
                if (instance.getComponent().isRemotable()) {
                    this.remoteManagerServices.unpublishRemoteImplementedServices(instance);
                }
        
                // stop sinks and remote services
                this.stopSinksAndRemoteServices(instance);
        
                // stop instance
                isTransitionOK = this.instanceStateManagerServices.stopInstance(instance);
        
                // notify
                if (isTransitionOK && this.instancesManagerEventsSource != null) {
                    InstancesManagerEvent instancesManagerEvent = new InstancesManagerEvent(InstancesManagerEvent.INSTANCE_STOPPED, instanceId);
                    this.instancesManagerEventsSource.postEvent(instancesManagerEvent);
                }
            }
        } else {
            if (this.loggingServices != null) {
                this.loggingServices.debug("Cannot stop " + instanceId + " because one of its children cannot stop.");
            }
        }
//end of modifiable zone..................E/90156111-e4bd-40f5-a14e-f7ddaa3b4acb
//begin of modifiable zone................T/85fda824-33cd-4302-9d1f-2269ac5906ba
        return isTransitionOK;
//end of modifiable zone..................E/85fda824-33cd-4302-9d1f-2269ac5906ba
    }

    /**
     * Undeploy an instance.
     * @param instanceId The instance id of the instance to remove.
     * @return the list of undeployed instances' ids
     */
    public List<InstanceId> undeployInstance(final InstanceId instanceId) {
//begin of modifiable zone................T/bb008808-5cfd-40a0-a218-5704a9df561a
        // recursively remove instances
        List<InstanceId> removedIds = new ArrayList<>();
        
        this.removeInstance(instanceId, removedIds);
//end of modifiable zone..................E/bb008808-5cfd-40a0-a218-5704a9df561a
//begin of modifiable zone................T/afa5365c-90f1-436c-8373-781cfc58496f
        return removedIds;
//end of modifiable zone..................E/afa5365c-90f1-436c-8373-781cfc58496f
    }

    /**
     * Undeploy an instance.
     * @param instance The instance id of the instance to remove.
     */
    public void undeployRemoteInstance(final Instance instance) {
//begin of modifiable zone................T/632f1a48-e0b5-40ff-8982-c6cc3e792c3c
        if (this.loggingServices != null) {
            this.loggingServices.debug("Undeploying remote instance '" + instance.getInstanceId() + "'");
        }
        
        // undeploy remote instance
        this.removeInstance(instance.getInstanceId(), new ArrayList<>());
//end of modifiable zone..................E/632f1a48-e0b5-40ff-8982-c6cc3e792c3c
    }

    private Object buildComponentInstanceObject(Component component) {
//begin of modifiable zone................T/8aea156c-afa3-4052-8cda-a5ee167ad1ff
        Object object;
        
        if (this.loggingServices != null) {
            this.loggingServices.debug("Building object of " + component.getClazz());
        }
        
        // build
        object = ReflectionUtils.getReflectionUtils().buildObject(component.getClazz());
//end of modifiable zone..................E/8aea156c-afa3-4052-8cda-a5ee167ad1ff
//begin of modifiable zone................T/7efdbe43-7946-4fae-aae3-4f88a44f34f4
        return object;
//end of modifiable zone..................E/7efdbe43-7946-4fae-aae3-4f88a44f34f4
    }

    /**
     * @param parentId The instance id of the parent of the deployed instance.
     * @param component The component to deploy.
     */
    private boolean checkDeploymentRules(final InstanceId parentId, Component component) {
//begin of modifiable zone................T/758a41e0-3467-4984-bb17-a58e2334d873
        boolean canBeDeployed;
        
        if (parentId == null) {
            // 1/ to be deployed as a singleton, it has not to be already deployed
            canBeDeployed = component.getInstances().isEmpty();
        
            // error ?
            if (!canBeDeployed && this.loggingServices != null) {
                this.loggingServices.error(component.getClazz() + " can not be deployed as a singleton because it is already deployed.");
            }
        } else {
            // 2/ to be deployed as a regular instance, it has to be deployed only as regular instances (or not to be deployed at all)
            canBeDeployed = component.getInstances().stream().noneMatch(instance -> instance.getInstanceId() == null);
        
            // error ?
            if (!canBeDeployed && this.loggingServices != null) {
                this.loggingServices.error(component.getClazz() + " can not be deployed because it is already deployed as a singleton.");
            }
        }
//end of modifiable zone..................E/758a41e0-3467-4984-bb17-a58e2334d873
//begin of modifiable zone................T/3358bb25-faa3-456f-93c3-d2b029c0db81
        return canBeDeployed;
//end of modifiable zone..................E/3358bb25-faa3-456f-93c3-d2b029c0db81
    }

    private Instance buildInstance(final Component component, final Object object) {
//begin of modifiable zone................T/e02c4713-0618-4eb8-b7a0-46bb6c8197ec
        if (this.loggingServices != null) {
            this.loggingServices.debug("Building instance for object " + object);
        }
        
        Instance instance = new Instance(component, object);
        
        // inject the id field
        if (component.getIdPort() != null) {
            ReflectionUtils.getReflectionUtils().setFieldValue(
                    instance.getObject(),
                    component.getIdPort().getName(),
                    instance.getInstanceId());
        }
        
        // set the implemented services
        for (ImplementedServicePort implementedServicePort : component.getImplementedServicePorts()) {
            ImplementedService implementedService = new ImplementedService(instance, implementedServicePort);
            instance.addImplementedService(implementedService);
        }
        
        // set used services
        for (UsedServicePort usedServicePort : component.getUsedServicePorts()) {
            UsedService usedService = new UsedService(instance, usedServicePort);
            instance.addUsedService(usedService);
        }
        
        // set events sources
        for (EventsSourcePort eventsSourcePort : component.getEventsSourcePorts()) {
            EventsSource eventsSource = new EventsSource(instance, eventsSourcePort);
        
            // inject source dispatcher
            SourceDispatcher<Object> sourceDispatcher = new SourceDispatcher<>();
            eventsSource.setObject(sourceDispatcher);
        
            instance.addEventsSource(eventsSource);
        }
        
        // set events sinks
        for (EventsSinkPort eventsSinkPort : component.getEventsSinkPorts()) {
            EventsSink eventsSink = new EventsSink(instance, eventsSinkPort);
        
            // inject sink queue
            SinkQueue<?> sinkQueue = new SinkQueue<>((Sink<?>) eventsSink.getObject());
            eventsSink.setObject(sinkQueue);
        
            instance.addEventsSink(eventsSink);
        }
        
        // inject parameters
        for (ParametersPort parametersPort : component.getParametersPorts()) {
            Parameters<?> parameters = this.parametersManagerService.newParameters(instance, parametersPort);
            ReflectionUtils.getReflectionUtils().setFieldValue(
                    instance.getObject(),
                    parametersPort.getName(),
                    parameters);
        }
        
        // build RpcDispatchers
        if (instance.getComponent().isRemotable()) {
            this.remoteManagerServices.buildRemoteImplementedServices(instance);
        }
//end of modifiable zone..................E/e02c4713-0618-4eb8-b7a0-46bb6c8197ec
//begin of modifiable zone................T/3afcd469-c63d-4ee8-9f45-9ebc83d6b740
        return instance;
//end of modifiable zone..................E/3afcd469-c63d-4ee8-9f45-9ebc83d6b740
    }

    private void deployInstance(InstanceId parentId, Component component, Instance instance) {
//begin of modifiable zone................T/2ddccfb9-40fe-4427-b616-7706682ee9cc
        // deploy instance
        this.putInstanceInTree(parentId, instance);
        
        // recursively deploy composed components' instances
        if (component.getComposedComponents() != null) {
            component.getComposedComponents().forEach(composedComponent -> this.deployInstance(instance.getInstanceId(), composedComponent, null));
        }
        
        // resolve dependencies
        this.resolveDependencies(instance);
        
        // log
        if (this.loggingServices != null) {
            this.loggingServices.debug("Deployed instance " + instance.getInstanceId() + " [" + component.getClazz() + "] " + (parentId == null ? " as root in tree" : " under " + parentId + " in tree"));
        }
//end of modifiable zone..................E/2ddccfb9-40fe-4427-b616-7706682ee9cc
    }

    private void putInstanceInTree(final InstanceId parentId, final Instance instance) {
//begin of modifiable zone................T/ca8b4a8d-3845-4bac-ae81-d75c3ae2286d
        // put instance in tree
        if (this.loggingServices != null) {
            this.loggingServices.info("Put instance " + instance.getInstanceId() + " [" + instance.getComponent().getClazz() + "]" + (parentId == null ? " as root in tree" : " under " + parentId + " in tree"));
        }
        
        if (parentId == null) {
            this.instancesTree.addRoot(instance.getInstanceId(), instance);
        } else {
            this.instancesTree.addElement(parentId, instance.getInstanceId(), instance);
        }
//end of modifiable zone..................E/ca8b4a8d-3845-4bac-ae81-d75c3ae2286d
    }

    private void resolveDependencies(final Instance instance) {
//begin of modifiable zone................T/57d49383-44c9-4596-8801-8e9bdd23a6f0
        // TODO being able to only resolve dependencies on supplied instance
        if (this.loggingServices != null) {
            this.loggingServices.debug("Resolving dependencies for whole instance's tree");
        }
        
        this.dependenciesResolverServices.resolveDependencies(this.instancesTree);
//end of modifiable zone..................E/57d49383-44c9-4596-8801-8e9bdd23a6f0
    }

    @SuppressWarnings("rawtypes")
    private void removeInstance(final InstanceId instanceId, final List<InstanceId> removedIds) {
//begin of modifiable zone................T/841cb103-72cb-44e1-a653-8b4c3d1a1484
        // first check that instance is still in the tree (it might have been removed as a child of another instance)
        if (this.instancesTree.contains(instanceId)) {
            Instance instance = this.instancesTree.getRef(instanceId);
        
            // remove children first
            List<InstanceId> childrenIds = this.instancesTree.getChildren(instanceId);
            if (this.loggingServices != null) {
                this.loggingServices.debug("Removing instance [" + instanceId + "] w/ " + (childrenIds.size() != 0 ? "children " + childrenIds : "no children"));
            }
        
            for (InstanceId childId : childrenIds.toArray(new InstanceId[]{})) {
                this.removeInstance(childId, removedIds);
            }
        
            // remove dependencies for instance
            this.dependenciesResolverServices.removeDependencies(instanceId, this.instancesTree);
        
            // remove instance from the tree
            Instance removedInstance = this.instancesTree.removeElement(instanceId);
        
            // remove instance from component (nothing to do for remote instances)
            if (!removedInstance.isRemote()) {
                removedInstance.getComponent().getInstances().remove(removedInstance);
            }
        
            // add instance id to removed ids list
            removedIds.add(instanceId);
        
            // dispose instance (nothing to do for remote instances)
            boolean isTransitionOK = true;
            if (!removedInstance.isRemote()) {
                isTransitionOK = this.instanceStateManagerServices.disposeInstance(removedInstance);
            }
        
            if (isTransitionOK) {
                // log and notify
                if (this.loggingServices != null) {
                    this.loggingServices.debug("Instance [" + instanceId + "] has been undeployed");
                }
                if (this.instancesManagerEventsSource != null) {
                    InstancesManagerEvent instancesManagerEvent = new InstancesManagerEvent(InstancesManagerEvent.INSTANCE_DISPOSED, instanceId);
                    this.instancesManagerEventsSource.postEvent(instancesManagerEvent);
                }
            }
        }
//end of modifiable zone..................E/841cb103-72cb-44e1-a653-8b4c3d1a1484
    }

    private void startSinksAndRemoteServices(final Instance instance) {
//begin of modifiable zone................T/55f9906b-1600-46ca-bace-651cd32c5b7a
        // start all instance's sinks
        for (EventsSink eventsSink : instance.getEventsSinks()) {
            ((SinkQueue) eventsSink.getObject()).startProcessingEvents();
        }
        
        // publish remote implemented services into registry
        if (instance.getComponent().isRemotable()) {
            this.remoteManagerServices.startRemoteImplementedServices(instance);
        }
//end of modifiable zone..................E/55f9906b-1600-46ca-bace-651cd32c5b7a
    }

    private void stopSinksAndRemoteServices(final Instance instance) {
//begin of modifiable zone................T/39df4a17-9490-4dda-8130-afc9ea522f49
        // stop all instance's sinks
        for (EventsSink eventsSink : instance.getEventsSinks()) {
            ((SinkQueue) eventsSink.getObject()).stopProcessingEvents();
        }
        
        // unregister remote implemented services
        this.remoteManagerServices.stopRemoteImplementedServices(instance);
//end of modifiable zone..................E/39df4a17-9490-4dda-8130-afc9ea522f49
    }

}
