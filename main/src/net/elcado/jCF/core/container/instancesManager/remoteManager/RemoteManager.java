/*
 * Copyright (c). Frédéric Cadier (2014-2018)
 *
 * This software is a computer program whose purpose is to allow component programming in pure java.
 *
 * This software is governed by the CeCILL-C license under French law and abiding by the rules of distribution of free software. You can use, modify and/or redistribute the software under the terms of the CeCILL-C license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy, modify and redistribute granted by the license, users are provided only with a limited warranty and the software's author, the holder of the economic rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, that may mean that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured and, more generally, to use and operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had knowledge of the CeCILL-C license and that you accept its terms.
 */

package net.elcado.jCF.core.container.instancesManager.remoteManager;

import java.lang.reflect.Proxy;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;
import net.elcado.jCF.core.annotations.Component;
import net.elcado.jCF.core.annotations.UsedService;
import net.elcado.jCF.core.container.instancesManager.InstancesManagerRemoteServices;
import net.elcado.jCF.core.container.instancesManager.remoteManager.remoteRegistry.RemoteRegistryServices;
import net.elcado.jCF.core.lang.instance.Instance;
import net.elcado.jCF.core.lang.instance.services.ImplementedService;
import net.elcado.jCF.log.LoggingServices;
import org.jgroups.Address;
import org.jgroups.JChannel;
import org.jgroups.blocks.MethodCall;
import org.jgroups.blocks.RequestOptions;
import org.jgroups.blocks.RpcDispatcher;
import org.jgroups.util.UUID;
import org.jgroups.util.Util;

@Component
public class RemoteManager implements RemoteManagerServices, RemoteManagerRegistryServices {
    @net.elcado.jCF.core.annotations.ImplementedService
    public final transient RemoteManagerServices remoteManagerServices = this;

    @net.elcado.jCF.core.annotations.ImplementedService
    private final transient RemoteManagerRegistryServices remoteManagerRegistryServices = this;

    @UsedService
    private final transient LoggingServices loggingServices = null;

    @UsedService
    private final transient RemoteRegistryServices remoteRegistryServices = null;

    @UsedService
    private final transient InstancesManagerRemoteServices instancesManagerRemoteServices = null;

    private final Map<ImplementedService, RpcDispatcher> servers = new HashMap<>();

    private final Map<ImplementedService, RpcDispatcher> clients = new HashMap<>();

    private RemoteManager() {
//begin of modifiable zone(JavaSuper).....C/e514df2e-b382-4d6d-9d64-ff966a000a26

//end of modifiable zone(JavaSuper).......E/e514df2e-b382-4d6d-9d64-ff966a000a26
//begin of modifiable zone(JavaCode)......C/e514df2e-b382-4d6d-9d64-ff966a000a26

//end of modifiable zone(JavaCode)........E/e514df2e-b382-4d6d-9d64-ff966a000a26
    }

    public void buildRemoteImplementedServices(final Instance instance) {
//begin of modifiable zone................T/4f901720-9bea-42eb-b22a-6869a01ba2e1
        instance.getImplementedServices().stream()
                // for all remotable implemented service
                .filter(ImplementedService::isRemotable)
                // build RpcDispatcher that will act as a server for remote calls
                .forEach(implementedService -> {
                    try {
                        JChannel rpcServicesChannel = new JChannel("udp.xml");
                        RpcDispatcher rpcServicesDispatcher = new RpcDispatcher(rpcServicesChannel, implementedService.getObject());
        
                        // add to servers
                        this.servers.put(implementedService, rpcServicesDispatcher);
                    } catch (Exception channelException) {
                        this.loggingServices.error("Cannot build channel for implemented service '" + implementedService + "' due to " + channelException.getMessage());
                    }
        });
//end of modifiable zone..................E/4f901720-9bea-42eb-b22a-6869a01ba2e1
    }

    public void startRemoteImplementedServices(final Instance instance) {
//begin of modifiable zone................T/14320ca3-2150-4351-9725-edb671378574
        instance.getImplementedServices().stream()
                // for all remotable implemented service
                .filter(ImplementedService::isRemotable)
                // connect channel to cluster
                .forEach(implementedService -> {
                    try {
                        JChannel rpcServicesChannel = this.servers.get(implementedService).getChannel();
                        rpcServicesChannel.connect(implementedService.getClazz().getCanonicalName());
        
                        // set remote implemented service address
                        Address address = rpcServicesChannel.getAddress();
                        implementedService.setAddress(((UUID) address).toStringLong());
                    } catch (Exception channelException) {
                        this.loggingServices.error("Cannot connect channel for implemented service '" + implementedService + "' due to " + channelException.getMessage());
                    }
                });
//end of modifiable zone..................E/14320ca3-2150-4351-9725-edb671378574
    }

    public void publishRemoteImplementedServices(final Instance instance) {
//begin of modifiable zone................T/0dca5be1-7f5a-42a8-83e1-d07a7fe1051b
        this.remoteRegistryServices.addInstance(instance);
//end of modifiable zone..................E/0dca5be1-7f5a-42a8-83e1-d07a7fe1051b
    }

    public void stopRemoteImplementedServices(final Instance instance) {
//begin of modifiable zone................T/85aaba4f-e7a4-427a-a016-59bf2232bf8a
        instance.getImplementedServices().stream()
                // for all remotable implemented service
                .filter(ImplementedService::isRemotable)
                // connect channel to cluster
                .forEach(implementedService -> {
                    try {
                        RpcDispatcher rpcServicesDispatcher = this.servers.remove(implementedService);
                        JChannel channel = rpcServicesDispatcher.getChannel();
                        if (rpcServicesDispatcher != null) {
                            rpcServicesDispatcher.stop();
                        }
                        Util.close(channel);
                    } catch (Exception channelException) {
                        this.loggingServices.error("Cannot connect channel for implemented service '" + implementedService + "' due to " + channelException.getMessage());
                    }
                });
//end of modifiable zone..................E/85aaba4f-e7a4-427a-a016-59bf2232bf8a
    }

    public void unpublishRemoteImplementedServices(final Instance instance) {
//begin of modifiable zone................T/d3ab0ea4-3699-44f1-848a-c68b0d2b609f
        this.remoteRegistryServices.removeInstance(instance);
//end of modifiable zone..................E/d3ab0ea4-3699-44f1-848a-c68b0d2b609f
    }

    public void injectRPCDispatcher(final Instance instance) {
//begin of modifiable zone................T/c09fd410-fdd1-49a1-9dd6-a10f504e80fd
        // inject RPC dispatchers into implemented services of instance
        instance.getImplementedServices().stream()
                // for all remotable implemented service
                .filter(ImplementedService::isRemotable)
                .forEach(implementedService -> {
                    UUID address = UUID.fromString(implementedService.getAddress());
                    Class<?> serviceClazz = implementedService.getClazz();
        
                    // inject RPC into implemented service
                    try {
                        // build RPC channel
                        JChannel rpcServicesChannel = new JChannel("udp.xml");
        
                        // build RpcDispatcher that will act as a client for remote calls
                        RpcDispatcher rpcServicesDispatcher = new RpcDispatcher(rpcServicesChannel, implementedService.getObject());
                        clients.put(implementedService, rpcServicesDispatcher);
        
                        //System.err.println("########## CONNECT '" + rpcServicesChannel + "'\n");
        
                        // connect to channel
                        rpcServicesChannel.connect(implementedService.getClazz().getCanonicalName());
        
                        //System.err.println("########## CONNECTED");
        
                        // proxy for invocation
                        Object rpcImplementedService = Proxy.newProxyInstance(
                                this.getClass().getClassLoader(),
                                new Class[]{serviceClazz},
                                (proxy, method, args) -> {
                                    MethodCall methodCall = new MethodCall(method, args);
                                    return rpcServicesDispatcher.callRemoteMethod(address, methodCall, RequestOptions.SYNC());
                                }
                        );
        
                        implementedService.setObject(rpcImplementedService);
                    } catch (Exception channelException) {
                        loggingServices.error("Cannot build or connect channel for implemented service '" + implementedService + "' due to " + channelException.getMessage());
                    }
                });
        
        // deploy remote instance
        this.instancesManagerRemoteServices.deployRemoteInstance(instance);
//end of modifiable zone..................E/c09fd410-fdd1-49a1-9dd6-a10f504e80fd
    }

    public void closeRPCDispatcher(final Instance instance) {
//begin of modifiable zone................T/0bcc3d7d-633f-4a72-9f6c-b5673236d919
        // undeploy remote instance
        this.instancesManagerRemoteServices.undeployRemoteInstance(instance);
        
        // close RPC dispatchers of implemented services of instance
        instance.getImplementedServices().stream()
                // for all remotable implemented service
                .filter(ImplementedService::isRemotable)
                .forEach(implementedService -> {
                    // cannot browse clients with implemented service directly because state replication has cloned it
                    AtomicReference<ImplementedService> toBeRemoved = new AtomicReference<>();
        
                    UUID address = UUID.fromString(implementedService.getAddress());
                    Class<?> serviceClazz = implementedService.getClazz();
                    clients.forEach((is, rpcServicesDispatcher) -> {
                        if (UUID.fromString(is.getAddress()).equals(address) && is.getClazz().equals(serviceClazz)) {
                            // disconnect channel from cluster
                            JChannel rpcChannel = rpcServicesDispatcher.getChannel();
                            if (rpcServicesDispatcher != null) {
                                rpcServicesDispatcher.stop();
                            }
                            Util.close(rpcChannel);
        
                            toBeRemoved.set(is);
                        }
                    });
        
                    if (toBeRemoved.get() != null) {
                        // remove rpcServicesDispatcher from list of clients
                        clients.remove(toBeRemoved.get());
                    }
                });
//end of modifiable zone..................E/0bcc3d7d-633f-4a72-9f6c-b5673236d919
    }

}
