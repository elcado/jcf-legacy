/*
 * Copyright (c). Frédéric Cadier (2014-2018)
 *
 * This software is a computer program whose purpose is to allow component programming in pure java.
 *
 * This software is governed by the CeCILL-C license under French law and abiding by the rules of distribution of free software. You can use, modify and/or redistribute the software under the terms of the CeCILL-C license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy, modify and redistribute granted by the license, users are provided only with a limited warranty and the software's author, the holder of the economic rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, that may mean that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured and, more generally, to use and operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had knowledge of the CeCILL-C license and that you accept its terms.
 */

package net.elcado.jCF.core.container.instancesManager.parametersManager;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import net.elcado.jCF.core.annotations.Component;
import net.elcado.jCF.core.annotations.ImplementedService;
import net.elcado.jCF.core.annotations.UsedService;
import net.elcado.jCF.core.lang.component.parameters.ParametersPort;
import net.elcado.jCF.core.lang.instance.Instance;
import net.elcado.jCF.core.lang.instance.InstanceId;
import net.elcado.jCF.core.lang.instance.parameters.Parameters;
import net.elcado.jCF.log.LoggingServices;

@Component
public class ParametersManager implements ParametersManagerService {
    @ImplementedService
    public final transient ParametersManagerService parametersManagerService = this;

    @UsedService
    private final transient LoggingServices loggingServices = null;

    private final Properties globalProperties;

    private final Map<InstanceId, Properties> instancesProperties = new HashMap<>();

    private ParametersManager() {
//begin of modifiable zone(JavaSuper).....C/68d4358d-2d90-41bd-8420-9514c03fad58

//end of modifiable zone(JavaSuper).......E/68d4358d-2d90-41bd-8420-9514c03fad58
//begin of modifiable zone................T/1fb0b552-5260-48d1-a022-0b35386b0930
        this.globalProperties = new Properties();
//end of modifiable zone..................E/1fb0b552-5260-48d1-a022-0b35386b0930
    }

    public <T> Parameters<T> newParameters(final Instance anInstance, final ParametersPort aPort) {
//begin of modifiable zone................T/6beab6a0-7ed2-4541-a097-073ce873476f
        InstanceId instanceId = anInstance.getInstanceId();
        
        // prepare instance's properties
        if (!this.instancesProperties.containsKey(instanceId)) {
            this.instancesProperties.put(anInstance.getInstanceId(), new Properties());
        }
        
        Parameters<T> parameters = new Parameters<T>() {
            @Override
            public Object get(T param) {
                return ParametersManager.this.globalProperties.get(param);
            }
        
            @Override
            public void set(T param, Object value) {
                ParametersManager.this.globalProperties.put(param, value);
            }
        
            @Override
            public Object get(InstanceId instanceId, T param) {
                return ParametersManager.this.instancesProperties.get(instanceId).get(param);
            }
        
            @Override
            public void set(InstanceId instanceId, T param, Object value) {
                ParametersManager.this.instancesProperties.get(instanceId).put(param, value);
            }
        };
//end of modifiable zone..................E/6beab6a0-7ed2-4541-a097-073ce873476f
//begin of modifiable zone................T/7ed675ab-00b4-40c4-a749-20982c7e051d
        return parameters;
//end of modifiable zone..................E/7ed675ab-00b4-40c4-a749-20982c7e051d
    }

}
