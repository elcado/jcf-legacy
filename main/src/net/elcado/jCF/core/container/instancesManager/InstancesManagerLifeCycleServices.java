/*
 * Copyright (c). Frédéric Cadier (2014-2018)
 *
 * This software is a computer program whose purpose is to allow component programming in pure java.
 *
 * This software is governed by the CeCILL-C license under French law and abiding by the rules of distribution of free software. You can use, modify and/or redistribute the software under the terms of the CeCILL-C license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy, modify and redistribute granted by the license, users are provided only with a limited warranty and the software's author, the holder of the economic rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, that may mean that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured and, more generally, to use and operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had knowledge of the CeCILL-C license and that you accept its terms.
 */

package net.elcado.jCF.core.container.instancesManager;

import net.elcado.jCF.core.lang.instance.InstanceId;

/**
 * Provides control over components' life cycles.
 * 
 * @author Frédéric Cadier
 */
public interface InstancesManagerLifeCycleServices {
    /**
     * Recursively initializes supplied instance.
     * @param instanceId The instance id of the instance to remove.
     * @return true if instance has been initialized, false otherwise
     */
    boolean initInstance(final InstanceId instanceId);

    /**
     * Starts all instances that implements {@link net.elcado.jCF.core.lang.component.Startable Startable}.
     * @return true if all instances have been started, false otherwise
     */
    boolean startInstances();

    /**
     * Starts supplied instance if it is deployed and it implements {@link net.elcado.jCF.core.lang.component.Startable Startable}. Do nothing otherwise.
     * @param instanceId The instance id of the instance to remove.
     * @return true if instance has been started, false otherwise
     */
    boolean startInstance(final InstanceId instanceId);

    /**
     * Pauses all deployed instances that implements {@link net.elcado.jCF.core.lang.component.Startable Startable}.
     * @return true if all instances have been paused, false otherwise
     */
    boolean pauseInstances();

    /**
     * Pauses supplied instance if it is deployed and it implements {@link net.elcado.jCF.core.lang.component.Startable Startable}. Do nothing otherwise.
     * @param instanceId The instance id of the instance to remove.
     * @return true if all instances have been paused, false otherwise
     */
    boolean pauseInstance(final InstanceId instanceId);

    /**
     * Resumes all deployed instances that implements {@link net.elcado.jCF.core.lang.component.Startable Startable}.
     * @return true if all instances have been paused, false otherwise
     */
    boolean resumeInstances();

    /**
     * Resumes supplied instance if it is deployed and it implements {@link net.elcado.jCF.core.lang.component.Startable Startable}. Do nothing otherwise.
     * @param instanceId The instance id of the instance to remove.
     * @return true if all instances have been paused, false otherwise
     */
    boolean resumeInstance(final InstanceId instanceId);

    /**
     * Stops all deployed instances that implements {@link net.elcado.jCF.core.lang.component.Startable Startable}.
     * @return true if all instances have been paused, false otherwise
     */
    boolean stopInstances();

    /**
     * Stops supplied instance if it is deployed and it implements {@link net.elcado.jCF.core.lang.component.Startable Startable}. Do nothing otherwise.
     * @param instanceId The instance id of the instance to remove.
     * @return true if all instances have been paused, false otherwise
     */
    boolean stopInstance(final InstanceId instanceId);

}
