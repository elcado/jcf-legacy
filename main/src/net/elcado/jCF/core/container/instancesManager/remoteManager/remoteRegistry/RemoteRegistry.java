/*
 * Copyright (c). Frédéric Cadier (2014-2018)
 *
 * This software is a computer program whose purpose is to allow component programming in pure java.
 *
 * This software is governed by the CeCILL-C license under French law and abiding by the rules of distribution of free software. You can use, modify and/or redistribute the software under the terms of the CeCILL-C license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy, modify and redistribute granted by the license, users are provided only with a limited warranty and the software's author, the holder of the economic rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, that may mean that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured and, more generally, to use and operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had knowledge of the CeCILL-C license and that you accept its terms.
 */

package net.elcado.jCF.core.container.instancesManager.remoteManager.remoteRegistry;

import java.io.BufferedOutputStream;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import net.elcado.jCF.core.annotations.Component;
import net.elcado.jCF.core.annotations.UsedService;
import net.elcado.jCF.core.container.instancesManager.remoteManager.RemoteManagerRegistryServices;
import net.elcado.jCF.core.lang.component.Startable;
import net.elcado.jCF.core.lang.instance.Instance;
import net.elcado.jCF.log.LoggingServices;
import org.jgroups.Address;
import org.jgroups.JChannel;
import org.jgroups.MembershipListener;
import org.jgroups.Message;
import org.jgroups.StateListener;
import org.jgroups.View;
import org.jgroups.blocks.MethodCall;
import org.jgroups.blocks.RequestOptions;
import org.jgroups.blocks.RpcDispatcher;
import org.jgroups.util.UUID;
import org.jgroups.util.Util;

@Component
public class RemoteRegistry implements RemoteRegistryServices, Startable {
    private static short METHOD_ID = 1;

    private static final short ADD_INSTANCE = METHOD_ID++;

    private static final short REMOVE_INSTANCE = METHOD_ID++;

    protected static final Map<Short, Method> methods;

    @net.elcado.jCF.core.annotations.ImplementedService
    public final transient RemoteRegistryServices remoteRegistryServices = this;

    @UsedService
    private final transient LoggingServices loggingServices = null;

    @UsedService
    private final transient RemoteManagerRegistryServices remoteManagerRegistryServices = null;

    private JChannel channel;

    private RpcDispatcher dispatcher = null;

    private Address ownAddress;

    private final Map<Address, List<Instance>> instances = new HashMap<>();

    public RemoteRegistry() {
//begin of modifiable zone(JavaSuper).....C/ea258f7c-0f0f-429d-ad67-97f84f0f67d6

//end of modifiable zone(JavaSuper).......E/ea258f7c-0f0f-429d-ad67-97f84f0f67d6
//begin of modifiable zone................T/d50d8080-f655-46f7-b797-2de85eecf38d
        try {
            // build registry channel
            this.channel = new JChannel("udp.xml");
        } catch (Exception channelException) {
            this.loggingServices.error("Cannot build channel for remote manager due to " + channelException.getMessage());
        }
        
        if (this.channel != null) {
            // build registry dispatcher
            this.dispatcher = new RpcDispatcher(channel, this).setMethodLookup(id -> {
                Method method = methods.get(id);
                method.setAccessible(true);
                return method;
            });
            this.dispatcher
                    .setStateListener(new RegistryStateListener())
                    .setMembershipListener(new RegistryMembershipListener());
        }
//end of modifiable zone..................E/d50d8080-f655-46f7-b797-2de85eecf38d
    }

    /**
     * Called when the instance state has been set to {@link net.elcado.jCF.core.lang.instance.InstanceState#STARTED InstanceState.STARTED}.
     * @return true if user code accepts the state change, false otherwise. In this latter case, instance state will be set to {@link net.elcado.jCF.core.lang.instance.InstanceState#FAILED InstanceState.FAILED}.
     */
    public boolean onStarted() {
//begin of modifiable zone................T/cc600768-473f-441a-a416-8fd23d040ecd
        if (this.channel != null) {
            // connect channel
            try {
                this.channel.connect(this.getClass().getSimpleName() + "-Cluster");
            } catch (Exception channelException) {
                this.loggingServices.error("Cannot connect to remote registry's channel due to " + channelException.getMessage());
                channelException.printStackTrace();
                return false;
            }
        
            // starts dispatcher
            this.dispatcher.start();
        
            // keep one's address
            this.ownAddress = this.channel.getAddress();
        
            // exchange state
            try {
                this.channel.getState(null, 0);
            } catch (Exception channelException) {
                this.loggingServices.error("Cannot exchange state due to " + channelException.getMessage());
                channelException.printStackTrace();
                return false;
            }
        }
//end of modifiable zone..................E/cc600768-473f-441a-a416-8fd23d040ecd
//begin of modifiable zone................T/200b0ea4-e508-4c25-8897-159b4b28b9be
        return true;
//end of modifiable zone..................E/200b0ea4-e508-4c25-8897-159b4b28b9be
    }

    /**
     * Called when the instance state has been set to {@link net.elcado.jCF.core.lang.instance.InstanceState#PAUSED InstanceState.PAUSED}.
     * @return true if user code accepts the state change, false otherwise. In this latter case, instance state will be set to {@link net.elcado.jCF.core.lang.instance.InstanceState#FAILED InstanceState.FAILED}.
     */
    public boolean onPaused() {
//begin of modifiable zone(JavaCode)......C/d509b6a4-805f-4e71-9afe-e4d92c4d6c67

//end of modifiable zone(JavaCode)........E/d509b6a4-805f-4e71-9afe-e4d92c4d6c67
//begin of modifiable zone................T/e08f0936-00c3-481c-9197-fe0e0710a896
        return true;
//end of modifiable zone..................E/e08f0936-00c3-481c-9197-fe0e0710a896
    }

    /**
     * Called when the instance state has been set to {@link net.elcado.jCF.core.lang.instance.InstanceState#RESUMED InstanceState.RESUMED}.
     * @return true if user code accepts the state change, false otherwise. In this latter case, instance state will be set to {@link net.elcado.jCF.core.lang.instance.InstanceState#FAILED InstanceState.FAILED}.
     */
    public boolean onResumed() {
//begin of modifiable zone(JavaCode)......C/abee1d36-9044-41f0-a70d-1a9238e5bc1a

//end of modifiable zone(JavaCode)........E/abee1d36-9044-41f0-a70d-1a9238e5bc1a
//begin of modifiable zone................T/1ac4dad5-da71-4e7a-8c32-dbf1e009437e
        return true;
//end of modifiable zone..................E/1ac4dad5-da71-4e7a-8c32-dbf1e009437e
    }

    /**
     * Called when the instance state has been set to {@link net.elcado.jCF.core.lang.instance.InstanceState#STOPPED InstanceState.STOPPED}.
     * @return true if user code accepts the state change, false otherwise. In this latter case, instance state will be set to {@link net.elcado.jCF.core.lang.instance.InstanceState#FAILED InstanceState.FAILED}.
     */
    public boolean onStopped() {
//begin of modifiable zone................T/18bc0e52-cc98-479b-8ac9-c2ae72a5ad6c
        if (this.dispatcher != null) {
            this.dispatcher.stop();
            this.dispatcher = null;
        }
        Util.close(this.channel);
//end of modifiable zone..................E/18bc0e52-cc98-479b-8ac9-c2ae72a5ad6c
//begin of modifiable zone................T/3fc733cf-b119-45d8-bc74-02f1ff70dd2f
        return true;
//end of modifiable zone..................E/3fc733cf-b119-45d8-bc74-02f1ff70dd2f
    }

    /**
     * Called when the instance state has been set to {@link net.elcado.jCF.core.lang.instance.InstanceState#STOPPED InstanceState.STOPPED}.
     * @return true if user code accepts the state change, false otherwise. In this latter case, instance state will be set to {@link net.elcado.jCF.core.lang.instance.InstanceState#FAILED InstanceState.FAILED}.
     */
    public boolean onFailed() {
//begin of modifiable zone(JavaCode)......C/cf73cb78-8d07-4927-8c7c-d0ca6e176864

//end of modifiable zone(JavaCode)........E/cf73cb78-8d07-4927-8c7c-d0ca6e176864
//begin of modifiable zone................T/6978a3c0-51bb-4dab-a965-273af1a4319d
        return true;
//end of modifiable zone..................E/6978a3c0-51bb-4dab-a965-273af1a4319d
    }

    public void addInstance(final Instance instance) {
//begin of modifiable zone................T/f557f145-e14c-4841-a0dd-7f1d2d78b74c
        try {
            // add to local list
            List<Instance> ownListOfInstances = this.instances.getOrDefault(this.ownAddress, new ArrayList<>());
            ownListOfInstances.add(instance);
            this.instances.put(this.ownAddress, ownListOfInstances);
        
            // add on all registries
            this.dispatcher.callRemoteMethods(
                    null,
                    new MethodCall(ADD_INSTANCE, this.ownAddress, instance),
                    RequestOptions.ASYNC().setTransientFlags(Message.TransientFlag.DONT_LOOPBACK));
        } catch (Exception e) {
            throw new RuntimeException("RemoteRegistry: adding instance '" + instance.getInstanceId() + "' failed", e);
        }
//end of modifiable zone..................E/f557f145-e14c-4841-a0dd-7f1d2d78b74c
    }

    private void _addInstance(final Address registryAddress, final Instance instance) {
//begin of modifiable zone................T/45eabb5d-2508-48ce-991e-236e61bdd016
        // tag instance as remote
        instance.setRemote(true);
        
        // add to local list
        List<Instance> registryListOfInstances = this.instances.getOrDefault(registryAddress, new ArrayList<>());
        registryListOfInstances.add(instance);
        this.instances.put(registryAddress, registryListOfInstances);
        
        // deploy instance locally
        this.remoteManagerRegistryServices.injectRPCDispatcher(instance);
//end of modifiable zone..................E/45eabb5d-2508-48ce-991e-236e61bdd016
    }

    public void removeInstance(final Instance instance) {
//begin of modifiable zone................T/aa388cc9-da62-41aa-b81e-b84e346da857
        try {
            // remove from local list
            this.instances.computeIfPresent(this.ownAddress, (address, ownListOfInstances) -> {
                ownListOfInstances.remove(instance);
                if (ownListOfInstances.size() > 0) return ownListOfInstances;
                return null;
            });
        
            // remove from all registries
            this.dispatcher.callRemoteMethods(
                    null,
                    new MethodCall(REMOVE_INSTANCE, this.ownAddress, instance),
                    // SYNC call to avoid InstancesManager to remove instance before all clients have left
                    RequestOptions.SYNC().setTransientFlags(Message.TransientFlag.DONT_LOOPBACK));
        } catch (Exception e) {
            throw new RuntimeException("RemoteRegistry: removing instance '" + instance.getInstanceId() + "' failed", e);
        }
//end of modifiable zone..................E/aa388cc9-da62-41aa-b81e-b84e346da857
    }

    private void _removeInstance(final Address registryAddress, final Instance instance) {
//begin of modifiable zone................T/bd5c1221-c0da-42f4-9ec4-69999d28c5e4
        // undeploy instance locally
        this.remoteManagerRegistryServices.closeRPCDispatcher(instance);
        
        // remove from local list
        this.instances.computeIfPresent(registryAddress, (address, registryInstances) -> {
            registryInstances.remove(instance);
            if (registryInstances.size() > 0) return registryInstances;
            return null;
        });
//end of modifiable zone..................E/bd5c1221-c0da-42f4-9ec4-69999d28c5e4
    }


//begin of modifiable zone................T/559a83cc-47f3-4e60-9295-adc57399581d
static {
        try {
            methods = new HashMap<>(2);
            methods.put(ADD_INSTANCE, RemoteRegistry.class.getDeclaredMethod(
                    "_addInstance",
                    Address.class,
                    Instance.class
            ));

            methods.put(REMOVE_INSTANCE, RemoteRegistry.class.getDeclaredMethod(
                    "_removeInstance",
                    Address.class,
                    Instance.class
            ));
        }
        catch(NoSuchMethodException e) {
            throw new RuntimeException(e);
        }
    }
//end of modifiable zone..................E/559a83cc-47f3-4e60-9295-adc57399581d
    public class RegistryStateListener implements StateListener {
        public void getState(OutputStream ostream) throws Exception {
//begin of modifiable zone................T/363aada5-07c0-4c45-947f-a6512cc5176f
            try (ObjectOutputStream oos = new ObjectOutputStream(new BufferedOutputStream(ostream, 1024))) {
                // transform map to serialize (because Address is not Serializable)
                Map<String, List<Instance>> mapToSerialize = new HashMap<>();
                instances.forEach((address, listOfInstances) -> mapToSerialize.put(((UUID) address).toStringLong(), listOfInstances));
            
                // write
                oos.writeObject(mapToSerialize);
            }
//end of modifiable zone..................E/363aada5-07c0-4c45-947f-a6512cc5176f
        }

        public void setState(InputStream istream) throws Exception {
//begin of modifiable zone................T/fe547776-2ba5-4837-801a-c2d79dff3690
            try (ObjectInputStream ois = new ObjectInputStream(istream)) {
                // read
                @SuppressWarnings("unchecked")
                Map<String, List<Instance>> deserializedMap = (Map<String, List<Instance>>) ois.readObject();
            
                // each instance is added and notified
                deserializedMap.forEach((addressAsString, instances) ->
                    instances.forEach(instance -> {
                        _addInstance(UUID.fromString(addressAsString), instance);
                    })
                );
            }
//end of modifiable zone..................E/fe547776-2ba5-4837-801a-c2d79dff3690
        }

    }

    public class RegistryMembershipListener implements MembershipListener {
        public void viewAccepted(View newRegistry) {
//begin of modifiable zone(JavaCode)......C/242fa41b-d39e-4063-a1c8-c668943326d7

//end of modifiable zone(JavaCode)........E/242fa41b-d39e-4063-a1c8-c668943326d7
        }

        public void suspect(Address suspectedDeadRegistry) {
//begin of modifiable zone................T/0c895049-4619-4afd-8993-feb49226add4
            // remove dead registry's instances
            List<Instance> deadInstancesOrDefault = RemoteRegistry.this.instances.getOrDefault(suspectedDeadRegistry, new ArrayList<>());
            deadInstancesOrDefault.forEach(instance -> _removeInstance(suspectedDeadRegistry, instance));
            
            // finally locally remove dead registry
            RemoteRegistry.this.instances.remove(suspectedDeadRegistry);
//end of modifiable zone..................E/0c895049-4619-4afd-8993-feb49226add4
        }

        public void block() {
//begin of modifiable zone(JavaCode)......C/4735aa47-3601-45ff-80ee-f8069b632685

//end of modifiable zone(JavaCode)........E/4735aa47-3601-45ff-80ee-f8069b632685
        }

        public void unblock() {
//begin of modifiable zone(JavaCode)......C/dfc82909-d61a-41e5-bf6b-bb7a91bc2aff

//end of modifiable zone(JavaCode)........E/dfc82909-d61a-41e5-bf6b-bb7a91bc2aff
        }

    }

}
