/*
 * Copyright (c). Frédéric Cadier (2014-2018)
 *
 * This software is a computer program whose purpose is to allow component programming in pure java.
 *
 * This software is governed by the CeCILL-C license under French law and abiding by the rules of distribution of free software. You can use, modify and/or redistribute the software under the terms of the CeCILL-C license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy, modify and redistribute granted by the license, users are provided only with a limited warranty and the software's author, the holder of the economic rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, that may mean that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured and, more generally, to use and operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had knowledge of the CeCILL-C license and that you accept its terms.
 */

package net.elcado.jCF.core.container.instancesManager;

import net.elcado.jCF.core.lang.instance.InstanceId;

public class InstancesManagerEvent {
    public static final InstancesManagerEventType INSTANCE_INITIALIZED = new InstancesManagerEventType("INSTANCE_DEPLOYED");

    public static final InstancesManagerEventType INSTANCE_STARTED = new InstancesManagerEventType("INSTANCE_STARTED");

    public static final InstancesManagerEventType INSTANCE_PAUSED = new InstancesManagerEventType("INSTANCE_PAUSED");

    public static final InstancesManagerEventType INSTANCE_RESUMED = new InstancesManagerEventType("INSTANCE_RESUMED");

    public static final InstancesManagerEventType INSTANCE_STOPPED = new InstancesManagerEventType("INSTANCE_STOPPED");

    public static final InstancesManagerEventType INSTANCE_DISPOSED = new InstancesManagerEventType("INSTANCE_DISPOSED");

    protected final InstancesManagerEventType eventType;

    protected final InstanceId instanceId;

    public InstancesManagerEvent(final InstancesManagerEventType anEventType, final InstanceId anInstanceId) {
//begin of modifiable zone(JavaSuper).....C/046ccb17-7106-4ce5-9eb5-e7f3dc341040

//end of modifiable zone(JavaSuper).......E/046ccb17-7106-4ce5-9eb5-e7f3dc341040
//begin of modifiable zone................T/2e9fe1f6-3b85-402f-8847-f261507016db
        this.eventType = anEventType;
        this.instanceId = anInstanceId;
//end of modifiable zone..................E/2e9fe1f6-3b85-402f-8847-f261507016db
    }

    public InstancesManagerEventType getEventType() {
//begin of modifiable zone(JavaCode)......C/9a9e8497-2778-4c46-9a7c-d3522141c57f

//end of modifiable zone(JavaCode)........E/9a9e8497-2778-4c46-9a7c-d3522141c57f
//begin of modifiable zone................T/7851183d-3975-43de-a835-b84299a26b92
        return this.eventType;
//end of modifiable zone..................E/7851183d-3975-43de-a835-b84299a26b92
    }

    public InstanceId getInstanceId() {
//begin of modifiable zone................T/509787bf-abd1-4c34-b342-8f6599391cde
        // Automatically generated method. Please delete this comment before entering specific code.
//end of modifiable zone..................E/509787bf-abd1-4c34-b342-8f6599391cde
//begin of modifiable zone................T/9dfebf3d-c9e9-4c9a-ace3-8c947f93719b
        return this.instanceId;
//end of modifiable zone..................E/9dfebf3d-c9e9-4c9a-ace3-8c947f93719b
    }

    @Override
    public String toString() {
//begin of modifiable zone(JavaCode)......C/da7765d6-a7da-444c-a1f3-0b149c497c6d

//end of modifiable zone(JavaCode)........E/da7765d6-a7da-444c-a1f3-0b149c497c6d
//begin of modifiable zone................T/83f83069-76ad-4147-9eac-3576a8a412d8
        return this.eventType.toString() + " [" + this.instanceId + "]";
//end of modifiable zone..................E/83f83069-76ad-4147-9eac-3576a8a412d8
    }

    public static class InstancesManagerEventType {
        protected final String eventName;

        public InstancesManagerEventType(final String anEventName) {
//begin of modifiable zone(JavaSuper).....C/02316284-8fe7-4f43-aa50-a2ed3a006c8b

//end of modifiable zone(JavaSuper).......E/02316284-8fe7-4f43-aa50-a2ed3a006c8b
//begin of modifiable zone................T/22a2d1f4-a74c-46d8-b651-f621ce0bb4b7
            this.eventName = anEventName;
//end of modifiable zone..................E/22a2d1f4-a74c-46d8-b651-f621ce0bb4b7
        }

        @Override
        public String toString() {
//begin of modifiable zone(JavaCode)......C/71900e7a-04eb-4e93-93a0-ab3a304404fa

//end of modifiable zone(JavaCode)........E/71900e7a-04eb-4e93-93a0-ab3a304404fa
//begin of modifiable zone................T/cab26190-65b2-4926-8522-e0bce870ffef
            return this.eventName;
//end of modifiable zone..................E/cab26190-65b2-4926-8522-e0bce870ffef
        }

    }

}
