/*
 * Copyright (c). Frédéric Cadier (2014-2018)
 *
 * This software is a computer program whose purpose is to allow component programming in pure java.
 *
 * This software is governed by the CeCILL-C license under French law and abiding by the rules of distribution of free software. You can use, modify and/or redistribute the software under the terms of the CeCILL-C license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy, modify and redistribute granted by the license, users are provided only with a limited warranty and the software's author, the holder of the economic rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, that may mean that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured and, more generally, to use and operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had knowledge of the CeCILL-C license and that you accept its terms.
 */

package net.elcado.jCF.core.container.instancesManager.instanceStateManager;

import net.elcado.jCF.core.annotations.Component;
import net.elcado.jCF.core.annotations.ImplementedService;
import net.elcado.jCF.core.annotations.UsedService;
import net.elcado.jCF.core.exception.IllegalComponentStateTransitionException;
import net.elcado.jCF.core.lang.instance.Instance;
import net.elcado.jCF.core.lang.instance.InstanceState;
import net.elcado.jCF.core.lang.instance.events.EventsSink;
import net.elcado.jCF.core.lang.instance.events.SinkQueue;
import net.elcado.jCF.log.LoggingServices;

@Component
public class InstanceStateManager implements InstanceStateManagerServices {
    @ImplementedService
    public final transient InstanceStateManagerServices instanceStateManagerServices = this;

    @UsedService
    private final transient LoggingServices loggingServices = null;

    private InstanceStateManager() {
//begin of modifiable zone(JavaSuper).....C/466d5763-b89c-4c15-80aa-bb1cb98e3604

//end of modifiable zone(JavaSuper).......E/466d5763-b89c-4c15-80aa-bb1cb98e3604
//begin of modifiable zone................T/cac75b43-7691-4b94-b836-11fe114eb12b
        
//end of modifiable zone..................E/cac75b43-7691-4b94-b836-11fe114eb12b
    }

    /**
     * Set supplied instance's state to {@link net.elcado.jCF.core.lang.instance.InstanceState#INITIALIZED InstanceState.INITIALIZED} if it's {@link net.elcado.jCF.core.lang.component.Initializable Initializable} facet returns true or if there is not such facet, to {@link net.elcado.jCF.core.lang.instance.InstanceState#FAILED InstanceState.FAILED} otherwise.
     * @param anInstance instance to initialize
     * @return true if instance has been initialized, false otherwise
     */
    public boolean initInstance(final Instance anInstance) {
//begin of modifiable zone................T/637d7029-d2b9-450c-9f9c-2805411a7755
        if (this.loggingServices != null) {
            this.loggingServices.debug("Initializing " + anInstance.getInstanceId());
        }
        
        // try to initialize from current state
        boolean transitionResult = this.doTransition(anInstance,
                anInstance.getCurrentInstanceState()::init,
                InstanceState.INITIALIZED
        );
//end of modifiable zone..................E/637d7029-d2b9-450c-9f9c-2805411a7755
//begin of modifiable zone................T/da7e9c6c-2d5b-4b49-a90c-9fcb1f13bfab
        return transitionResult;
//end of modifiable zone..................E/da7e9c6c-2d5b-4b49-a90c-9fcb1f13bfab
    }

    /**
     * @param anInstance instance to dispose
     * @return true if instance is initialized
     */
    public boolean isInitialized(final Instance anInstance) {
//begin of modifiable zone(JavaCode)......C/d041eefc-366e-4062-adce-31e71dc28555

//end of modifiable zone(JavaCode)........E/d041eefc-366e-4062-adce-31e71dc28555
//begin of modifiable zone................T/7bc5ca69-2edf-4131-ae92-5c770c256925
        return anInstance.getCurrentInstanceState() == InstanceState.INITIALIZED;
//end of modifiable zone..................E/7bc5ca69-2edf-4131-ae92-5c770c256925
    }

    /**
     * Set supplied instance's state to {@link net.elcado.jCF.core.lang.instance.InstanceState#STARTED InstanceState.STARTED} if it's {@link net.elcado.jCF.core.lang.component.Startable Startable} facet returns true or if there is not such facet, to {@link net.elcado.jCF.core.lang.instance.InstanceState#FAILED InstanceState.FAILED} otherwise.
     * @param anInstance instance to start
     * @return true if instance has been started, false otherwise
     */
    public boolean startInstance(final Instance anInstance) {
//begin of modifiable zone................T/d67aa0db-1a27-4fee-b35e-850fe7f6d405
        if (this.loggingServices != null) {
            this.loggingServices.debug("Starting " + anInstance.getInstanceId());
        }
        
        // try to start from current state
        boolean transitionResult = this.doTransition(anInstance,
                anInstance.getCurrentInstanceState()::start,
                InstanceState.STARTED
        );
//end of modifiable zone..................E/d67aa0db-1a27-4fee-b35e-850fe7f6d405
//begin of modifiable zone................T/ba56c4de-0441-4ace-b754-ac1bfa257d79
        return transitionResult;
//end of modifiable zone..................E/ba56c4de-0441-4ace-b754-ac1bfa257d79
    }

    /**
     * @param anInstance instance to dispose
     * @return true if instance is started
     */
    public boolean isStarted(final Instance anInstance) {
//begin of modifiable zone(JavaCode)......C/36752bf7-fcc1-42d8-8e4f-6d2a963766fe

//end of modifiable zone(JavaCode)........E/36752bf7-fcc1-42d8-8e4f-6d2a963766fe
//begin of modifiable zone................T/ef9dbb42-8d63-4cd3-90d0-e85c9436f47e
        return anInstance.getCurrentInstanceState() == InstanceState.STARTED;
//end of modifiable zone..................E/ef9dbb42-8d63-4cd3-90d0-e85c9436f47e
    }

    /**
     * Set supplied instance's state to {@link net.elcado.jCF.core.lang.instance.InstanceState#PAUSED InstanceState.PAUSED} if it's {@link net.elcado.jCF.core.lang.component.Startable Startable} facet returns true or if there is not such facet, to {@link net.elcado.jCF.core.lang.instance.InstanceState#FAILED InstanceState.FAILED} otherwise.
     * @param anInstance instance to pause
     * @return true if instance has been paused, false otherwise
     */
    public boolean pauseInstance(final Instance anInstance) {
//begin of modifiable zone................T/c560ecc8-f59e-4181-a440-92f0cb214e2e
        if (this.loggingServices != null) {
            this.loggingServices.debug("Pausing " + anInstance.getInstanceId());
        }
        
        // try to pause from current state
        boolean transitionResult = this.doTransition(anInstance,
                anInstance.getCurrentInstanceState()::pause,
                InstanceState.PAUSED
        );
//end of modifiable zone..................E/c560ecc8-f59e-4181-a440-92f0cb214e2e
//begin of modifiable zone................T/455c5927-ca91-4650-8b2b-0d4962cd6193
        return transitionResult;
//end of modifiable zone..................E/455c5927-ca91-4650-8b2b-0d4962cd6193
    }

    /**
     * @param anInstance instance to dispose
     * @return true if instance is paused
     */
    public boolean isPaused(final Instance anInstance) {
//begin of modifiable zone(JavaCode)......C/fd3cca8e-ea9b-4828-9d1d-e5c5e9e14b34

//end of modifiable zone(JavaCode)........E/fd3cca8e-ea9b-4828-9d1d-e5c5e9e14b34
//begin of modifiable zone................T/4c1ec5d8-a500-4523-87f5-00480ca731f9
        return anInstance.getCurrentInstanceState() == InstanceState.PAUSED;
//end of modifiable zone..................E/4c1ec5d8-a500-4523-87f5-00480ca731f9
    }

    /**
     * Set supplied instance's state to {@link net.elcado.jCF.core.lang.instance.InstanceState#RESUMED InstanceState.RESUMED} if it's {@link net.elcado.jCF.core.lang.component.Startable Startable} facet returns true or if there is not such facet, to {@link net.elcado.jCF.core.lang.instance.InstanceState#FAILED InstanceState.FAILED} otherwise.
     * @param anInstance instance to resume
     * @return true if instance has been resumed, false otherwise
     */
    public boolean resumeInstance(final Instance anInstance) {
//begin of modifiable zone................T/4c8afbe7-c69b-429e-aa94-e5562b275927
        if (this.loggingServices != null) {
            this.loggingServices.debug("Resuming " + anInstance.getInstanceId());
        }
        
        // try to resume from current state
        boolean transitionResult = this.doTransition(anInstance,
                anInstance.getCurrentInstanceState()::resume,
                InstanceState.RESUMED
        );
//end of modifiable zone..................E/4c8afbe7-c69b-429e-aa94-e5562b275927
//begin of modifiable zone................T/f924654e-b6ce-4ac3-aed5-e1469d8e7010
        return transitionResult;
//end of modifiable zone..................E/f924654e-b6ce-4ac3-aed5-e1469d8e7010
    }

    /**
     * @param anInstance instance to dispose
     * @return true if instance is resumed
     */
    public boolean isResumed(final Instance anInstance) {
//begin of modifiable zone(JavaCode)......C/682e52ff-f0a8-4bbf-8417-92a2796672e9

//end of modifiable zone(JavaCode)........E/682e52ff-f0a8-4bbf-8417-92a2796672e9
//begin of modifiable zone................T/02eaec20-9b7f-4677-9d07-fd728984a963
        return anInstance.getCurrentInstanceState() == InstanceState.RESUMED;
//end of modifiable zone..................E/02eaec20-9b7f-4677-9d07-fd728984a963
    }

    /**
     * Set supplied instance's state to {@link net.elcado.jCF.core.lang.instance.InstanceState#STOPPED InstanceState.STOPPED} if it's {@link net.elcado.jCF.core.lang.component.Startable Startable} facet returns true or if there is not such facet, to {@link net.elcado.jCF.core.lang.instance.InstanceState#FAILED InstanceState.FAILED} otherwise.
     * @param anInstance instance to stop
     * @return true if instance is stopped
     */
    public boolean stopInstance(final Instance anInstance) {
//begin of modifiable zone................T/93f57802-e981-4204-a5b7-1e9c0874f5b2
        if (this.loggingServices != null) {
            this.loggingServices.debug("Stopping " + anInstance.getInstanceId());
        }
        
        // try to stop from current state
        boolean transitionResult = this.doTransition(anInstance,
                anInstance.getCurrentInstanceState()::stop,
                InstanceState.STOPPED
        );
//end of modifiable zone..................E/93f57802-e981-4204-a5b7-1e9c0874f5b2
//begin of modifiable zone................T/532e9a30-0986-4808-93eb-a03e41bebe1e
        return transitionResult;
//end of modifiable zone..................E/532e9a30-0986-4808-93eb-a03e41bebe1e
    }

    /**
     * @param anInstance instance to dispose
     * @return true if instance has been disposed, false otherwise
     */
    public boolean isStopped(final Instance anInstance) {
//begin of modifiable zone(JavaCode)......C/01b8852b-9d84-485c-8b4c-f73790321449

//end of modifiable zone(JavaCode)........E/01b8852b-9d84-485c-8b4c-f73790321449
//begin of modifiable zone................T/38f18f5a-3ff4-4cd0-a187-70fe8198ddfb
        return anInstance.getCurrentInstanceState() == InstanceState.STOPPED;
//end of modifiable zone..................E/38f18f5a-3ff4-4cd0-a187-70fe8198ddfb
    }

    /**
     * Set supplied instance's state to {@link net.elcado.jCF.core.lang.instance.InstanceState#DISPOSED InstanceState.DISPOSED} if it's {@link net.elcado.jCF.core.lang.component.Initializable Initializable} facet returns true or if there is not such facet, to {@link net.elcado.jCF.core.lang.instance.InstanceState#FAILED InstanceState.FAILED} otherwise.
     * @param anInstance instance to dispose
     * @return true if instance has been disposed, false otherwise
     */
    public boolean disposeInstance(final Instance anInstance) {
//begin of modifiable zone................T/9bd579ed-4dec-4051-a320-0fdd4854920d
        if (this.loggingServices != null) {
            this.loggingServices.debug("Disposing " + anInstance.getInstanceId());
        }
        
        // try to dispose from current state
        boolean transitionResult = this.doTransition(anInstance,
                anInstance.getCurrentInstanceState()::dispose,
                InstanceState.DISPOSED
        );
//end of modifiable zone..................E/9bd579ed-4dec-4051-a320-0fdd4854920d
//begin of modifiable zone................T/81160db1-11cb-4bd2-929c-991d9efa4e1c
        return transitionResult;
//end of modifiable zone..................E/81160db1-11cb-4bd2-929c-991d9efa4e1c
    }

    /**
     * @param anInstance instance to dispose
     * @return true if instance is disposed
     */
    public boolean isDisposed(final Instance anInstance) {
//begin of modifiable zone(JavaCode)......C/47ccc074-e0e2-4d00-93f8-cd564b8cb8c9

//end of modifiable zone(JavaCode)........E/47ccc074-e0e2-4d00-93f8-cd564b8cb8c9
//begin of modifiable zone................T/686e1edf-0ebf-4d22-ae49-b8e3bbb8571e
        return anInstance.getCurrentInstanceState() == InstanceState.DISPOSED;
//end of modifiable zone..................E/686e1edf-0ebf-4d22-ae49-b8e3bbb8571e
    }

    private boolean doTransition(final Instance anInstance, final NextStateFunction nextStateFunction, final InstanceState expectedState) {
//begin of modifiable zone................T/24d8da9c-99b0-4e3f-a943-9b7ed4790de7
        // try transition
        InstanceState currentInstanceState = anInstance.getCurrentInstanceState();
        try {
            nextStateFunction.accept(anInstance);
        } catch (IllegalComponentStateTransitionException e) {
            if (this.loggingServices != null) {
                this.loggingServices.error("Illegal transition from " + currentInstanceState + " to " + expectedState + " for " + anInstance.getInstanceId() + ": stay in " + currentInstanceState + ", due to: " + e.getMessage());
            }
        }
        
        // get new current state to test that state is as expected
        InstanceState newCurrentInstanceState = anInstance.getCurrentInstanceState();
        
        // transition is only ok if state changed and now equals supplied expectedState
        boolean transitionResult = false;
        if (!newCurrentInstanceState.equals(currentInstanceState)) {
            // fail instance if new state is FAILED
            if (InstanceState.FAILED.equals(newCurrentInstanceState)) {
                this.failInstance(anInstance, expectedState);
            }
        
            transitionResult = expectedState.equals(newCurrentInstanceState);
        }
//end of modifiable zone..................E/24d8da9c-99b0-4e3f-a943-9b7ed4790de7
//begin of modifiable zone................T/139a1764-f4d8-4ca6-9c7d-5b1a66273713
        return transitionResult;
//end of modifiable zone..................E/139a1764-f4d8-4ca6-9c7d-5b1a66273713
    }

    @SuppressWarnings("rawtypes")
    private void failInstance(final Instance anInstance, final InstanceState nextState) {
//begin of modifiable zone................T/c60c0535-52ef-4054-ae11-b12c883ac128
        if (this.loggingServices != null) {
            this.loggingServices.debug("Transition to " + nextState + " for " + anInstance.getInstanceId() + " KO, now in " + InstanceState.FAILED + " state, due to user code");
        }
        
        // stops all sinks
        for (EventsSink eventsSink : anInstance.getEventsSinks()) {
            ((SinkQueue) eventsSink.getObject()).stopProcessingEvents();
        }
        
        // TODO what about children ? -> they should also fail
//end of modifiable zone..................E/c60c0535-52ef-4054-ae11-b12c883ac128
    }

    public interface NextStateFunction {
        void accept(final Instance anInstance) throws IllegalComponentStateTransitionException;

    }

}
