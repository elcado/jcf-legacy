/*
 * Copyright (c). Frédéric Cadier (2014-2018)
 *
 * This software is a computer program whose purpose is to allow component programming in pure java.
 *
 * This software is governed by the CeCILL-C license under French law and abiding by the rules of distribution of free software. You can use, modify and/or redistribute the software under the terms of the CeCILL-C license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy, modify and redistribute granted by the license, users are provided only with a limited warranty and the software's author, the holder of the economic rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, that may mean that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured and, more generally, to use and operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had knowledge of the CeCILL-C license and that you accept its terms.
 */

package net.elcado.jCF.core.container.instancesManager.dependenciesResolver;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.lang.model.element.Modifier;
import net.elcado.jCF.core.annotations.Component;
import net.elcado.jCF.core.container.instancesManager.remoteManager.RemoteManagerServices;
import net.elcado.jCF.core.lang.instance.Instance;
import net.elcado.jCF.core.lang.instance.InstanceId;
import net.elcado.jCF.core.lang.instance.events.EventsSink;
import net.elcado.jCF.core.lang.instance.events.EventsSource;
import net.elcado.jCF.core.lang.instance.events.SinkQueue;
import net.elcado.jCF.core.lang.instance.events.SourceDispatcher;
import net.elcado.jCF.core.lang.instance.services.ImplementedService;
import net.elcado.jCF.core.lang.instance.services.UsedService;
import net.elcado.jCF.log.LoggingServices;
import net.elcado.jCF.utils.tree.Tree;

@Component
public class DependenciesResolver implements DependenciesResolverServices {
    protected Tree<InstanceId,Instance> instancesTree;

    @net.elcado.jCF.core.annotations.ImplementedService
    public final transient DependenciesResolverServices dependenciesResolverServices = this;

    @net.elcado.jCF.core.annotations.UsedService
    private final transient LoggingServices loggingServices = null;

    @net.elcado.jCF.core.annotations.UsedService
    private final transient RemoteManagerServices remoteManagerServices = null;

    private DependenciesResolver() {
//begin of modifiable zone(JavaSuper).....C/9a62f660-c98e-47fc-ab8d-9b7e803cfa1f

//end of modifiable zone(JavaSuper).......E/9a62f660-c98e-47fc-ab8d-9b7e803cfa1f
//begin of modifiable zone(JavaCode)......C/9a62f660-c98e-47fc-ab8d-9b7e803cfa1f

//end of modifiable zone(JavaCode)........E/9a62f660-c98e-47fc-ab8d-9b7e803cfa1f
    }

    /**
     * Resolve the dependencies inside the tree of instances.
     * @param instancesTree instances' sub tree where dependencies have to be resolved
     */
    public void resolveDependencies(Tree<InstanceId,Instance> instancesTree) {
//begin of modifiable zone................T/da0fad98-4010-4009-8730-db92cdf7457e
        this.instancesTree = instancesTree;
        
        // resolve dependencies of tree roots
        for (InstanceId instanceId : this.instancesTree.getRoots()) {
            // ignore remote instances
            if (this.instancesTree.getRef(instanceId).isRemote()) {
                continue;
            }
            this.resolveDependencies(instanceId);
        }
//end of modifiable zone..................E/da0fad98-4010-4009-8730-db92cdf7457e
    }

    public void removeDependencies(final InstanceId instanceId, Tree<InstanceId,Instance> instancesTree) {
//begin of modifiable zone................T/d7cf14fe-d963-4911-986d-d4b2339756c2
        this.instancesTree = instancesTree;
        
        this.removeDependencies(instanceId);
//end of modifiable zone..................E/d7cf14fe-d963-4911-986d-d4b2339756c2
    }

    private void resolveDependencies(final InstanceId instanceId) {
//begin of modifiable zone................T/f612b909-230a-4097-be49-767f7f0b323a
        if (this.loggingServices != null) {
            this.loggingServices.debug("Resolving dependencies for " + instanceId);
        }
        
        Instance instance = this.instancesTree.getRef(instanceId);
        
        // resolve used services dependencies
        for (net.elcado.jCF.core.lang.instance.services.UsedService usedService : instance.getUsedServices()) {
            this.resolveUsedServiceDependency(instanceId, instance, usedService);
        }
        
        // resolve events sinks
        for (EventsSink eventsSink : instance.getEventsSinks()) {
            this.resolveEventsSinkDependency(instanceId, instance, eventsSink);
        }
        
        // resolve dependencies of all children
        for (InstanceId childId : this.instancesTree.getChildren(instanceId)) {
            this.resolveDependencies(childId);
        }
//end of modifiable zone..................E/f612b909-230a-4097-be49-767f7f0b323a
    }

    @SuppressWarnings("unchecked")
    private void resolveUsedServiceDependency(final InstanceId instanceId, final Instance instance, final UsedService usedService) {
//begin of modifiable zone................T/0a310097-ad18-4693-8169-41561207a452
        if (this.loggingServices != null) {
            this.loggingServices.debug("Looking for implementation for @UsedService " + usedService.getClazz() + " in " + instanceId + " [" + instance.getComponent().getClazz() + "]");
        }
        
        // find all implementation for this used service
        Map<InstanceId, ImplementedService> foundImplementationServices = this.findImplementedServices(instanceId, usedService);
        if (foundImplementationServices.size() > 0) {
            if (usedService.isCardinalityMany()) {
                // eventually inject Map
                if (usedService.getObject() == null) {
                    usedService.setObject(new HashMap<>());
                }
        
                if (this.loggingServices != null) {
                    this.loggingServices.debug("Found implementations in " + foundImplementationServices.keySet());
                }
        
                // for each found implementations...
                foundImplementationServices.forEach((id, is) -> {
                    // inject found implementation
                    ((Map<InstanceId, Object>) usedService.getObject()).put(id, is.getObject());
                    is.addUsedService(usedService);
                });
            }
            else {
                if (foundImplementationServices.size() == 1) {
                    InstanceId implementationInstanceId = foundImplementationServices.keySet().iterator().next();
                    ImplementedService implementedService = foundImplementationServices.values().iterator().next();
                    if (this.loggingServices != null) {
                        this.loggingServices.debug("Found implementation in " + implementationInstanceId);
                    }
        
                    // inject found implementation
                    usedService.setObject(implementedService.getObject());
                    implementedService.addUsedService(usedService);
                }
                // too much found implementations
                else if (foundImplementationServices.size() > 1) {
                    if (this.loggingServices != null) {
                        this.loggingServices.info("Found too much implementations for @UsedService " + usedService.getClazz().getSimpleName() + " in " + instanceId + " [" + instance.getComponent().getClazz() + "]");
                    }
                }
            }
        }
        else {
            // no found implementations
            if (this.loggingServices != null) {
                this.loggingServices.warn("Found no implementation for @UsedService " + usedService.getClazz().getSimpleName() + " in " + instanceId + " [" + instance.getComponent().getClazz() + "]");
            }
        }
//end of modifiable zone..................E/0a310097-ad18-4693-8169-41561207a452
    }

    private Map<InstanceId, ImplementedService> findImplementedServices(InstanceId instanceId, UsedService usedService) {
//begin of modifiable zone................T/78d20ca6-63e7-4523-aa91-a6613b071bca
        Map<InstanceId, ImplementedService> foundImplementedServices = new HashMap<>();
        
        try {
            Class<?> usedServiceClazz = usedService.getClazz();
            List<InstanceId> lookedInstanceId = new ArrayList<>();
        
            // look for PUBLIC, PRIVATE or PROTECTED implementations in parent
            InstanceId parentId = this.instancesTree.getParent(instanceId);
            if (parentId != null) {
                lookedInstanceId.add(parentId);
        
                this.lookForImplementedService(parentId, usedServiceClazz, Modifier.PUBLIC, foundImplementedServices);
                this.lookForImplementedService(parentId, usedServiceClazz, Modifier.PROTECTED, foundImplementedServices);
                this.lookForImplementedService(parentId, usedServiceClazz, Modifier.PRIVATE, foundImplementedServices);
            }
        
            // look for PUBLIC or PROTECTED implementations in neighbors
            for (InstanceId neighborId : this.instancesTree.getNeighbors(instanceId)) {
                lookedInstanceId.add(neighborId);
        
                this.lookForImplementedService(neighborId, usedServiceClazz, Modifier.PUBLIC, foundImplementedServices);
                this.lookForImplementedService(neighborId, usedServiceClazz, Modifier.PROTECTED, foundImplementedServices);
            }
        
            // look for PUBLIC implementations in children
            for (InstanceId childId : this.instancesTree.getChildren(instanceId)) {
                lookedInstanceId.add(childId);
        
                this.lookForImplementedService(childId, usedServiceClazz, Modifier.PUBLIC, foundImplementedServices);
            }
        
            // look for PUBLIC implementations in roots
            for (InstanceId rootId : this.instancesTree.getRoots()) {
                // only look in roots that are not the parent, neighbors or children (impossible, though) of the instance
                if (!lookedInstanceId.contains(rootId)) {
                    this.lookForImplementedService(rootId, usedServiceClazz, Modifier.PUBLIC, foundImplementedServices);
                }
            }
        }
        catch (IllegalArgumentException iae) {
            foundImplementedServices.clear();
            this.loggingServices.error(iae.getMessage());
        }
//end of modifiable zone..................E/78d20ca6-63e7-4523-aa91-a6613b071bca
//begin of modifiable zone................T/0b9c46f0-2d0b-442b-968b-3fe10a57bd52
        return foundImplementedServices;
//end of modifiable zone..................E/0b9c46f0-2d0b-442b-968b-3fe10a57bd52
    }

    private void lookForImplementedService(InstanceId instanceId, Class<?> serviceClazz, final Modifier modifier, final Map<InstanceId, ImplementedService> foundImplementedServices) {
//begin of modifiable zone................T/2a20b5f0-f159-4f33-8d6d-ba1729af434c
        ImplementedService foundImplementedService = null;
        
        // get instance
        Instance instance = this.instancesTree.getRef(instanceId);
        
        // find all implementations for this service type and these modifiers
        for (ImplementedService implementedService : instance.getImplementedServices()) {
            // test implementation type, modifiers and object
            if (implementedService.getClazz() == serviceClazz && implementedService.getModifiers().contains(modifier)) {// && instance.getObject() != null) {
                if (foundImplementedService == null) {
                    foundImplementedService = implementedService;
                }
                else {
                    // service already found -> error
                    this.loggingServices.error("Duplicate @ImplementedService " + serviceClazz + " with modifiers " + modifier + " found in " + instanceId);
                    foundImplementedService = null;
                    break;
                }
            }
        }
        
        if (foundImplementedService != null) {
            // add found implementation and test that nothing has been previously found
            ImplementedService previousValue = foundImplementedServices.put(instanceId, foundImplementedService);
            if (previousValue != null) {
                throw new IllegalArgumentException("Duplicate @ImplementedService " + serviceClazz + " found in " + instanceId);
            }
        }
//end of modifiable zone..................E/2a20b5f0-f159-4f33-8d6d-ba1729af434c
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    private void resolveEventsSinkDependency(final InstanceId instanceId, final Instance instance, final EventsSink eventsSink) {
//begin of modifiable zone................T/bdc10d01-ddeb-4487-9a62-86d91ff2be33
        if (this.loggingServices != null) {
            this.loggingServices.debug("Looking for sources for @EventsSink " + eventsSink.getEventClazz().getSimpleName() + " in " + instance.getComponent().getClazz().getSimpleName());
        }
        
        // find all events sources for this sink
        Map<InstanceId, EventsSource> foundEventsSources = this.findEventsSources(instanceId, eventsSink);
        if (foundEventsSources.size() > 0) {
            if (this.loggingServices != null) {
                this.loggingServices.debug("Found sources " + foundEventsSources);
            }
        
            // add sink to found sources
            foundEventsSources.values().forEach(eventsSource -> {
                // add sources and sink to each others
                eventsSink.addEventsSource(eventsSource);
                eventsSource.addEventsSink(eventsSink);
        
                // add sink to source
                // NOTE: cast are ok since, if we are here, it means that SinkQueue and SourceDispatcher havce already been injected
                SourceDispatcher sourceDispatcher = (SourceDispatcher) eventsSource.getObject();
                SinkQueue sinkQueue = (SinkQueue) eventsSink.getObject();
                sourceDispatcher.addSinkQueue(sinkQueue);
            });
        }
        else {
            if (this.loggingServices != null) {
                this.loggingServices.warn("Found no source for @EventsSink " + eventsSink.getEventClazz().getSimpleName() + " in " + instance.getComponent().getClazz().getSimpleName());
            }
        }
//end of modifiable zone..................E/bdc10d01-ddeb-4487-9a62-86d91ff2be33
    }

    private Map<InstanceId, EventsSource> findEventsSources(InstanceId instanceId, EventsSink eventsSink) {
//begin of modifiable zone................T/c1a26b7b-9bca-4817-bd51-fd9f7124f9fe
        Map<InstanceId, EventsSource> foundEventsSources = new HashMap<>();
        
        try {
            Class<?> sinkEventClazz = eventsSink.getEventClazz();
            List<InstanceId> lookedInstanceId = new ArrayList<>();
        
            // look for PUBLIC, PRIVATE or PROTECTED sources in parent
            InstanceId parentId = this.instancesTree.getParent(instanceId);
            if (parentId != null) {
                lookedInstanceId.add(parentId);
        
                this.lookForEventsSources(parentId, sinkEventClazz, Modifier.PUBLIC, foundEventsSources);
                this.lookForEventsSources(parentId, sinkEventClazz, Modifier.PROTECTED, foundEventsSources);
                this.lookForEventsSources(parentId, sinkEventClazz, Modifier.PRIVATE, foundEventsSources);
            }
        
            // look for PUBLIC or PROTECTED sources in neighbors
            for (InstanceId neighborId : this.instancesTree.getNeighbors(instanceId)) {
                lookedInstanceId.add(neighborId);
        
                this.lookForEventsSources(neighborId, sinkEventClazz, Modifier.PUBLIC, foundEventsSources);
                this.lookForEventsSources(neighborId, sinkEventClazz, Modifier.PROTECTED, foundEventsSources);
            }
        
            // look for PUBLIC sources in children
            for (InstanceId childId : this.instancesTree.getChildren(instanceId)) {
                lookedInstanceId.add(childId);
        
                this.lookForEventsSources(childId, sinkEventClazz, Modifier.PUBLIC, foundEventsSources);
            }
        
            // look for PUBLIC sources in roots
            for (InstanceId rootId : this.instancesTree.getRoots()) {
                // only look in roots that are not the parent, neighbors or children (impossible, though) of the instance
                if (!lookedInstanceId.contains(rootId)) {
                    this.lookForEventsSources(rootId, sinkEventClazz, Modifier.PUBLIC, foundEventsSources);
                }
            }
        }
        catch (IllegalArgumentException iae) {
            foundEventsSources.clear();
            this.loggingServices.error(iae.getMessage());
        }
//end of modifiable zone..................E/c1a26b7b-9bca-4817-bd51-fd9f7124f9fe
//begin of modifiable zone................T/a7aaa581-1632-47fb-bd6b-d59551a80506
        return foundEventsSources;
//end of modifiable zone..................E/a7aaa581-1632-47fb-bd6b-d59551a80506
    }

    private void lookForEventsSources(InstanceId instanceId, Class<?> eventClazz, final Modifier modifier, final Map<InstanceId, EventsSource> foundEventsSources) {
//begin of modifiable zone................T/fc6bddec-4ff1-4245-bff6-d2321749cf70
        EventsSource foundEventsSource = null;
        
        // get instance
        Instance instance = this.instancesTree.getRef(instanceId);
        
        // ignore remote instances
        if (instance.isRemote()) {
            return;
        }
        
        // find all sources for this sink event type and these modifiers
        for (EventsSource eventsSource : instance.getEventsSources()) {
            // test source event type, modifiers and object
            if (eventsSource.getEventClazz() == eventClazz && eventsSource.getModifiers().contains(modifier) && instance.getObject() != null) {
                if (foundEventsSource == null) {
                    foundEventsSource = eventsSource;
                }
                else {
                    // source already found -> error
                    this.loggingServices.error("Duplicate @EventsSource " + eventClazz + " with modifiers " + modifier + " found in " + instanceId);
                    foundEventsSource = null;
                    break;
                }
            }
        }
        
        if (foundEventsSource != null) {
            // add found implementation and test that nothing has been previously found
            EventsSource previousValue = foundEventsSources.put(instanceId, foundEventsSource);
            if (previousValue != null) {
                throw new IllegalArgumentException("Duplicate @EventsSource " + eventClazz + " found in " + instanceId);
            }
        }
//end of modifiable zone..................E/fc6bddec-4ff1-4245-bff6-d2321749cf70
    }

    private void removeDependencies(final InstanceId instanceId) {
//begin of modifiable zone................T/7d09d454-1205-4672-b68c-76acf5df3da4
        if (this.loggingServices != null) {
            this.loggingServices.debug("Removing dependencies of instance [" + instanceId + "]");
        }
        
        // get instance
        Instance instance = this.instancesTree.getRef(instanceId);
        
        // remove implemented from used services
        for (ImplementedService implementedService : instance.getImplementedServices()) {
            for (net.elcado.jCF.core.lang.instance.services.UsedService usedService : implementedService.getUsedServices()) {
                if (usedService.isCardinalityMany()) {
                    @SuppressWarnings("unchecked")
                    Map<InstanceId, Object> map = (Map<InstanceId, Object>) usedService.getObject();
                    map.remove(instanceId);
                }
                else {
                    usedService.setObject(null);
                }
            }
        }
        
        // remove events sinks from sources (nothing to do for remote instances that don't have sinks)
        if (!instance.isRemote()) {
            for (EventsSink eventsSink : instance.getEventsSinks()) {
                eventsSink.getEventsSources().forEach(eventsSource -> eventsSource.removeEventsSink(eventsSink));
            }
        }
//end of modifiable zone..................E/7d09d454-1205-4672-b68c-76acf5df3da4
    }

}
