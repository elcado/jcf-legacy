/*
 * Copyright (c). Frédéric Cadier (2014-2018)
 *
 * This software is a computer program whose purpose is to allow component programming in pure java.
 *
 * This software is governed by the CeCILL-C license under French law and abiding by the rules of distribution of free software. You can use, modify and/or redistribute the software under the terms of the CeCILL-C license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy, modify and redistribute granted by the license, users are provided only with a limited warranty and the software's author, the holder of the economic rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, that may mean that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured and, more generally, to use and operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had knowledge of the CeCILL-C license and that you accept its terms.
 */

package net.elcado.jCF.core.container.componentsManager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.tools.Diagnostic;
import javax.tools.JavaFileObject;
import net.elcado.jCF.core.annotations.ImplementedService;
import net.elcado.jCF.core.annotations.UsedService;
import net.elcado.jCF.core.container.componentsManager.componentProcessor.ComponentProcessor;
import net.elcado.jCF.core.container.componentsManager.componentProcessor.ComponentProcessorResult;
import net.elcado.jCF.core.lang.component.Component;
import net.elcado.jCF.log.LoggingServices;

@net.elcado.jCF.core.annotations.Component
public class ComponentsManager implements ComponentsManagerServices {
    private final Map<Class<?>, Component> components = new HashMap<>();

    @ImplementedService
    public final transient ComponentsManagerServices componentsManagerServices = this;

    @UsedService
    private final transient LoggingServices loggingServices = null;

    private ComponentsManager() {
//begin of modifiable zone(JavaSuper).....C/2393f1d8-076c-4b5a-9d59-eb50986709d6

//end of modifiable zone(JavaSuper).......E/2393f1d8-076c-4b5a-9d59-eb50986709d6
//begin of modifiable zone(JavaCode)......C/2393f1d8-076c-4b5a-9d59-eb50986709d6

//end of modifiable zone(JavaCode)........E/2393f1d8-076c-4b5a-9d59-eb50986709d6
    }

    /**
     * Register a new component.
     * @param componentType The class of the component.
     * @return the built component. Null if component cannot be built.
     */
    public Component registerComponent(Class<?> componentType) {
//begin of modifiable zone................T/69a3a4cf-46fe-45f5-909c-dea685b14fe9
        Component component = this.getComponent(componentType);
        if (component == null) {
            if (this.loggingServices != null) {
                this.loggingServices.info("Discovering " + componentType);
            }
        
            ComponentProcessorResult cpr = ComponentProcessor.discoverComponent(componentType);
        
            // log diagnostics
            if (this.loggingServices != null) {
                for (Diagnostic<? extends JavaFileObject> diagnostic : cpr.getDiagnostics()) {
                    switch (diagnostic.getKind()) {
                        case OTHER:
                        case WARNING:
                            this.loggingServices.warn(diagnostic.toString());
                            break;
                        case NOTE:
                            this.loggingServices.info(diagnostic.toString());
                            break;
                        case MANDATORY_WARNING:
                        case ERROR:
                            this.loggingServices.error(diagnostic.toString());
                            break;
                    }
                }
            }
        
            // get component
            component = cpr.getComponent();
            if (component != null) {
                if (this.loggingServices != null) {
                    this.loggingServices.debug(component.toString());
                }
        
                // put component and composed components in map
                this.components.put(componentType, component);
                component.getComposedComponents().forEach(comp -> this.components.put(comp.getClazz(), comp));
            }
        }
//end of modifiable zone..................E/69a3a4cf-46fe-45f5-909c-dea685b14fe9
//begin of modifiable zone................T/0ae9a71d-91da-4a32-b5d9-6e4f9250e106
        return component;
//end of modifiable zone..................E/0ae9a71d-91da-4a32-b5d9-6e4f9250e106
    }

    /**
     * @param componentType The class of the component.
     * @return the built component. Null if component cannot be built.
     */
    public Component getComponent(Class<?> componentType) {
//begin of modifiable zone(JavaCode)......C/6875cff7-7236-46cd-88f0-6e62ddec9684

//end of modifiable zone(JavaCode)........E/6875cff7-7236-46cd-88f0-6e62ddec9684
//begin of modifiable zone................T/85d8217c-89f5-4473-8e1c-cb1e32996a3a
        return this.components.get(componentType);
//end of modifiable zone..................E/85d8217c-89f5-4473-8e1c-cb1e32996a3a
    }

    /**
     * @return the built component. Null if component cannot be built.
     */
    public List<Component> getComponents() {
//begin of modifiable zone................T/9ad75b6a-534a-423e-a6a1-6bc7f5c98d18
        List<net.elcado.jCF.core.lang.component.Component> components = new ArrayList<>(this.components.values());
//end of modifiable zone..................E/9ad75b6a-534a-423e-a6a1-6bc7f5c98d18
//begin of modifiable zone................T/c511acbf-618e-41c1-98d3-e0be6dd9b3bc
        return components;
//end of modifiable zone..................E/c511acbf-618e-41c1-98d3-e0be6dd9b3bc
    }

    /**
     * Register a new component.
     * @param component The class of the component.
     */
    public void unregisterComponent(Component component) {
//begin of modifiable zone................T/551483dc-1660-462d-93b5-5a3773f916ba
        this.components.remove(component.getClazz());
//end of modifiable zone..................E/551483dc-1660-462d-93b5-5a3773f916ba
    }

}
