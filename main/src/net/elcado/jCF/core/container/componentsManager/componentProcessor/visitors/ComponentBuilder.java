/*
 * Copyright (c). Frédéric Cadier (2014-2018)
 *
 * This software is a computer program whose purpose is to allow component programming in pure java.
 *
 * This software is governed by the CeCILL-C license under French law and abiding by the rules of distribution of free software. You can use, modify and/or redistribute the software under the terms of the CeCILL-C license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy, modify and redistribute granted by the license, users are provided only with a limited warranty and the software's author, the holder of the economic rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, that may mean that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured and, more generally, to use and operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had knowledge of the CeCILL-C license and that you accept its terms.
 */

package net.elcado.jCF.core.container.componentsManager.componentProcessor.visitors;

import java.util.List;
import javax.annotation.processing.Messager;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.MirroredTypesException;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.ElementKindVisitor8;
import javax.lang.model.util.Types;
import net.elcado.jCF.core.annotations.*;
import net.elcado.jCF.core.container.componentsManager.componentProcessor.utils.JavaxLangModelUtils;
import net.elcado.jCF.core.lang.component.Component;
import net.elcado.jCF.core.lang.component.Initializable;
import net.elcado.jCF.core.lang.component.Startable;
import net.elcado.jCF.core.lang.component.events.EventsSinkPort;
import net.elcado.jCF.core.lang.component.events.EventsSourcePort;
import net.elcado.jCF.core.lang.component.id.IdPort;
import net.elcado.jCF.core.lang.component.parameters.ParametersPort;
import net.elcado.jCF.core.lang.component.services.ImplementedServicePort;
import net.elcado.jCF.core.lang.component.services.UsedServicePort;

public class ComponentBuilder extends ElementKindVisitor8<Component,Component> {
    private final Types types;

    public ComponentBuilder(final Types anTypeUtils, final Messager aMessager) {
//begin of modifiable zone(JavaSuper).....C/918eeb78-c42f-4073-8245-f79e871baf4c

//end of modifiable zone(JavaSuper).......E/918eeb78-c42f-4073-8245-f79e871baf4c
//begin of modifiable zone................T/10e57c0f-c3df-4b5b-a82e-1009e1152dd2
        this.types = anTypeUtils;
        //        this.messager = aMessager;
//end of modifiable zone..................E/10e57c0f-c3df-4b5b-a82e-1009e1152dd2
    }

    @Override
    public Component visitType(final TypeElement typeElement, final Component component) {
//begin of modifiable zone................T/83caba1e-4e99-45f6-910f-a42e0839e567
        //        this.messager.printMessage(Kind.NOTE, "[" + typeElement.getKind() + "] " + typeElement.toString());
//end of modifiable zone..................E/83caba1e-4e99-45f6-910f-a42e0839e567
//begin of modifiable zone................T/fa4e5721-7036-4ec3-aff8-40acc532ada3
        return super.visitType(typeElement, component);
//end of modifiable zone..................E/fa4e5721-7036-4ec3-aff8-40acc532ada3
    }

    @Override
    public Component visitTypeAsClass(final TypeElement typeElement, final Component component) {
//begin of modifiable zone................T/c1e39e9a-9aff-4579-9d1a-588f6624a0f9
        net.elcado.jCF.core.annotations.Component annotation = typeElement.getAnnotation(net.elcado.jCF.core.annotations.Component.class);
        if (annotation != null) {
            // get component type
            component.setClazz(JavaxLangModelUtils.typeElementToClass(typeElement));
        
            // by default, component is not remotable
            component.setRemotable(false);
        
            // is component initializable
            boolean isComponentInitializable = this.checkTypeInterfaceImplementation(typeElement, Initializable.class);
            component.setInitializable(isComponentInitializable);
        
            // is component startable
            boolean isComponentStartable = this.checkTypeInterfaceImplementation(typeElement, Startable.class);
            component.setStartable(isComponentStartable);
        
            // element is a type, visit its enclosed elements
            typeElement.getEnclosedElements().forEach(enclosedElement -> enclosedElement.accept(this, component));
        
            // build composed components
            try {
                // provoke exception
                annotation.composition();
            } catch (MirroredTypesException mte) {
                List<? extends TypeMirror> typeMirrors = mte.getTypeMirrors();
                for (TypeMirror composedType : typeMirrors) {
                    TypeElement composedElement = (TypeElement) this.types.asElement(composedType);
                    net.elcado.jCF.core.lang.component.Component composedComponent = this.visitTypeAsClass(composedElement, new net.elcado.jCF.core.lang.component.Component());
                    component.addComposedComponent(composedComponent);
                }
            }
        }
//end of modifiable zone..................E/c1e39e9a-9aff-4579-9d1a-588f6624a0f9
//begin of modifiable zone................T/08cc829d-6e10-4197-a564-65d48284eee4
        return component;
//end of modifiable zone..................E/08cc829d-6e10-4197-a564-65d48284eee4
    }

    @Override
    public Component visitVariable(final VariableElement variableElement, final Component component) {
//begin of modifiable zone................T/4291e954-e8ba-46a5-a6d7-b344996341c6
        //        this.messager.printMessage(Kind.NOTE, "[" + variableElement.getKind() + "] " + variableElement.toString());
//end of modifiable zone..................E/4291e954-e8ba-46a5-a6d7-b344996341c6
//begin of modifiable zone................T/b508b3cc-3371-47ac-abb6-e69f3d202137
        return super.visitVariable(variableElement, component);
//end of modifiable zone..................E/b508b3cc-3371-47ac-abb6-e69f3d202137
    }

    @Override
    public Component visitVariableAsField(final VariableElement variableElement, final Component component) {
//begin of modifiable zone................T/074f5dee-e5fa-4fb1-a3f4-4c2b72514176
        // get id
        if (variableElement.getAnnotation(Id.class) != null) {
            IdPort idp = this.buildIdPort(variableElement);
            component.setIdPort(idp);
        }
        
        // get implemented services
        if (variableElement.getAnnotation(ImplementedService.class) != null) {
            ImplementedServicePort isp = this.buildImplementedServicePort(variableElement);
            component.addImplementedServicePort(isp);
        
            // component is remotable if any of its implemented service port is
            if (isp.isRemotable()) {
                component.setRemotable(true);
            }
        }
        
        // get used services
        if (variableElement.getAnnotation(UsedService.class) != null) {
            UsedServicePort usp = this.buildUsedServicePort(variableElement);
            component.addUsedServicePort(usp);
        }
        
        // get events sources
        if (variableElement.getAnnotation(EventsSource.class) != null) {
            EventsSourcePort esp = this.buildEventsSourcePort(variableElement);
            component.addEventsSourcePort(esp);
        }
        
        // get events sinks
        if (variableElement.getAnnotation(EventsSink.class) != null) {
            EventsSinkPort esp = this.buildEventsSinkPort(variableElement);
            component.addEventsSinkPort(esp);
        }
        
        // get events sinks
        if (variableElement.getAnnotation(ParametersService.class) != null) {
            ParametersPort pp = this.buildParametersPort(variableElement);
            component.addParametersPort(pp);
        }
//end of modifiable zone..................E/074f5dee-e5fa-4fb1-a3f4-4c2b72514176
//begin of modifiable zone................T/96b354a0-0fae-4bc2-a6e0-7624affdd137
        return component;
//end of modifiable zone..................E/96b354a0-0fae-4bc2-a6e0-7624affdd137
    }

    private IdPort buildIdPort(final VariableElement aVariableElement) {
//begin of modifiable zone................T/39eb1dc6-76f1-460e-bade-c946aa0adaeb
        IdPort idp = new IdPort();
        idp.setModifiers(aVariableElement.getModifiers());
        idp.setClazz(JavaxLangModelUtils.typeMirrorToClass(aVariableElement.asType()));
        idp.setName(aVariableElement.getSimpleName().toString());
//end of modifiable zone..................E/39eb1dc6-76f1-460e-bade-c946aa0adaeb
//begin of modifiable zone................T/a83843d8-85d0-4d77-ba4e-1e72f54e86c6
        return idp;
//end of modifiable zone..................E/a83843d8-85d0-4d77-ba4e-1e72f54e86c6
    }

    private ImplementedServicePort buildImplementedServicePort(final VariableElement aVariableElement) {
//begin of modifiable zone................T/1f8a2cd4-1603-46b1-86b8-92cd1fbba7c5
        ImplementedServicePort isp = new ImplementedServicePort();
        isp.setModifiers(aVariableElement.getModifiers());
        isp.setClazz(JavaxLangModelUtils.typeMirrorToClass(aVariableElement.asType()));
        isp.setName(aVariableElement.getSimpleName().toString());
        isp.setRemotable(aVariableElement.getAnnotation(ImplementedService.class).remotable());
//end of modifiable zone..................E/1f8a2cd4-1603-46b1-86b8-92cd1fbba7c5
//begin of modifiable zone................T/545cebd4-9854-4266-a6d8-0f1eb48b81f4
        return isp;
//end of modifiable zone..................E/545cebd4-9854-4266-a6d8-0f1eb48b81f4
    }

    private UsedServicePort buildUsedServicePort(final VariableElement aVariableElement) {
//begin of modifiable zone................T/f0e7ade6-6906-4770-b56b-b9a2dfb0827f
        UsedServicePort usp = new UsedServicePort();
        usp.setModifiers(aVariableElement.getModifiers());
        usp.setCardinalityMany("java.util.Map".equals(this.types.erasure(aVariableElement.asType()).toString()));
        if (usp.isCardinalityMany()) {
            List<? extends TypeMirror> mapTypeArguments = ((DeclaredType) aVariableElement.asType()).getTypeArguments();
            usp.setClazz(JavaxLangModelUtils.typeMirrorToClass(mapTypeArguments.get(1)));
        }
        else {
            usp.setClazz(JavaxLangModelUtils.typeMirrorToClass(aVariableElement.asType()));
        }
        usp.setName(aVariableElement.getSimpleName().toString());
//end of modifiable zone..................E/f0e7ade6-6906-4770-b56b-b9a2dfb0827f
//begin of modifiable zone................T/592da4e5-6fa1-4b0e-88d9-ec7620aae70a
        return usp;
//end of modifiable zone..................E/592da4e5-6fa1-4b0e-88d9-ec7620aae70a
    }

    private EventsSourcePort buildEventsSourcePort(final VariableElement aVariableElement) {
//begin of modifiable zone................T/23b4a995-9fec-4235-8cef-fa63bf84b075
        EventsSourcePort esp = new EventsSourcePort();
        esp.setModifiers(aVariableElement.getModifiers());
        List<? extends TypeMirror> sourceTypes = ((DeclaredType) aVariableElement.asType()).getTypeArguments();
        esp.setClazz(JavaxLangModelUtils.typeMirrorToClass(sourceTypes.get(0)));
        esp.setName(aVariableElement.getSimpleName().toString());
//end of modifiable zone..................E/23b4a995-9fec-4235-8cef-fa63bf84b075
//begin of modifiable zone................T/4f4d91a6-2c4b-4a57-b0fc-e4970d946a96
        return esp;
//end of modifiable zone..................E/4f4d91a6-2c4b-4a57-b0fc-e4970d946a96
    }

    private EventsSinkPort buildEventsSinkPort(final VariableElement aVariableElement) {
//begin of modifiable zone................T/f1eadc3d-5ad7-4c85-b4ca-00b69dc3524f
        EventsSinkPort esp = new EventsSinkPort();
        esp.setModifiers(aVariableElement.getModifiers());
        List<? extends TypeMirror> sourceTypes = ((DeclaredType) aVariableElement.asType()).getTypeArguments();
        esp.setClazz(JavaxLangModelUtils.typeMirrorToClass(sourceTypes.get(0)));
        esp.setName(aVariableElement.getSimpleName().toString());
//end of modifiable zone..................E/f1eadc3d-5ad7-4c85-b4ca-00b69dc3524f
//begin of modifiable zone................T/2374429c-37f0-4068-af26-e99ac62279ed
        return esp;
//end of modifiable zone..................E/2374429c-37f0-4068-af26-e99ac62279ed
    }

    private ParametersPort buildParametersPort(final VariableElement aVariableElement) {
//begin of modifiable zone................T/6492b647-f891-4d3a-a1b8-9c596ba3b3b4
        ParametersPort pp = new ParametersPort();
        pp.setModifiers(aVariableElement.getModifiers());
        List<? extends TypeMirror> parametersType = ((DeclaredType) aVariableElement.asType()).getTypeArguments();
        pp.setClazz(JavaxLangModelUtils.typeMirrorToClass(parametersType.get(0)));
        pp.setName(aVariableElement.getSimpleName().toString());
//end of modifiable zone..................E/6492b647-f891-4d3a-a1b8-9c596ba3b3b4
//begin of modifiable zone................T/29df0eaf-eda6-45f3-b8ad-d8df3223fa1f
        return pp;
//end of modifiable zone..................E/29df0eaf-eda6-45f3-b8ad-d8df3223fa1f
    }

    private boolean checkTypeInterfaceImplementation(final TypeElement typeElement, final Class<?> interfaceClass) {
//begin of modifiable zone................T/e9df971a-289b-4b30-8de1-de9bb38b981b
        boolean anyMatch = typeElement.getInterfaces().stream()
                .anyMatch(typeMirror -> typeMirror.toString().equals(interfaceClass.getCanonicalName()));
//end of modifiable zone..................E/e9df971a-289b-4b30-8de1-de9bb38b981b
//begin of modifiable zone................T/13d0d3c0-8321-444a-b572-a2914cc301ac
        return anyMatch;
//end of modifiable zone..................E/13d0d3c0-8321-444a-b572-a2914cc301ac
    }

}
