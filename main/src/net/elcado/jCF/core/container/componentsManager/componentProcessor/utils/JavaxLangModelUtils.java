/*
 * Copyright (c). Frédéric Cadier (2014-2018)
 *
 * This software is a computer program whose purpose is to allow component programming in pure java.
 *
 * This software is governed by the CeCILL-C license under French law and abiding by the rules of distribution of free software. You can use, modify and/or redistribute the software under the terms of the CeCILL-C license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy, modify and redistribute granted by the license, users are provided only with a limited warranty and the software's author, the holder of the economic rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, that may mean that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured and, more generally, to use and operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had knowledge of the CeCILL-C license and that you accept its terms.
 */

package net.elcado.jCF.core.container.componentsManager.componentProcessor.utils;

import java.io.PrintWriter;
import java.io.StringWriter;
import javax.lang.model.element.TypeElement;
import javax.lang.model.type.TypeMirror;

public class JavaxLangModelUtils {
    public static Class<?> typeElementToClass(final TypeElement typeElement) {
//begin of modifiable zone................T/aff19542-6976-4a22-8cf6-e98ab63d0773
        Class<?> elementClass = null;
        try {
            elementClass = Class.forName(typeElement.getQualifiedName().toString());
        } catch (ClassNotFoundException e) {
            StringWriter errors = new StringWriter();
            e.printStackTrace(new PrintWriter(errors));
        }
//end of modifiable zone..................E/aff19542-6976-4a22-8cf6-e98ab63d0773
//begin of modifiable zone................T/3bfa656f-ec17-497f-86db-66cc8771fcd9
        return elementClass;
//end of modifiable zone..................E/3bfa656f-ec17-497f-86db-66cc8771fcd9
    }

    public static Class<?> typeMirrorToClass(final TypeMirror typeMirror) {
//begin of modifiable zone................T/4713e958-2ed2-4725-80d6-ed230ded2d8c
        Class<?> elementClass = null;
        try {
            elementClass = Class.forName(typeMirror.toString());
        } catch (ClassNotFoundException e) {
            StringWriter errors = new StringWriter();
            e.printStackTrace(new PrintWriter(errors));
        }
//end of modifiable zone..................E/4713e958-2ed2-4725-80d6-ed230ded2d8c
//begin of modifiable zone................T/27cb0cdf-fdd1-4105-806e-5252f485d990
        return elementClass;
//end of modifiable zone..................E/27cb0cdf-fdd1-4105-806e-5252f485d990
    }

}
