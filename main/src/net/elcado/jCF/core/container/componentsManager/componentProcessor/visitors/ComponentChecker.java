/*
 * Copyright (c). Frédéric Cadier (2014-2018)
 *
 * This software is a computer program whose purpose is to allow component programming in pure java.
 *
 * This software is governed by the CeCILL-C license under French law and abiding by the rules of distribution of free software. You can use, modify and/or redistribute the software under the terms of the CeCILL-C license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy, modify and redistribute granted by the license, users are provided only with a limited warranty and the software's author, the holder of the economic rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, that may mean that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured and, more generally, to use and operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had knowledge of the CeCILL-C license and that you accept its terms.
 */

package net.elcado.jCF.core.container.componentsManager.componentProcessor.visitors;

import java.lang.annotation.Annotation;
import java.util.List;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.MirroredTypesException;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.ElementKindVisitor8;
import javax.lang.model.util.Types;
import net.elcado.jCF.core.annotations.*;
import net.elcado.jCF.core.container.componentsManager.componentProcessor.utils.JavaxLangModelUtils;
import net.elcado.jCF.core.lang.component.Component;
import net.elcado.jCF.core.lang.instance.InstanceId;
import net.elcado.jCF.core.lang.instance.events.Sink;
import net.elcado.jCF.core.lang.instance.events.Source;
import net.elcado.jCF.core.lang.instance.parameters.Parameters;

public class ComponentChecker extends ElementKindVisitor8<Component,Void> {
    private final Types types;

    public static boolean assertHasAnnotation(final Class<?> aClass, final Class<? extends Annotation> annotationClass) {
//begin of modifiable zone................T/2682edf1-1248-4930-9710-3d8ef9a10c6d
        Annotation annotation = aClass.getAnnotation(annotationClass);
//end of modifiable zone..................E/2682edf1-1248-4930-9710-3d8ef9a10c6d
//begin of modifiable zone................T/e99b4338-b797-4ed4-b735-f45cc747d1d2
        return annotation != null;
//end of modifiable zone..................E/e99b4338-b797-4ed4-b735-f45cc747d1d2
    }

    public ComponentChecker(final Types anTypeUtils) {
//begin of modifiable zone(JavaSuper).....C/778a6267-a333-4019-86bf-cb654cbd8458

//end of modifiable zone(JavaSuper).......E/778a6267-a333-4019-86bf-cb654cbd8458
//begin of modifiable zone................T/1986d142-9f47-4453-b886-f73962b10a65
        this.types = anTypeUtils;
//end of modifiable zone..................E/1986d142-9f47-4453-b886-f73962b10a65
    }

    @Override
    public Component visitTypeAsClass(final TypeElement typeElement, final Void unused) {
//begin of modifiable zone................T/56a8a1c6-6fda-4b93-a4d6-f2ff0eb980e2
        net.elcado.jCF.core.annotations.Component annotation = typeElement.getAnnotation(net.elcado.jCF.core.annotations.Component.class);
        if (annotation != null) {
            assert JavaxLangModelUtils.typeElementToClass(typeElement) != null : "cannot get class for TypeElement " + typeElement.toString();
        
            // check composed components
            try {
                // provoke exception...
                annotation.composition();
            } catch (MirroredTypesException mte) {
                // ... to get type mirrors in exception
                List<? extends TypeMirror> typeMirrors = mte.getTypeMirrors();
                for (TypeMirror composedType : typeMirrors) {
                    TypeElement composedElement = (TypeElement) this.types.asElement(composedType);
                    this.visitTypeAsClass(composedElement, unused);
                }
            }
        
            // visit enclosed elements
            typeElement.getEnclosedElements().forEach(enclosedElement -> enclosedElement.accept(this, unused));
        }
//end of modifiable zone..................E/56a8a1c6-6fda-4b93-a4d6-f2ff0eb980e2
//begin of modifiable zone................T/c148544b-737b-4d0b-97c1-6d75f72b2d48
        return this.DEFAULT_VALUE;
//end of modifiable zone..................E/c148544b-737b-4d0b-97c1-6d75f72b2d48
    }

    @Override
    public Component visitVariableAsField(final VariableElement aVariableElement, final Void unused) {
//begin of modifiable zone................T/1e4bbc12-3497-4722-bdc5-ee52c098c3fe
        // check id field
        if (aVariableElement.getAnnotation(Id.class) != null) {
            this.checkIdField(aVariableElement);
        }
        
        // check implemented service
        if (aVariableElement.getAnnotation(ImplementedService.class) != null) {
            this.checkImplementedService(aVariableElement);
        }
        
        // check used service
        if (aVariableElement.getAnnotation(UsedService.class) != null) {
            this.checkUsedService(aVariableElement);
        }
        
        // check events source
        if (aVariableElement.getAnnotation(EventsSource.class) != null) {
            this.checkEventsSource(aVariableElement);
        }
        
        // check events sink
        if (aVariableElement.getAnnotation(EventsSink.class) != null) {
            this.checkEventsSink(aVariableElement);
        }
        
        // check parameters
        if (aVariableElement.getAnnotation(ParametersService.class) != null) {
            this.checkParametersField(aVariableElement);
        }
//end of modifiable zone..................E/1e4bbc12-3497-4722-bdc5-ee52c098c3fe
//begin of modifiable zone................T/8ce924e1-7331-4902-be7d-aa17966083be
        return this.DEFAULT_VALUE;
//end of modifiable zone..................E/8ce924e1-7331-4902-be7d-aa17966083be
    }

    public void checkIdField(final VariableElement aVariableElement) {
//begin of modifiable zone................T/3364b802-114f-4e3c-80c3-46783955c4eb
        assert InstanceId.class.equals(JavaxLangModelUtils.typeMirrorToClass(aVariableElement.asType())) : "an @Id field must be typed as " + InstanceId.class + ": " + aVariableElement.toString();
        assert aVariableElement.getModifiers().contains(Modifier.PRIVATE) : "an @Id field must be private: " + aVariableElement.toString();
        assert aVariableElement.getModifiers().contains(Modifier.FINAL) : "an @Id field must be final: " + aVariableElement.toString();
        assert aVariableElement.getModifiers().contains(Modifier.TRANSIENT) : "an @Id field must be transient: " + aVariableElement.toString();
//end of modifiable zone..................E/3364b802-114f-4e3c-80c3-46783955c4eb
    }

    public void checkImplementedService(final VariableElement aVariableElement) {
//begin of modifiable zone................T/0a6a4801-d034-4061-83e9-1bec9f3af84b
        assert JavaxLangModelUtils.typeMirrorToClass(aVariableElement.asType()) != null : "cannot get class for @ImplementedService: " + aVariableElement.toString();
        assert aVariableElement.getModifiers().contains(Modifier.FINAL) : "an @ImplementedService must be final: " + aVariableElement.toString();
        assert aVariableElement.getModifiers().contains(Modifier.TRANSIENT) : "an @ImplementedService must be transient: " + aVariableElement.toString();
//end of modifiable zone..................E/0a6a4801-d034-4061-83e9-1bec9f3af84b
    }

    public void checkUsedService(final VariableElement aVariableElement) {
//begin of modifiable zone................T/db0e5a9b-fdd6-4f1e-bf4b-df67d51ed94a
        TypeMirror variableElementAsType = aVariableElement.asType();
        if (java.util.Map.class.getName().equals(this.types.erasure(variableElementAsType).toString())) {
            // used services is a map of used service
            // -> check first type argument is typed InstanceId
            // -> check second type argument is a reachable class
            // NOTE: we can safely cast to DeclaredType because annotation can only be set on fields
            List<? extends TypeMirror> mapTypeArguments = ((DeclaredType) aVariableElement.asType()).getTypeArguments();
            Class<?> firstTypeArgument = JavaxLangModelUtils.typeMirrorToClass(mapTypeArguments.get(0));
            Class<?> secondTypeArgument = JavaxLangModelUtils.typeMirrorToClass(mapTypeArguments.get(1));
        
            assert InstanceId.class.isAssignableFrom(firstTypeArgument) : "";
            assert secondTypeArgument != null : "cannot get parameterized class for @UsedService " + aVariableElement.toString();
        }
        else {
            // check that element is typed with a reachable class
            assert JavaxLangModelUtils.typeMirrorToClass(variableElementAsType) != null : "cannot get class for @UsedService " + aVariableElement.toString();
        }
        
        // check element declaration
        assert aVariableElement.getModifiers().contains(Modifier.PRIVATE) : "a @UsedService must be private: " + aVariableElement.toString();
        assert aVariableElement.getModifiers().contains(Modifier.FINAL) : "a @UsedService must be final: " + aVariableElement.toString();
        assert aVariableElement.getModifiers().contains(Modifier.TRANSIENT) : "a @UsedService must be transient: " + aVariableElement.toString();
//end of modifiable zone..................E/db0e5a9b-fdd6-4f1e-bf4b-df67d51ed94a
    }

    public void checkEventsSource(final VariableElement aVariableElement) {
//begin of modifiable zone................T/a8e00001-a21e-49a6-897e-3a2220711ebd
        // check that source is a source
        assert Source.class.getName().equals(this.types.erasure(aVariableElement.asType()).toString()) : "an @EventsSource must be typed as " + Source.class + ": "+ aVariableElement.toString();
        // check that source data is typed with a reachable class
        List<? extends TypeMirror> sourceTypes = ((DeclaredType) aVariableElement.asType()).getTypeArguments();
        Class<?> sourceDataClass = JavaxLangModelUtils.typeMirrorToClass(sourceTypes.get(0));
        assert sourceDataClass != null : "cannot get event data class for @EventsSource " + aVariableElement.toString();
        assert aVariableElement.getModifiers().contains(Modifier.FINAL) : "an @EventsSource must be final: " + aVariableElement.toString();
        assert aVariableElement.getModifiers().contains(Modifier.TRANSIENT) : "an @EventsSource must be transient: " + aVariableElement.toString();
//end of modifiable zone..................E/a8e00001-a21e-49a6-897e-3a2220711ebd
    }

    public void checkEventsSink(final VariableElement aVariableElement) {
//begin of modifiable zone................T/335856d4-70e6-4384-9ade-21793de5e7e6
        // check that sink is a sink
        assert Sink.class.getName().equals(this.types.erasure(aVariableElement.asType()).toString()) : "an @EventsSink must be typed as " + Sink.class + ": "+ aVariableElement.toString();
        // check that sink data is typed with a reachable class
        List<? extends TypeMirror> sinkTypes = ((DeclaredType) aVariableElement.asType()).getTypeArguments();
        Class<?> sinkDataClass = JavaxLangModelUtils.typeMirrorToClass(sinkTypes.get(0));
        assert sinkDataClass != null : "cannot get event data class for @EventsSink" + aVariableElement.toString();
        assert aVariableElement.getModifiers().contains(Modifier.PRIVATE) : "an @EventsSink must be private: " + aVariableElement.toString();
        assert aVariableElement.getModifiers().contains(Modifier.FINAL) : "an @EventsSink must be final: " + aVariableElement.toString();
        assert aVariableElement.getModifiers().contains(Modifier.TRANSIENT) : "an @EventsSink must be transient: " + aVariableElement.toString();
//end of modifiable zone..................E/335856d4-70e6-4384-9ade-21793de5e7e6
    }

    public void checkParametersField(final VariableElement aVariableElement) {
//begin of modifiable zone................T/af87ca1a-8b4d-4147-ad1c-0555256a2dc6
        assert Parameters.class.getName().equals(this.types.erasure(aVariableElement.asType()).toString()) : "a @ParametersService field must be typed as " + Parameters.class + ": " + aVariableElement.toString();
        assert aVariableElement.getModifiers().contains(Modifier.PRIVATE) : "a @ParametersService field must be private: " + aVariableElement.toString();
        assert aVariableElement.getModifiers().contains(Modifier.FINAL) : "a @ParametersService field must be final: " + aVariableElement.toString();
        assert aVariableElement.getModifiers().contains(Modifier.TRANSIENT) : "a @ParametersService field must be transient: " + aVariableElement.toString();
//end of modifiable zone..................E/af87ca1a-8b4d-4147-ad1c-0555256a2dc6
    }

}
