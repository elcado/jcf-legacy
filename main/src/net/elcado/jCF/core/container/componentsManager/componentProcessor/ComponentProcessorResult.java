/*
 * Copyright (c). Frédéric Cadier (2014-2018)
 *
 * This software is a computer program whose purpose is to allow component programming in pure java.
 *
 * This software is governed by the CeCILL-C license under French law and abiding by the rules of distribution of free software. You can use, modify and/or redistribute the software under the terms of the CeCILL-C license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy, modify and redistribute granted by the license, users are provided only with a limited warranty and the software's author, the holder of the economic rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, that may mean that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured and, more generally, to use and operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had knowledge of the CeCILL-C license and that you accept its terms.
 */

package net.elcado.jCF.core.container.componentsManager.componentProcessor;

import java.util.List;
import javax.tools.Diagnostic;
import javax.tools.JavaFileObject;
import net.elcado.jCF.core.lang.component.Component;

public class ComponentProcessorResult {
    private final Component component;

    private final List<Diagnostic<? extends JavaFileObject>> diagnostics;

    public ComponentProcessorResult(final Component aComponent, final List<Diagnostic<? extends JavaFileObject>> someDiagnostics) {
//begin of modifiable zone(JavaSuper).....C/f70fdc7e-6a60-4b33-a6dd-7b776ae7883c

//end of modifiable zone(JavaSuper).......E/f70fdc7e-6a60-4b33-a6dd-7b776ae7883c
//begin of modifiable zone................T/aafcdb84-33a7-40b6-bf39-3e6609448375
        this.component = aComponent;
        this.diagnostics = someDiagnostics;
//end of modifiable zone..................E/aafcdb84-33a7-40b6-bf39-3e6609448375
    }

    public Component getComponent() {
//begin of modifiable zone(JavaCode)......C/f621166c-c4ac-416d-985b-b7687b52cda1

//end of modifiable zone(JavaCode)........E/f621166c-c4ac-416d-985b-b7687b52cda1
//begin of modifiable zone................T/623812be-a3f3-4ae2-88db-cd769ccc7b56
        return this.component;
//end of modifiable zone..................E/623812be-a3f3-4ae2-88db-cd769ccc7b56
    }

    public List<Diagnostic<? extends JavaFileObject>> getDiagnostics() {
//begin of modifiable zone(JavaCode)......C/bc7d39bd-1150-4f10-8ecf-f9fbccd87b80

//end of modifiable zone(JavaCode)........E/bc7d39bd-1150-4f10-8ecf-f9fbccd87b80
//begin of modifiable zone................T/fa736c09-7448-44fb-8ce9-ef1ad7bbad72
        return this.diagnostics;
//end of modifiable zone..................E/fa736c09-7448-44fb-8ce9-ef1ad7bbad72
    }

}
