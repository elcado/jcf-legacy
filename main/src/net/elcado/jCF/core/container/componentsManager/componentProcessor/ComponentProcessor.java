/*
 * Copyright (c). Frédéric Cadier (2014-2018)
 *
 * This software is a computer program whose purpose is to allow component programming in pure java.
 *
 * This software is governed by the CeCILL-C license under French law and abiding by the rules of distribution of free software. You can use, modify and/or redistribute the software under the terms of the CeCILL-C license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy, modify and redistribute granted by the license, users are provided only with a limited warranty and the software's author, the holder of the economic rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, that may mean that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured and, more generally, to use and operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had knowledge of the CeCILL-C license and that you accept its terms.
 */

package net.elcado.jCF.core.container.componentsManager.componentProcessor;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.Messager;
import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.RoundEnvironment;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.TypeElement;
import javax.lang.model.util.Types;
import javax.tools.Diagnostic.Kind;
import javax.tools.DiagnosticCollector;
import javax.tools.JavaCompiler.CompilationTask;
import javax.tools.JavaFileObject;
import javax.tools.ToolProvider;
import net.elcado.jCF.core.container.componentsManager.componentProcessor.visitors.ComponentBuilder;
import net.elcado.jCF.core.container.componentsManager.componentProcessor.visitors.ComponentChecker;
import net.elcado.jCF.core.lang.component.Component;

public class ComponentProcessor extends AbstractProcessor {
    private Types types;

    private Messager messager;

    private Component component;

    /**
     * Discover the supplied component.
     * @param componentClass the class to discover as a component
     * @return the result of the discovery process
     */
    public static ComponentProcessorResult discoverComponent(Class<?> componentClass) {
//begin of modifiable zone................T/1ddb1b75-9553-47d9-9825-34355798045c
        assert ComponentChecker.assertHasAnnotation(componentClass, net.elcado.jCF.core.annotations.Component.class) : "missing @Component on component " + componentClass;
        
        ComponentProcessor componentProcessor = new ComponentProcessor();
        
        List<String> classesNames = Stream.<Class>of(componentClass)
                .map(Class::getCanonicalName)
                .collect(Collectors.toList());
        
        DiagnosticCollector<JavaFileObject> diagnosticsCollector = new DiagnosticCollector<>();
        
        CompilationTask task = ToolProvider.getSystemJavaCompiler().getTask(
                null,
                null,
                diagnosticsCollector,
                null,
                classesNames,
                null);
        task.setProcessors(Collections.singletonList(componentProcessor));
        
        // process
        if (!task.call()) {
            componentProcessor.component = null;
        }
        
        // build result
        ComponentProcessorResult cpr = new ComponentProcessorResult(componentProcessor.component, diagnosticsCollector.getDiagnostics());
//end of modifiable zone..................E/1ddb1b75-9553-47d9-9825-34355798045c
//begin of modifiable zone................T/073e391d-7712-431d-801a-e31c0982ad2f
        return cpr;
//end of modifiable zone..................E/073e391d-7712-431d-801a-e31c0982ad2f
    }

    private ComponentProcessor() {
//begin of modifiable zone(JavaSuper).....C/b82294a2-131d-4720-a396-04438dd95709

//end of modifiable zone(JavaSuper).......E/b82294a2-131d-4720-a396-04438dd95709
//begin of modifiable zone(JavaCode)......C/b82294a2-131d-4720-a396-04438dd95709

//end of modifiable zone(JavaCode)........E/b82294a2-131d-4720-a396-04438dd95709
    }

    @Override
    public SourceVersion getSupportedSourceVersion() {
//begin of modifiable zone(JavaCode)......C/f64cc45f-9e33-40be-81be-3ab3a1b18e94

//end of modifiable zone(JavaCode)........E/f64cc45f-9e33-40be-81be-3ab3a1b18e94
//begin of modifiable zone................T/1c3a626b-0dfd-44cb-948e-6e76ad433e44
        return SourceVersion.latestSupported();
//end of modifiable zone..................E/1c3a626b-0dfd-44cb-948e-6e76ad433e44
    }

    @Override
    public Set<String> getSupportedAnnotationTypes() {
//begin of modifiable zone................T/574ebd79-1a4c-4ebb-b181-e5e248f2e7b0
        Set<String> annotations = new HashSet<>();
        annotations.add(net.elcado.jCF.core.annotations.Component.class.getCanonicalName());
//end of modifiable zone..................E/574ebd79-1a4c-4ebb-b181-e5e248f2e7b0
//begin of modifiable zone................T/9920aa1f-66ba-47ba-8db0-1da96b1aec10
        return annotations;
//end of modifiable zone..................E/9920aa1f-66ba-47ba-8db0-1da96b1aec10
    }

    @Override
    public void init(ProcessingEnvironment processingEnvironment) {
//begin of modifiable zone................T/645788ff-3ce7-4b4c-8044-568525ac50b9
        super.init(processingEnvironment);
        
                //        this.elements = processingEnvironment.getElementUtils();
                this.types = processingEnvironment.getTypeUtils();
                this.messager = processingEnvironment.getMessager();
//end of modifiable zone..................E/645788ff-3ce7-4b4c-8044-568525ac50b9
    }

    @Override
    public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
//begin of modifiable zone................T/429dd7cf-a7dc-435a-bc61-03bfd731fc12
        boolean result = true;
        
        for (Element rootElement : roundEnv.getRootElements()) {
            // init result
            this.component = null;
        
            try {
                assert (rootElement.getKind() == ElementKind.CLASS) : "component is to be a Class. Check definition of " + rootElement.toString();
        
                // first run checker if assertions are enabled
                assert rootElement.accept(new ComponentChecker(this.types), null) == null;
            }
            catch (AssertionError ae) {
                this.messager.printMessage(Kind.ERROR, (ae.getMessage() != null) ? ae.getMessage() : "");
                result = false;
                break;
            }
        
            // now we can build the meta component
            this.component = rootElement.accept(new ComponentBuilder(this.types, this.messager), new Component());
        }
//end of modifiable zone..................E/429dd7cf-a7dc-435a-bc61-03bfd731fc12
//begin of modifiable zone................T/86466488-3367-4c87-a5b6-409b5ae3ed20
        return result;
//end of modifiable zone..................E/86466488-3367-4c87-a5b6-409b5ae3ed20
    }

}
