/*
 * Copyright (c). Frédéric Cadier (2014-2018)
 *
 * This software is a computer program whose purpose is to allow component programming in pure java.
 *
 * This software is governed by the CeCILL-C license under French law and abiding by the rules of distribution of free software. You can use, modify and/or redistribute the software under the terms of the CeCILL-C license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy, modify and redistribute granted by the license, users are provided only with a limited warranty and the software's author, the holder of the economic rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, that may mean that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured and, more generally, to use and operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had knowledge of the CeCILL-C license and that you accept its terms.
 */

package net.elcado.jCF.core.lang.component;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.lang.model.element.Modifier;

public abstract class AbstractPort implements Serializable {
    protected Set<Modifier> modifiers = new HashSet<>();

    protected String name;

    protected Class<?> clazz;

    public Class<?> getClazz() {
//begin of modifiable zone(JavaCode)......C/5336016e-36fc-4dd6-9800-4c4104e3d4c6

//end of modifiable zone(JavaCode)........E/5336016e-36fc-4dd6-9800-4c4104e3d4c6
//begin of modifiable zone................T/94892648-9e2b-44a3-8230-356370adefd8
        return this.clazz;
//end of modifiable zone..................E/94892648-9e2b-44a3-8230-356370adefd8
    }

    public void setClazz(final Class<?> value) {
//begin of modifiable zone................T/a87ac150-0d72-4a39-8602-b07a17f8a777
        this.clazz = value;
//end of modifiable zone..................E/a87ac150-0d72-4a39-8602-b07a17f8a777
    }

    public Set<Modifier> getModifiers() {
//begin of modifiable zone(JavaCode)......C/4bc73420-e0ea-4f02-ac95-4403b381e524

//end of modifiable zone(JavaCode)........E/4bc73420-e0ea-4f02-ac95-4403b381e524
//begin of modifiable zone................T/25c6e121-08f9-4c19-9bd3-e6a2a319ada5
        return this.modifiers;
//end of modifiable zone..................E/25c6e121-08f9-4c19-9bd3-e6a2a319ada5
    }

    public void setModifiers(final Set<Modifier> value) {
//begin of modifiable zone................T/66734e13-21e2-41bd-89b2-0ca6e9b23679
        this.modifiers = value;
//end of modifiable zone..................E/66734e13-21e2-41bd-89b2-0ca6e9b23679
    }

    public String getName() {
//begin of modifiable zone(JavaCode)......C/940cd965-0c08-4314-a5bb-e418e8ac2825

//end of modifiable zone(JavaCode)........E/940cd965-0c08-4314-a5bb-e418e8ac2825
//begin of modifiable zone................T/d34b0667-7ec7-4c1f-8540-2e87916705d0
        return this.name;
//end of modifiable zone..................E/d34b0667-7ec7-4c1f-8540-2e87916705d0
    }

    public void setName(final String value) {
//begin of modifiable zone................T/33074305-7edc-451a-acc6-6a7c274183c2
        this.name = value;
//end of modifiable zone..................E/33074305-7edc-451a-acc6-6a7c274183c2
    }

}
