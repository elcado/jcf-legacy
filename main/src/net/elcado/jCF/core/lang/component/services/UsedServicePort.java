/*
 * Copyright (c). Frédéric Cadier (2014-2018)
 *
 * This software is a computer program whose purpose is to allow component programming in pure java.
 *
 * This software is governed by the CeCILL-C license under French law and abiding by the rules of distribution of free software. You can use, modify and/or redistribute the software under the terms of the CeCILL-C license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy, modify and redistribute granted by the license, users are provided only with a limited warranty and the software's author, the holder of the economic rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, that may mean that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured and, more generally, to use and operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had knowledge of the CeCILL-C license and that you accept its terms.
 */

package net.elcado.jCF.core.lang.component.services;


public class UsedServicePort extends ServicePort {
    private boolean cardinalityMany;

    public boolean isCardinalityMany() {
//begin of modifiable zone(JavaCode)......C/f76aea0e-dd61-4d12-b1c9-27df8c952bc6

//end of modifiable zone(JavaCode)........E/f76aea0e-dd61-4d12-b1c9-27df8c952bc6
//begin of modifiable zone................T/415f9f18-e978-44cd-bfd9-21712611f1bd
        return this.cardinalityMany;
//end of modifiable zone..................E/415f9f18-e978-44cd-bfd9-21712611f1bd
    }

    public void setCardinalityMany(final boolean value) {
//begin of modifiable zone................T/735a7102-e57d-4bfb-8495-e5e4cde8d7a5
        this.cardinalityMany = value;
//end of modifiable zone..................E/735a7102-e57d-4bfb-8495-e5e4cde8d7a5
    }

}
