/*
 * Copyright (c). Frédéric Cadier (2014-2018)
 *
 * This software is a computer program whose purpose is to allow component programming in pure java.
 *
 * This software is governed by the CeCILL-C license under French law and abiding by the rules of distribution of free software. You can use, modify and/or redistribute the software under the terms of the CeCILL-C license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy, modify and redistribute granted by the license, users are provided only with a limited warranty and the software's author, the holder of the economic rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, that may mean that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured and, more generally, to use and operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had knowledge of the CeCILL-C license and that you accept its terms.
 */

package net.elcado.jCF.core.lang.component;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import net.elcado.jCF.core.lang.component.events.EventsSinkPort;
import net.elcado.jCF.core.lang.component.events.EventsSourcePort;
import net.elcado.jCF.core.lang.component.id.IdPort;
import net.elcado.jCF.core.lang.component.parameters.ParametersPort;
import net.elcado.jCF.core.lang.component.services.ImplementedServicePort;
import net.elcado.jCF.core.lang.component.services.UsedServicePort;
import net.elcado.jCF.core.lang.instance.Instance;

public class Component implements Serializable {
    protected boolean remotable;

    protected boolean initializable;

    protected boolean startable;

    protected Class<?> clazz;

    protected final transient List<Component> composedComponents = new ArrayList<>();

    protected transient IdPort idPort;

    protected final transient List<ImplementedServicePort> implementedServicePorts = new ArrayList<>();

    protected final transient List<UsedServicePort> usedServicePorts = new ArrayList<>();

    protected final transient List<ParametersPort> parametersPorts = new ArrayList<>();

    protected final transient List<EventsSourcePort> eventsSourcePorts = new ArrayList<>();

    protected final transient List<EventsSinkPort> eventsSinkPorts = new ArrayList<>();

    protected final transient List<Instance> instances = new ArrayList<>();

    public boolean isRemotable() {
//begin of modifiable zone(JavaCode)......C/afcb5375-914e-4107-a200-9fda1943d495

//end of modifiable zone(JavaCode)........E/afcb5375-914e-4107-a200-9fda1943d495
//begin of modifiable zone................T/a0c4c5a5-b56e-471c-bcc3-35315ad97c2e
        return this.remotable;
//end of modifiable zone..................E/a0c4c5a5-b56e-471c-bcc3-35315ad97c2e
    }

    public void setRemotable(final boolean value) {
//begin of modifiable zone................T/d062397f-3267-4663-ad03-e878ddbbcb1e
        this.remotable = value;
//end of modifiable zone..................E/d062397f-3267-4663-ad03-e878ddbbcb1e
    }

    public boolean isInitializable() {
//begin of modifiable zone(JavaCode)......C/0239e581-6fa2-4aa9-9140-88c5047151ec

//end of modifiable zone(JavaCode)........E/0239e581-6fa2-4aa9-9140-88c5047151ec
//begin of modifiable zone................T/a20245e1-67b6-4d3a-98be-3c039ea82c2f
        return this.initializable;
//end of modifiable zone..................E/a20245e1-67b6-4d3a-98be-3c039ea82c2f
    }

    public void setInitializable(final boolean value) {
//begin of modifiable zone................T/3ff0435f-4ca5-43af-b91d-df0ffc1817c7
        this.initializable = value;
//end of modifiable zone..................E/3ff0435f-4ca5-43af-b91d-df0ffc1817c7
    }

    public boolean isStartable() {
//begin of modifiable zone(JavaCode)......C/ab311c36-c1a5-4bd9-9486-074104fdd9c3

//end of modifiable zone(JavaCode)........E/ab311c36-c1a5-4bd9-9486-074104fdd9c3
//begin of modifiable zone................T/888a730e-8f05-48aa-a897-0d82fa7d2dcd
        return this.startable;
//end of modifiable zone..................E/888a730e-8f05-48aa-a897-0d82fa7d2dcd
    }

    public void setStartable(final boolean value) {
//begin of modifiable zone................T/84b5e5a7-4a87-4e7e-9093-fd856636fbde
        this.startable = value;
//end of modifiable zone..................E/84b5e5a7-4a87-4e7e-9093-fd856636fbde
    }

    public Class<?> getClazz() {
//begin of modifiable zone(JavaCode)......C/04f92e2c-1781-4203-906e-ba0924551f4e

//end of modifiable zone(JavaCode)........E/04f92e2c-1781-4203-906e-ba0924551f4e
//begin of modifiable zone................T/0bdc9362-98f2-43a9-9ef0-32107f46c9ed
        return this.clazz;
//end of modifiable zone..................E/0bdc9362-98f2-43a9-9ef0-32107f46c9ed
    }

    public void setClazz(final Class<?> value) {
//begin of modifiable zone................T/a1d714f3-4b42-4228-8f01-968cf249de7a
        this.clazz = value;
//end of modifiable zone..................E/a1d714f3-4b42-4228-8f01-968cf249de7a
    }

    public List<Component> getComposedComponents() {
//begin of modifiable zone(JavaCode)......C/6191e97c-c676-49a2-a28c-79971bb00383

//end of modifiable zone(JavaCode)........E/6191e97c-c676-49a2-a28c-79971bb00383
//begin of modifiable zone................T/054d9a5d-78c5-4d0f-a114-efde0517dc02
        return this.composedComponents;
//end of modifiable zone..................E/054d9a5d-78c5-4d0f-a114-efde0517dc02
    }

    public void addComposedComponent(final Component aComposedComponent) {
//begin of modifiable zone................T/b0b9a981-3f77-4fc5-af34-805093cd4fce
        if (aComposedComponent != null)
            this.composedComponents.add(aComposedComponent);
//end of modifiable zone..................E/b0b9a981-3f77-4fc5-af34-805093cd4fce
    }

    public IdPort getIdPort() {
//begin of modifiable zone(JavaCode)......C/6ed610db-f305-43a9-b4d4-faef47fc79af

//end of modifiable zone(JavaCode)........E/6ed610db-f305-43a9-b4d4-faef47fc79af
//begin of modifiable zone................T/1ce0aea2-5459-4fca-bcf9-040e815d10b4
        return this.idPort;
//end of modifiable zone..................E/1ce0aea2-5459-4fca-bcf9-040e815d10b4
    }

    public void setIdPort(final IdPort value) {
//begin of modifiable zone................T/40a4c9fa-8dc7-420b-98ad-b99862d8b75c
        this.idPort = value;
//end of modifiable zone..................E/40a4c9fa-8dc7-420b-98ad-b99862d8b75c
    }

    public List<ImplementedServicePort> getImplementedServicePorts() {
//begin of modifiable zone(JavaCode)......C/b1f30ca2-996c-4f99-b04a-5cd69af25f6e

//end of modifiable zone(JavaCode)........E/b1f30ca2-996c-4f99-b04a-5cd69af25f6e
//begin of modifiable zone................T/f90f4d74-3234-46e3-8e8d-0cfaf3ac519f
        return this.implementedServicePorts;
//end of modifiable zone..................E/f90f4d74-3234-46e3-8e8d-0cfaf3ac519f
    }

    public void addImplementedServicePort(final ImplementedServicePort aImplementedServicePort) {
//begin of modifiable zone................T/9cca2415-7645-4cc2-b9eb-061295bcdd57
        if (aImplementedServicePort != null)
            this.implementedServicePorts.add(aImplementedServicePort);
//end of modifiable zone..................E/9cca2415-7645-4cc2-b9eb-061295bcdd57
    }

    public List<UsedServicePort> getUsedServicePorts() {
//begin of modifiable zone(JavaCode)......C/59a03895-3d7f-49c0-a0a8-79776a261693

//end of modifiable zone(JavaCode)........E/59a03895-3d7f-49c0-a0a8-79776a261693
//begin of modifiable zone................T/5183f037-64e9-487a-8ecf-8ca79cc362b4
        return this.usedServicePorts;
//end of modifiable zone..................E/5183f037-64e9-487a-8ecf-8ca79cc362b4
    }

    public void addUsedServicePort(final UsedServicePort aUsedServicePort) {
//begin of modifiable zone................T/19a9e5c2-748e-4669-a27f-130385f7dca4
        if (aUsedServicePort != null)
            this.usedServicePorts.add(aUsedServicePort);
//end of modifiable zone..................E/19a9e5c2-748e-4669-a27f-130385f7dca4
    }

    public List<EventsSourcePort> getEventsSourcePorts() {
//begin of modifiable zone(JavaCode)......C/2518dbd8-e4df-4dcc-8e25-0497703f2e6f

//end of modifiable zone(JavaCode)........E/2518dbd8-e4df-4dcc-8e25-0497703f2e6f
//begin of modifiable zone................T/5c1768f7-0728-4253-a293-4375bcfb437d
        return this.eventsSourcePorts;
//end of modifiable zone..................E/5c1768f7-0728-4253-a293-4375bcfb437d
    }

    public void addEventsSourcePort(final EventsSourcePort anEventsSourcePort) {
//begin of modifiable zone................T/e833751d-370b-491e-b139-d0fdf0d41638
        if (anEventsSourcePort != null)
            this.eventsSourcePorts.add(anEventsSourcePort);
//end of modifiable zone..................E/e833751d-370b-491e-b139-d0fdf0d41638
    }

    public List<EventsSinkPort> getEventsSinkPorts() {
//begin of modifiable zone(JavaCode)......C/cfe82bbe-4a94-4a86-91f9-e2abd553497c

//end of modifiable zone(JavaCode)........E/cfe82bbe-4a94-4a86-91f9-e2abd553497c
//begin of modifiable zone................T/2fa52cd6-cb3d-42b8-8153-ddd27373feff
        return this.eventsSinkPorts;
//end of modifiable zone..................E/2fa52cd6-cb3d-42b8-8153-ddd27373feff
    }

    public void addEventsSinkPort(final EventsSinkPort anEventsSinkPort) {
//begin of modifiable zone................T/426aa145-b33c-43d5-a8b3-7ad558597829
        if (anEventsSinkPort != null)
            this.eventsSinkPorts.add(anEventsSinkPort);
//end of modifiable zone..................E/426aa145-b33c-43d5-a8b3-7ad558597829
    }

    public List<ParametersPort> getParametersPorts() {
//begin of modifiable zone(JavaCode)......C/667452eb-1eae-4df9-8cd5-9cea3180cf82

//end of modifiable zone(JavaCode)........E/667452eb-1eae-4df9-8cd5-9cea3180cf82
//begin of modifiable zone................T/ff2ecc67-0ea6-404d-97a1-c3f129ab3f8c
        return this.parametersPorts;
//end of modifiable zone..................E/ff2ecc67-0ea6-404d-97a1-c3f129ab3f8c
    }

    public void addParametersPort(final ParametersPort aParameterPort) {
//begin of modifiable zone................T/1ed46f09-73f5-4b82-8208-cf13e42ad9f5
        if (aParameterPort != null)
            this.parametersPorts.add(aParameterPort);
//end of modifiable zone..................E/1ed46f09-73f5-4b82-8208-cf13e42ad9f5
    }

    public List<Instance> getInstances() {
//begin of modifiable zone(JavaCode)......C/d7d76037-b5ff-46ad-915a-5fb2242e3368

//end of modifiable zone(JavaCode)........E/d7d76037-b5ff-46ad-915a-5fb2242e3368
//begin of modifiable zone................T/164a67a7-c06b-41b4-9034-334711453a69
        return this.instances;
//end of modifiable zone..................E/164a67a7-c06b-41b4-9034-334711453a69
    }

    public void addInstance(final Instance instance) {
//begin of modifiable zone................T/7bbd939c-2239-4df6-95f1-86e39749eb95
        this.instances.add(instance);
//end of modifiable zone..................E/7bbd939c-2239-4df6-95f1-86e39749eb95
    }

    @Override
    public String toString() {
//begin of modifiable zone................T/4e61b753-15cd-498d-9bde-059daaee2f19
        String prefix = "|    ";
        String sepLineMaker = "<sep_line>";
        String subSepLineMaker = "<sub_sep_line>";
        
        StringBuilder bob = new StringBuilder();
        
        bob.append(sepLineMaker).append("\n");
        
        // component class
        bob.append(prefix).append("Class:    ").append(this.getClazz().toString()).append("\n");
        
        // is component stated ?
        bob.append(prefix).append("Stated:   ").append(this.isStartable()).append("\n");
        
        if (
        //                this.getParametersService() != null
        //            ||
                !this.getImplementedServicePorts().isEmpty()
            ||
                !this.getUsedServicePorts().isEmpty()
            ||
                !this.getEventsSourcePorts().isEmpty()
            ||
                !this.getEventsSinkPorts().isEmpty())
            bob.append(subSepLineMaker).append("\n");
        
        //        // parameters interface
        //        if (this.getParametersService() != null) {
        //            bob.append(prefix + "Parameters interface:\n");
        //            bob.append(prefix + "    " + this.getParametersService().getServiceType().getName());
        //            bob.append("\n");
        //        }
        
        // implemented services
        if ((!this.getImplementedServicePorts().isEmpty())) {
            bob.append(prefix).append("Implemented services:\n");
            for (ImplementedServicePort implService : this.getImplementedServicePorts()) {
                StringBuilder modifiersBob = new StringBuilder();
                implService.getModifiers().forEach(mod -> modifiersBob.append(mod.toString()).append(" "));
                modifiersBob.delete(modifiersBob.length() - 1, modifiersBob.length());
        
                bob.append(prefix).append("    ").append("(").append(modifiersBob).append(") ").append(implService.getClazz().getTypeName()).append(implService.isRemotable() ? " - REMOTABLE" : "");
                bob.append("\n");
            }
        }
        
        // used services
        if (!this.getUsedServicePorts().isEmpty()) {
            bob.append(prefix).append("Used services:\n");
            for (UsedServicePort usedService : this.getUsedServicePorts()) {
                StringBuilder modifiersBob = new StringBuilder();
                usedService.getModifiers().forEach(mod -> modifiersBob.append(mod.toString()).append(" "));
                modifiersBob.delete(modifiersBob.length() - 1, modifiersBob.length());
        
                bob.append(prefix).append("    ").append("(").append(modifiersBob).append(") ").append(usedService.getClazz().getTypeName()).append(usedService.isCardinalityMany() ? " []" : "");
                bob.append("\n");
            }
        }
        
        //        // plug services
        //        if (!this.getUsedServices().isEmpty()) {
        //            bob.append(prefix + "Plug services:\n");
        //            for (AbstractUsedServiceField usedService : this.getUsedServices()) {
        //                // filter
        //                if (usedService instanceof UsedServiceField && ((UsedServiceField) usedService).isPlug()) {
        //                    String modString = "";
        //                    switch (usedService.getModifiers()) {
        //                        case Modifier.PUBLIC:
        //                            modString = "public";
        //                            break;
        //                        case Modifier.PROTECTED:
        //                            modString = "protected";
        //                            break;
        //                        case Modifier.PRIVATE:
        //                            modString = "private";
        //                            break;
        //                    }
        //
        //                    bob.append(prefix + "    " + "(" + modString + ") " + usedService.getServiceType().getName());
        //                    bob.append("\n");
        //                }
        //            }
        //        }
        
        // event sources
        if (!this.getEventsSourcePorts().isEmpty()) {
            bob.append(prefix).append("Events sources:\n");
            for (EventsSourcePort eventsSourcePort : this.getEventsSourcePorts()) {
                StringBuilder modifiersBob = new StringBuilder();
                eventsSourcePort.getModifiers().forEach(mod -> modifiersBob.append(mod.toString()).append(" "));
                modifiersBob.delete(modifiersBob.length() - 1, modifiersBob.length());
        
                bob.append(prefix).append("    ").append("(").append(modifiersBob).append(") ").append(eventsSourcePort.getClazz().getTypeName());
                bob.append("\n");
            }
        }
        
        // event sinks
        if (!this.getEventsSinkPorts().isEmpty()) {
            bob.append(prefix).append("Events sinks:\n");
            for (EventsSinkPort eventsSinkPort : this.getEventsSinkPorts()) {
                StringBuilder modifiersBob = new StringBuilder();
                eventsSinkPort.getModifiers().forEach(mod -> modifiersBob.append(mod.toString()).append(" "));
                modifiersBob.delete(modifiersBob.length() - 1, modifiersBob.length());
        
                bob.append(prefix).append("    ").append("(").append(modifiersBob).append(") ").append(eventsSinkPort.getClazz().getTypeName());
                bob.append("\n");
            }
        }
        
        // composed components
        if (!this.getComposedComponents().isEmpty()) {
            bob.append(subSepLineMaker).append("\n");
            bob.append(prefix).append("Composed components:").append("\n");
        
            for (Component composedComponent : this.getComposedComponents()) {
                // get sub description
                String subDescString = composedComponent.toString();
                // append prefix at each new line of sub description
                subDescString = prefix + subDescString.replaceAll("\n", "\n" + prefix);
                // remove last prefix
                subDescString = subDescString.substring(0, subDescString.length() - prefix.length());
        
                bob.append(subDescString).append("\n");
            }
        }
        
        bob.append(sepLineMaker);
        
        /*
         * post-production
         */
        
        // extract lines
        String[] lines = bob.toString().split("\n");
        
        // get max line length
        int max = 0;
        for (String line : lines) {
            int length = line.length();
            if (length > max)
                max = length;
        }
        max += 5;
        
        // build the sep & sub sep lines
        StringBuilder sepLineString = new StringBuilder();
        for (int i = 0; i < max; i++)
            sepLineString.append("=");
        
        StringBuilder subSepLineString = new StringBuilder("|");
        for (int i = 1; i < max - 1; i++)
            subSepLineString.append("-");
        subSepLineString.append("|");
        
        // rebuilt one single string w/ normalized lines and replaced markers
        StringBuilder result = new StringBuilder();
        for (String line : lines) {
            // test sep line marker
            if (line.contains(sepLineMaker))
                result.append(sepLineString).append("\n");
            else if (line.contains(subSepLineMaker))
                result.append(subSepLineString).append("\n");
            else {
                // add line
                result.append(line);
        
                // normalize line
                int lineLength = line.length();
                if (lineLength == 0)
                    result.append("|");
                else
                    result.append(" ");
                if (lineLength < max)
                    for (int i = 1; i < max - lineLength - 1; i++)
                        result.append(" ");
                result.append("|\n");
            }
        }
//end of modifiable zone..................E/4e61b753-15cd-498d-9bde-059daaee2f19
//begin of modifiable zone................T/ddd444dc-9158-4976-8018-d11491aff8c1
        return result.toString();
//end of modifiable zone..................E/ddd444dc-9158-4976-8018-d11491aff8c1
    }

}
