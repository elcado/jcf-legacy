/*
 * Copyright (c). Frédéric Cadier (2014-2018)
 *
 * This software is a computer program whose purpose is to allow component programming in pure java.
 *
 * This software is governed by the CeCILL-C license under French law and abiding by the rules of distribution of free software. You can use, modify and/or redistribute the software under the terms of the CeCILL-C license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy, modify and redistribute granted by the license, users are provided only with a limited warranty and the software's author, the holder of the economic rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, that may mean that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured and, more generally, to use and operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had knowledge of the CeCILL-C license and that you accept its terms.
 */

package net.elcado.jCF.core.lang.component.services;


public class ImplementedServicePort extends ServicePort {
    private boolean remotable;

    public boolean isRemotable() {
//begin of modifiable zone................T/3c657360-7920-46c6-a210-33b1e24cacb8
        
//end of modifiable zone..................E/3c657360-7920-46c6-a210-33b1e24cacb8
//begin of modifiable zone................T/e1c49f42-9061-41aa-9b8c-ed9f160fbb9f
        return this.remotable;
//end of modifiable zone..................E/e1c49f42-9061-41aa-9b8c-ed9f160fbb9f
    }

    public void setRemotable(final boolean value) {
//begin of modifiable zone................T/6ed8abef-929a-4639-adb1-50ab4741b49b
        this.remotable = value;
//end of modifiable zone..................E/6ed8abef-929a-4639-adb1-50ab4741b49b
    }

}
