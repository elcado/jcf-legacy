/*
 * Copyright (c). Frédéric Cadier (2014-2018)
 *
 * This software is a computer program whose purpose is to allow component programming in pure java.
 *
 * This software is governed by the CeCILL-C license under French law and abiding by the rules of distribution of free software. You can use, modify and/or redistribute the software under the terms of the CeCILL-C license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy, modify and redistribute granted by the license, users are provided only with a limited warranty and the software's author, the holder of the economic rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, that may mean that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured and, more generally, to use and operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had knowledge of the CeCILL-C license and that you accept its terms.
 */

package net.elcado.jCF.core.lang.instance.services;

import java.util.ArrayList;
import java.util.List;
import net.elcado.jCF.core.lang.component.services.ImplementedServicePort;
import net.elcado.jCF.core.lang.component.services.ServicePort;
import net.elcado.jCF.core.lang.instance.Instance;

public class ImplementedService extends AbstractService {
    /**
     * JGroups' address for remotable services
     */
    private String address;

    protected final List<UsedService> usedServices = new ArrayList<>();

    public ImplementedService(final Instance instance, final ServicePort port) {
//begin of modifiable zone(JavaSuper).....C/ae52521d-1076-4fad-af58-973382d92bdc

//end of modifiable zone(JavaSuper).......E/ae52521d-1076-4fad-af58-973382d92bdc
//begin of modifiable zone................T/c52a275f-744d-4bce-8805-00cb838d2500
        super(instance, port);
//end of modifiable zone..................E/c52a275f-744d-4bce-8805-00cb838d2500
    }

    public List<UsedService> getUsedServices() {
//begin of modifiable zone(JavaCode)......C/0242e9ba-66e8-4907-8526-3054bdcab49e

//end of modifiable zone(JavaCode)........E/0242e9ba-66e8-4907-8526-3054bdcab49e
//begin of modifiable zone................T/c57bf374-353a-4728-a564-e24bdca19360
        return this.usedServices;
//end of modifiable zone..................E/c57bf374-353a-4728-a564-e24bdca19360
    }

    public void addUsedService(final UsedService value) {
//begin of modifiable zone................T/3f919c3c-b8fe-4fd7-bb0d-6fd6eaf345c3
        this.usedServices.add(value);
//end of modifiable zone..................E/3f919c3c-b8fe-4fd7-bb0d-6fd6eaf345c3
    }

    public void removeUsedService(final UsedService value) {
//begin of modifiable zone................T/282a4482-ea4d-48cc-a639-f314b6006a34
        this.usedServices.remove(value);
//end of modifiable zone..................E/282a4482-ea4d-48cc-a639-f314b6006a34
    }

    public boolean isRemotable() {
//begin of modifiable zone................T/7974bc21-ed3b-4c7b-9553-5fcecb30c8a4
        boolean result = false;
        if (this.port instanceof ImplementedServicePort) {
            result = ((ImplementedServicePort) this.port).isRemotable();
        }
//end of modifiable zone..................E/7974bc21-ed3b-4c7b-9553-5fcecb30c8a4
//begin of modifiable zone................T/3b433845-b9f0-4657-99b7-d31080f803bc
        return result;
//end of modifiable zone..................E/3b433845-b9f0-4657-99b7-d31080f803bc
    }

    public String getAddress() {
//begin of modifiable zone(JavaCode)......C/cb1af2b4-3756-4a6a-82c1-d926ab92c855

//end of modifiable zone(JavaCode)........E/cb1af2b4-3756-4a6a-82c1-d926ab92c855
//begin of modifiable zone................T/abca7979-c12e-4240-ad14-b8c844f9cedd
        return this.address;
//end of modifiable zone..................E/abca7979-c12e-4240-ad14-b8c844f9cedd
    }

    public void setAddress(final String value) {
//begin of modifiable zone................T/63300aa3-d1b1-4920-8493-733b2129174c
        this.address = value;
//end of modifiable zone..................E/63300aa3-d1b1-4920-8493-733b2129174c
    }

}
