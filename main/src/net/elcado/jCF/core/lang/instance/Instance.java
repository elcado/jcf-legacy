/*
 * Copyright (c). Frédéric Cadier (2014-2018)
 *
 * This software is a computer program whose purpose is to allow component programming in pure java.
 *
 * This software is governed by the CeCILL-C license under French law and abiding by the rules of distribution of free software. You can use, modify and/or redistribute the software under the terms of the CeCILL-C license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy, modify and redistribute granted by the license, users are provided only with a limited warranty and the software's author, the holder of the economic rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, that may mean that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured and, more generally, to use and operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had knowledge of the CeCILL-C license and that you accept its terms.
 */

package net.elcado.jCF.core.lang.instance;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import net.elcado.jCF.core.lang.component.Component;
import net.elcado.jCF.core.lang.instance.events.EventsSink;
import net.elcado.jCF.core.lang.instance.events.EventsSource;
import net.elcado.jCF.core.lang.instance.services.ImplementedService;
import net.elcado.jCF.core.lang.instance.services.UsedService;

public class Instance implements Serializable {
    protected boolean remote;

    protected final Component component;

    protected final InstanceId instanceId;

    protected transient InstanceState currentInstanceState;

    protected final transient Object object;

    protected final List<ImplementedService> implementedServices = new ArrayList<>();

    protected final transient List<UsedService> usedServices = new ArrayList<>();

    protected final transient List<EventsSource> eventsSources = new ArrayList<>();

    protected final transient List<EventsSink> eventsSinks = new ArrayList<>();

    public Instance(final Component component, final Object object) {
//begin of modifiable zone(JavaSuper).....C/33c15886-335c-4bd3-a417-36a01abd9b16

//end of modifiable zone(JavaSuper).......E/33c15886-335c-4bd3-a417-36a01abd9b16
//begin of modifiable zone................T/53ee5dc2-ad1e-4fa3-8487-daa47588b82c
        this.component = component;
        component.addInstance(this);
        
        this.remote = false; // by default, an instance is local, not remote
        this.instanceId = new InstanceId();
        this.currentInstanceState = InstanceState.IDLE;
        this.object = object;
//end of modifiable zone..................E/53ee5dc2-ad1e-4fa3-8487-daa47588b82c
    }

    public boolean isRemote() {
//begin of modifiable zone(JavaCode)......C/2cdd65b4-6811-430e-bcc5-e8633b7ceb1b

//end of modifiable zone(JavaCode)........E/2cdd65b4-6811-430e-bcc5-e8633b7ceb1b
//begin of modifiable zone................T/b21cbb89-1e05-4984-bdc1-a71f7437604e
        return this.remote;
//end of modifiable zone..................E/b21cbb89-1e05-4984-bdc1-a71f7437604e
    }

    public void setRemote(final boolean value) {
//begin of modifiable zone................T/6ef1f39f-f72b-4ec0-b070-2f015d352f12
        this.remote = value;
//end of modifiable zone..................E/6ef1f39f-f72b-4ec0-b070-2f015d352f12
    }

    public Component getComponent() {
//begin of modifiable zone(JavaCode)......C/15e75606-c50e-45ce-b4e4-9209daddf1d3

//end of modifiable zone(JavaCode)........E/15e75606-c50e-45ce-b4e4-9209daddf1d3
//begin of modifiable zone................T/38d4b957-6143-41fc-9d61-08c2b0864974
        return this.component;
//end of modifiable zone..................E/38d4b957-6143-41fc-9d61-08c2b0864974
    }

    public Object getObject() {
//begin of modifiable zone(JavaCode)......C/ccc9dbb9-fc12-4cda-a2a5-1c52b6310d50

//end of modifiable zone(JavaCode)........E/ccc9dbb9-fc12-4cda-a2a5-1c52b6310d50
//begin of modifiable zone................T/4984bed5-729b-4e47-be71-68fa7c47f380
        return this.object;
//end of modifiable zone..................E/4984bed5-729b-4e47-be71-68fa7c47f380
    }

    public InstanceId getInstanceId() {
//begin of modifiable zone(JavaCode)......C/9bf25e58-107a-4eaf-b5eb-21023868f8e5

//end of modifiable zone(JavaCode)........E/9bf25e58-107a-4eaf-b5eb-21023868f8e5
//begin of modifiable zone................T/2b3f4800-c31c-447e-b8b4-fa4b605c7b01
        return this.instanceId;
//end of modifiable zone..................E/2b3f4800-c31c-447e-b8b4-fa4b605c7b01
    }

    public List<ImplementedService> getImplementedServices() {
//begin of modifiable zone(JavaCode)......C/5bd8e26c-68fb-4ec2-8086-d7ce32b8709e

//end of modifiable zone(JavaCode)........E/5bd8e26c-68fb-4ec2-8086-d7ce32b8709e
//begin of modifiable zone................T/5f8df745-0439-4e5e-b821-db0cce60ef0a
        return this.implementedServices;
//end of modifiable zone..................E/5f8df745-0439-4e5e-b821-db0cce60ef0a
    }

    public void addImplementedService(final ImplementedService value) {
//begin of modifiable zone................T/f83a1a7b-8389-46b0-a488-d1e7531d2019
        this.implementedServices.add(value);
//end of modifiable zone..................E/f83a1a7b-8389-46b0-a488-d1e7531d2019
    }

    public List<UsedService> getUsedServices() {
//begin of modifiable zone(JavaCode)......C/1747cc02-8d77-4cf5-87a4-8f705f5aed5d

//end of modifiable zone(JavaCode)........E/1747cc02-8d77-4cf5-87a4-8f705f5aed5d
//begin of modifiable zone................T/309ee764-cbf1-4367-8bec-b4d4dad2f965
        return this.usedServices;
//end of modifiable zone..................E/309ee764-cbf1-4367-8bec-b4d4dad2f965
    }

    public void addUsedService(final UsedService value) {
//begin of modifiable zone................T/960d81d4-dcab-41b6-aa2f-4476b3689e20
        this.usedServices.add(value);
//end of modifiable zone..................E/960d81d4-dcab-41b6-aa2f-4476b3689e20
    }

    public List<EventsSource> getEventsSources() {
//begin of modifiable zone(JavaCode)......C/985c7b7a-b2c8-4b7a-a2e9-7250633bc3d2

//end of modifiable zone(JavaCode)........E/985c7b7a-b2c8-4b7a-a2e9-7250633bc3d2
//begin of modifiable zone................T/7d3740da-2ea1-4b99-a4dc-dcf121fc09e5
        return this.eventsSources;
//end of modifiable zone..................E/7d3740da-2ea1-4b99-a4dc-dcf121fc09e5
    }

    public void addEventsSource(final EventsSource value) {
//begin of modifiable zone................T/ea6bd61a-618a-4777-b518-06a86a5f8999
        this.eventsSources.add(value);
//end of modifiable zone..................E/ea6bd61a-618a-4777-b518-06a86a5f8999
    }

    public List<EventsSink> getEventsSinks() {
//begin of modifiable zone(JavaCode)......C/1e9ee092-4494-41a2-8ac1-393a11d9a538

//end of modifiable zone(JavaCode)........E/1e9ee092-4494-41a2-8ac1-393a11d9a538
//begin of modifiable zone................T/3a7972a0-faef-4da0-a480-338e2da4f361
        return this.eventsSinks;
//end of modifiable zone..................E/3a7972a0-faef-4da0-a480-338e2da4f361
    }

    public void addEventsSink(final EventsSink value) {
//begin of modifiable zone................T/486a8c28-6009-4a78-a941-954ad044d732
        this.eventsSinks.add(value);
//end of modifiable zone..................E/486a8c28-6009-4a78-a941-954ad044d732
    }

    public InstanceState getCurrentInstanceState() {
//begin of modifiable zone(JavaCode)......C/1bf5bc4c-0f67-4ace-a119-7a752d5cc82b

//end of modifiable zone(JavaCode)........E/1bf5bc4c-0f67-4ace-a119-7a752d5cc82b
//begin of modifiable zone................T/865f7dd1-24a6-4d58-87de-53dfa4b9963c
        return this.currentInstanceState;
//end of modifiable zone..................E/865f7dd1-24a6-4d58-87de-53dfa4b9963c
    }

    public void setCurrentInstanceState(final InstanceState value) {
//begin of modifiable zone................T/e2af5658-9c7e-4afc-b256-4167d8587754
        this.currentInstanceState = value;
//end of modifiable zone..................E/e2af5658-9c7e-4afc-b256-4167d8587754
    }

}
