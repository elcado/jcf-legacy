/*
 * Copyright (c). Frédéric Cadier (2014-2018)
 *
 * This software is a computer program whose purpose is to allow component programming in pure java.
 *
 * This software is governed by the CeCILL-C license under French law and abiding by the rules of distribution of free software. You can use, modify and/or redistribute the software under the terms of the CeCILL-C license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy, modify and redistribute granted by the license, users are provided only with a limited warranty and the software's author, the holder of the economic rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, that may mean that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured and, more generally, to use and operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had knowledge of the CeCILL-C license and that you accept its terms.
 */

package net.elcado.jCF.core.lang.instance.services;

import java.io.Serializable;
import java.util.Set;
import javax.lang.model.element.Modifier;
import net.elcado.jCF.core.lang.component.services.ServicePort;
import net.elcado.jCF.core.lang.instance.Instance;
import net.elcado.jCF.utils.reflection.ReflectionUtils;

public abstract class AbstractService implements Serializable {
    protected final transient Instance instance;

    protected final ServicePort port;

    private transient Object object;

    public AbstractService(final Instance instance, final ServicePort port) {
//begin of modifiable zone(JavaSuper).....C/cfb6627b-6527-4956-9f76-3ea28051d03f

//end of modifiable zone(JavaSuper).......E/cfb6627b-6527-4956-9f76-3ea28051d03f
//begin of modifiable zone................T/2cc09162-ce4d-46e1-b977-4574ceeb17b7
        this.instance = instance;
        this.port = port;
        
        this.object = ReflectionUtils.getReflectionUtils().getFieldValue(this.instance.getObject(), this.port.getName());
//end of modifiable zone..................E/2cc09162-ce4d-46e1-b977-4574ceeb17b7
    }

    public Class<?> getClazz() {
//begin of modifiable zone(JavaCode)......C/551c9c59-fb01-436e-8f9a-77dfa4552ff5

//end of modifiable zone(JavaCode)........E/551c9c59-fb01-436e-8f9a-77dfa4552ff5
//begin of modifiable zone................T/409b9823-f84e-4de5-b2b9-76ffc9ef3695
        return this.port.getClazz();
//end of modifiable zone..................E/409b9823-f84e-4de5-b2b9-76ffc9ef3695
    }

    public Set<Modifier> getModifiers() {
//begin of modifiable zone(JavaCode)......C/27a1991e-4188-4207-9dda-eadff535c542

//end of modifiable zone(JavaCode)........E/27a1991e-4188-4207-9dda-eadff535c542
//begin of modifiable zone................T/0ddfb6bd-b9af-4a27-9d92-5813ed648e1e
        return this.port.getModifiers();
//end of modifiable zone..................E/0ddfb6bd-b9af-4a27-9d92-5813ed648e1e
    }

    public Object getObject() {
//begin of modifiable zone(JavaCode)......C/ddefc6c3-99fd-4b66-8a7a-8566142509ba

//end of modifiable zone(JavaCode)........E/ddefc6c3-99fd-4b66-8a7a-8566142509ba
//begin of modifiable zone................T/39467425-0258-4048-b0c1-cb841f8bb132
        return this.object;
//end of modifiable zone..................E/39467425-0258-4048-b0c1-cb841f8bb132
    }

    public void setObject(final Object value) {
//begin of modifiable zone................T/be8c43fd-5a80-4a1f-8de0-f196478ec2d8
        this.object = value;
        
        // instance can be null when AbstractService is deserialized by JGroups for RPC
        if (this.instance != null) {
            ReflectionUtils.getReflectionUtils().setFieldValue(this.instance.getObject(), this.port.getName(), this.object);
        }
//end of modifiable zone..................E/be8c43fd-5a80-4a1f-8de0-f196478ec2d8
    }

}
