/*
 * Copyright (c). Frédéric Cadier (2014-2018)
 *
 * This software is a computer program whose purpose is to allow component programming in pure java.
 *
 * This software is governed by the CeCILL-C license under French law and abiding by the rules of distribution of free software. You can use, modify and/or redistribute the software under the terms of the CeCILL-C license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy, modify and redistribute granted by the license, users are provided only with a limited warranty and the software's author, the holder of the economic rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, that may mean that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured and, more generally, to use and operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had knowledge of the CeCILL-C license and that you accept its terms.
 */

package net.elcado.jCF.core.lang.instance.events;

import java.util.concurrent.LinkedBlockingQueue;

public class SinkQueue<T> extends LinkedBlockingQueue<T> implements Sink<T> {
    private static final long serialVersionUID = -3260852582048069945L;

    protected final Sink<T> sink;

    protected Thread processingThread;

    public SinkQueue(final Sink<T> aSink) {
//begin of modifiable zone(JavaSuper).....C/64eaabc5-c5b1-44ef-8a9b-edfc252e6d7a

//end of modifiable zone(JavaSuper).......E/64eaabc5-c5b1-44ef-8a9b-edfc252e6d7a
//begin of modifiable zone................T/83b256bd-e7a1-484f-b9eb-b2d3d1ea5e78
        this.sink = aSink;
//end of modifiable zone..................E/83b256bd-e7a1-484f-b9eb-b2d3d1ea5e78
    }

    /**
     * Implements this to process event.
     * @param event The received event to be processed.
     */
    public void processEvent(final T event) {
//begin of modifiable zone................T/279b2d4e-42ae-42d9-bc64-e5dfa1730025
        this.sink.processEvent(event);
//end of modifiable zone..................E/279b2d4e-42ae-42d9-bc64-e5dfa1730025
    }

    public void startProcessingEvents() {
//begin of modifiable zone................T/40eb93d4-9d9e-4bb9-b0b3-55a43685078c
        // TODO replace with executor
        this.processingThread = new Thread(() -> {
            try {
                while (true) {
                    // wait for a event
                    T event = this.take();
        
                    // handle event
                    if (event != null) {
                        this.processEvent(event);
                    }
                }
            } catch (InterruptedException e) { /* NOP */ }
        });
        
        this.processingThread.start();
//end of modifiable zone..................E/40eb93d4-9d9e-4bb9-b0b3-55a43685078c
    }

    public void stopProcessingEvents() {
//begin of modifiable zone................T/37956faa-d430-4248-be34-7d2306834280
        this.processingThread.interrupt();
//end of modifiable zone..................E/37956faa-d430-4248-be34-7d2306834280
    }

}
