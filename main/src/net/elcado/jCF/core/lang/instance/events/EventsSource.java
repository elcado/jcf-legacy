/*
 * Copyright (c). Frédéric Cadier (2014-2018)
 *
 * This software is a computer program whose purpose is to allow component programming in pure java.
 *
 * This software is governed by the CeCILL-C license under French law and abiding by the rules of distribution of free software. You can use, modify and/or redistribute the software under the terms of the CeCILL-C license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy, modify and redistribute granted by the license, users are provided only with a limited warranty and the software's author, the holder of the economic rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, that may mean that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured and, more generally, to use and operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had knowledge of the CeCILL-C license and that you accept its terms.
 */

package net.elcado.jCF.core.lang.instance.events;

import java.util.ArrayList;
import java.util.List;
import net.elcado.jCF.core.lang.component.events.EventsPort;
import net.elcado.jCF.core.lang.instance.Instance;

public class EventsSource extends AbstractEvent {
    protected final List<EventsSink> eventsSinks = new ArrayList<>();

    public EventsSource(final Instance instance, final EventsPort port) {
//begin of modifiable zone(JavaSuper).....C/e3daf9d3-44f5-4f94-a509-35ac19504a25

//end of modifiable zone(JavaSuper).......E/e3daf9d3-44f5-4f94-a509-35ac19504a25
//begin of modifiable zone................T/a4194c84-7ae4-44a7-ae00-a1f8bc5643fd
        super(instance, port);
//end of modifiable zone..................E/a4194c84-7ae4-44a7-ae00-a1f8bc5643fd
    }

    public List<EventsSink> getEventsSinks() {
//begin of modifiable zone(JavaCode)......C/abf20388-ed49-4102-a03d-8901a703522c

//end of modifiable zone(JavaCode)........E/abf20388-ed49-4102-a03d-8901a703522c
//begin of modifiable zone................T/041bf0c4-6ff6-42ff-b816-b522b1f6a6fb
        return this.eventsSinks;
//end of modifiable zone..................E/041bf0c4-6ff6-42ff-b816-b522b1f6a6fb
    }

    public void addEventsSink(final EventsSink value) {
//begin of modifiable zone................T/7de8cf19-b5cf-4da1-93a0-841a087e8fb5
        this.eventsSinks.add(value);
//end of modifiable zone..................E/7de8cf19-b5cf-4da1-93a0-841a087e8fb5
    }

    public void removeEventsSink(final EventsSink value) {
//begin of modifiable zone................T/cd79dca3-e3eb-4627-92b4-9ae3054d4495
        this.eventsSinks.remove(value);
//end of modifiable zone..................E/cd79dca3-e3eb-4627-92b4-9ae3054d4495
    }

}
