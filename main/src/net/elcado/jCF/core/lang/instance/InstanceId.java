/*
 * Copyright (c). Frédéric Cadier (2014-2018)
 *
 * This software is a computer program whose purpose is to allow component programming in pure java.
 *
 * This software is governed by the CeCILL-C license under French law and abiding by the rules of distribution of free software. You can use, modify and/or redistribute the software under the terms of the CeCILL-C license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy, modify and redistribute granted by the license, users are provided only with a limited warranty and the software's author, the holder of the economic rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, that may mean that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured and, more generally, to use and operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had knowledge of the CeCILL-C license and that you accept its terms.
 */

package net.elcado.jCF.core.lang.instance;

import java.io.Serializable;
import org.jgroups.util.UUID;

/**
 * @author Frédéric Cadier
 */
public class InstanceId implements Serializable {
    private final String uuid;

    public InstanceId() {
//begin of modifiable zone(JavaSuper).....C/3ee15de0-cf03-409c-a108-3e2204210fa1

//end of modifiable zone(JavaSuper).......E/3ee15de0-cf03-409c-a108-3e2204210fa1
//begin of modifiable zone................T/ce8edbdc-a4a7-47a3-9d71-c29c9d3cf422
        this.uuid = UUID.randomUUID().toStringLong();
//end of modifiable zone..................E/ce8edbdc-a4a7-47a3-9d71-c29c9d3cf422
    }

    public boolean equals(Object obj) {
//begin of modifiable zone................T/22e68725-c6f7-4a3a-997e-61f3eae0c7f6
        if (!(obj instanceof InstanceId)) return false;
//end of modifiable zone..................E/22e68725-c6f7-4a3a-997e-61f3eae0c7f6
//begin of modifiable zone................T/1ae9ee56-a674-4b35-95f4-3984b5b9dadc
        return this.uuid.equals(((InstanceId) obj).uuid);
//end of modifiable zone..................E/1ae9ee56-a674-4b35-95f4-3984b5b9dadc
    }

    public int hashCode() {
//begin of modifiable zone(JavaCode)......C/64e78328-faff-4cb7-a404-9cd5f66e7e7c

//end of modifiable zone(JavaCode)........E/64e78328-faff-4cb7-a404-9cd5f66e7e7c
//begin of modifiable zone................T/1ebe9fe8-44ce-474b-be2b-7591086bb0b6
        return this.uuid.hashCode();
//end of modifiable zone..................E/1ebe9fe8-44ce-474b-be2b-7591086bb0b6
    }

    public @Override String toString() {
//begin of modifiable zone(JavaCode)......C/e4da0ab7-9cd3-4ed6-a212-dcfca6650f63

//end of modifiable zone(JavaCode)........E/e4da0ab7-9cd3-4ed6-a212-dcfca6650f63
//begin of modifiable zone................T/96428982-0068-41aa-8deb-8a89c8d7a8b0
        return uuid;
//end of modifiable zone..................E/96428982-0068-41aa-8deb-8a89c8d7a8b0
    }

}
