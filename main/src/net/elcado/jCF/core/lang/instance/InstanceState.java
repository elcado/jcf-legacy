/*
 * Copyright (c). Frédéric Cadier (2014-2018)
 *
 * This software is a computer program whose purpose is to allow component programming in pure java.
 *
 * This software is governed by the CeCILL-C license under French law and abiding by the rules of distribution of free software. You can use, modify and/or redistribute the software under the terms of the CeCILL-C license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy, modify and redistribute granted by the license, users are provided only with a limited warranty and the software's author, the holder of the economic rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, that may mean that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured and, more generally, to use and operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had knowledge of the CeCILL-C license and that you accept its terms.
 */

package net.elcado.jCF.core.lang.instance;

import net.elcado.jCF.core.exception.IllegalComponentStateTransitionException;
import net.elcado.jCF.core.lang.component.Failable;
import net.elcado.jCF.core.lang.component.Initializable;
import net.elcado.jCF.core.lang.component.Startable;

public abstract class InstanceState {
    public static final InstanceState IDLE = new InstanceState() {
		@Override public void init(Instance instance) { this.enterInit(instance); }
	};

    public static final InstanceState INITIALIZED = new InstanceState() {
		@Override public void start(Instance instance) { this.enterStart(instance); }
		@Override public void dispose(Instance instance) { this.enterDispose(instance); }
	};

    public static final InstanceState STARTED = new InstanceState() {
		@Override public void pause(Instance instance) { this.enterPause(instance); }
		@Override public void stop(Instance instance) { this.enterStop(instance); }
	};

    public static final InstanceState PAUSED = new InstanceState() {
		@Override public void resume(Instance instance) { this.enterResume(instance); }
		@Override public void stop(Instance instance) { this.enterStop(instance); }
	};

    public static final InstanceState RESUMED = new InstanceState() {
		@Override public void pause(Instance instance) { this.enterPause(instance); }
		@Override public void stop(Instance instance) { this.enterStop(instance); }
	};

    public static final InstanceState STOPPED = new InstanceState() {
		@Override public void start(Instance instance) { this.enterStart(instance); }
		@Override public void dispose(Instance instance) { this.enterDispose(instance); }
	};

    public static final InstanceState DISPOSED = new InstanceState() {};

    public static final InstanceState FAILED = new InstanceState() {
		@Override public void init(Instance instance) { this.enterInit(instance); }
		@Override public void dispose(Instance instance) { this.enterDispose(instance); }
	};

    public void init(Instance instance) throws IllegalComponentStateTransitionException {
//begin of modifiable zone................T/68d1b688-855d-41ac-898b-0824d71fb698
        throw new IllegalComponentStateTransitionException("Illegal transition from '" + this + "' to '" + InstanceState.INITIALIZED + "'");
//end of modifiable zone..................E/68d1b688-855d-41ac-898b-0824d71fb698
    }

    public void start(Instance instance) throws IllegalComponentStateTransitionException {
//begin of modifiable zone................T/db97f488-7c80-4ebb-8faa-62357d0b5e9a
        throw new IllegalComponentStateTransitionException("Illegal transition from '" + this + "' to '" + InstanceState.STARTED + "'");
//end of modifiable zone..................E/db97f488-7c80-4ebb-8faa-62357d0b5e9a
    }

    public void pause(Instance instance) throws IllegalComponentStateTransitionException {
//begin of modifiable zone................T/6be6c67c-88d0-42d8-b154-43febc6e5f8a
        throw new IllegalComponentStateTransitionException("Illegal transition from '" + this + "' to '" + InstanceState.PAUSED + "'");
//end of modifiable zone..................E/6be6c67c-88d0-42d8-b154-43febc6e5f8a
    }

    public void resume(Instance instance) throws IllegalComponentStateTransitionException {
//begin of modifiable zone................T/5539eaed-aa53-4686-b0d0-54f32c293727
        throw new IllegalComponentStateTransitionException("Illegal transition from '" + this + "' to '" + InstanceState.RESUMED + "'");
//end of modifiable zone..................E/5539eaed-aa53-4686-b0d0-54f32c293727
    }

    public void stop(Instance instance) throws IllegalComponentStateTransitionException {
//begin of modifiable zone................T/9461a068-6707-4d79-9ff9-9ce72f43bd33
        throw new IllegalComponentStateTransitionException("Illegal transition from '" + this + "' to '" + InstanceState.STOPPED + "'");
//end of modifiable zone..................E/9461a068-6707-4d79-9ff9-9ce72f43bd33
    }

    public void dispose(Instance instance) throws IllegalComponentStateTransitionException {
//begin of modifiable zone................T/3a083bf7-d56b-4752-ba3a-f399b03b99e3
        throw new IllegalComponentStateTransitionException("Illegal transition from '" + this + "' to '" + InstanceState.DISPOSED + "'");
//end of modifiable zone..................E/3a083bf7-d56b-4752-ba3a-f399b03b99e3
    }

    @Override
    public String toString() {
//begin of modifiable zone................T/d428c622-2233-43f6-a489-089894cb14cb
        String result = "UNKOWN";
        
        if (InstanceState.IDLE.equals(this)) { result = "IDLE"; }
        else if (InstanceState.INITIALIZED.equals(this)) { result = "INITIALIZED"; }
        else if (InstanceState.STARTED.equals(this)) { result = "STARTED"; }
        else if (InstanceState.PAUSED.equals(this)) { result = "PAUSED"; }
        else if (InstanceState.RESUMED.equals(this)) { result = "RESUMED"; }
        else if (InstanceState.STOPPED.equals(this)) { result = "STOPPED"; }
        else if (InstanceState.DISPOSED.equals(this)) { result = "DISPOSED"; }
//end of modifiable zone..................E/d428c622-2233-43f6-a489-089894cb14cb
//begin of modifiable zone................T/8823c053-d0ec-4477-a3ab-b44f60927902
        return result;
//end of modifiable zone..................E/8823c053-d0ec-4477-a3ab-b44f60927902
    }

    protected void enterInit(Instance instance) {
//begin of modifiable zone................T/5d783dee-8d24-4a69-b3d5-e4f8e5607f26
        // set state
        instance.setCurrentInstanceState(InstanceState.INITIALIZED);
        
        // invoke user listener
        Initializable initializableFacet = this.getInitializableFacet(instance);
        if (initializableFacet != null && !initializableFacet.onInitialized()) {
            this.enterFail(instance);
        }
//end of modifiable zone..................E/5d783dee-8d24-4a69-b3d5-e4f8e5607f26
    }

    protected void enterStart(Instance instance) {
//begin of modifiable zone................T/b2bf155c-b877-4661-944c-ee311fec7797
        // set state
        instance.setCurrentInstanceState(InstanceState.STARTED);
        
        // invoke user listener
        Startable startableFacet = this.getStartableFacet(instance);
        if (startableFacet != null && !startableFacet.onStarted()) {
            this.enterFail(instance);
        }
//end of modifiable zone..................E/b2bf155c-b877-4661-944c-ee311fec7797
    }

    protected void enterPause(Instance instance) {
//begin of modifiable zone................T/b4413ff7-f117-4833-afbe-7c994783ab97
        // set state
        instance.setCurrentInstanceState(InstanceState.PAUSED);
        
        // invoke user listener
        Startable startableFacet = this.getStartableFacet(instance);
        if (startableFacet != null && !startableFacet.onPaused()) {
            this.enterFail(instance);
        }
//end of modifiable zone..................E/b4413ff7-f117-4833-afbe-7c994783ab97
    }

    protected void enterResume(Instance instance) {
//begin of modifiable zone................T/0ac7df84-8f8f-461a-9c1e-bfe1149f19cc
        // set state
        instance.setCurrentInstanceState(InstanceState.RESUMED);
        
        // invoke user listener
        Startable startableFacet = this.getStartableFacet(instance);
        if (startableFacet != null && !startableFacet.onResumed()) {
            this.enterFail(instance);
        }
//end of modifiable zone..................E/0ac7df84-8f8f-461a-9c1e-bfe1149f19cc
    }

    protected void enterStop(Instance instance) {
//begin of modifiable zone................T/9726d1b8-6ece-429b-8705-ce183b25aa23
        // set state
        instance.setCurrentInstanceState(InstanceState.STOPPED);
        
        // invoke user listener
        Startable startableFacet = this.getStartableFacet(instance);
        if (startableFacet != null && !startableFacet.onStopped()) {
            this.enterFail(instance);
        }
//end of modifiable zone..................E/9726d1b8-6ece-429b-8705-ce183b25aa23
    }

    protected void enterDispose(Instance instance) {
//begin of modifiable zone................T/39344f7c-2754-488c-aecd-520e1e1521d0
        // set state
        instance.setCurrentInstanceState(InstanceState.DISPOSED);
        
        // invoke user listener
        Initializable initializableFacet = this.getInitializableFacet(instance);
        if (initializableFacet != null) {
            initializableFacet.onDisposed();
        }
//end of modifiable zone..................E/39344f7c-2754-488c-aecd-520e1e1521d0
    }

    protected void enterFail(Instance instance) {
//begin of modifiable zone................T/814ae2f2-0cb1-47e6-9df1-1588b3840fb3
        // set state
        instance.setCurrentInstanceState(InstanceState.FAILED);
        
        // invoke user listener
        Failable failableFacet = this.getFailableFacet(instance);
        if (failableFacet != null) {
            failableFacet.onFailed();
        }
//end of modifiable zone..................E/814ae2f2-0cb1-47e6-9df1-1588b3840fb3
    }

    private Failable getFailableFacet(final Instance anInstance) {
//begin of modifiable zone................T/6f4bfa4f-f1e1-4398-bdf2-3c561ae31ddc
        Failable failableFacet = null;
        
        if ((anInstance.getComponent().isInitializable() || anInstance.getComponent().isStartable())
                && anInstance.getObject() != null) {
            failableFacet = (Failable) anInstance.getObject();
        }
//end of modifiable zone..................E/6f4bfa4f-f1e1-4398-bdf2-3c561ae31ddc
//begin of modifiable zone................T/ca37136f-471f-402b-8e3f-69a25ecf662b
        return failableFacet;
//end of modifiable zone..................E/ca37136f-471f-402b-8e3f-69a25ecf662b
    }

    private Initializable getInitializableFacet(final Instance anInstance) {
//begin of modifiable zone................T/08d2b0d9-28e6-4ada-a4f2-8c300ae5c7b6
        Initializable initializableFacet = null;
        
        if (anInstance.getComponent().isInitializable() && anInstance.getObject() != null) {
            initializableFacet = (Initializable) anInstance.getObject();
        }
//end of modifiable zone..................E/08d2b0d9-28e6-4ada-a4f2-8c300ae5c7b6
//begin of modifiable zone................T/329127dd-634b-485d-a5a3-fed13060c28e
        return initializableFacet;
//end of modifiable zone..................E/329127dd-634b-485d-a5a3-fed13060c28e
    }

    private Startable getStartableFacet(final Instance anInstance) {
//begin of modifiable zone................T/50573974-8041-4292-b3da-4b995108a90f
        Startable startableFacet = null;
        
        if (anInstance.getComponent().isStartable() && anInstance.getObject() != null) {
            startableFacet = (Startable) anInstance.getObject();
        }
//end of modifiable zone..................E/50573974-8041-4292-b3da-4b995108a90f
//begin of modifiable zone................T/92c046b0-cc38-494c-a50c-4771b126e781
        return startableFacet;
//end of modifiable zone..................E/92c046b0-cc38-494c-a50c-4771b126e781
    }

}
