/*
 * Copyright (c). Frédéric Cadier (2014-2018)
 *
 * This software is a computer program whose purpose is to allow component programming in pure java.
 *
 * This software is governed by the CeCILL-C license under French law and abiding by the rules of distribution of free software. You can use, modify and/or redistribute the software under the terms of the CeCILL-C license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy, modify and redistribute granted by the license, users are provided only with a limited warranty and the software's author, the holder of the economic rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, that may mean that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured and, more generally, to use and operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had knowledge of the CeCILL-C license and that you accept its terms.
 */

package net.elcado.jCF.core.lang.instance.services;

import net.elcado.jCF.core.lang.component.services.ServicePort;
import net.elcado.jCF.core.lang.component.services.UsedServicePort;
import net.elcado.jCF.core.lang.instance.Instance;

public class UsedService extends AbstractService {
    public UsedService(final Instance instance, final ServicePort port) {
//begin of modifiable zone(JavaSuper).....C/820ff067-04bc-421d-a0ac-50bf51cdcf1b

//end of modifiable zone(JavaSuper).......E/820ff067-04bc-421d-a0ac-50bf51cdcf1b
//begin of modifiable zone................T/798dd2d6-0324-48e8-9607-a59407ff79b7
        super(instance, port);
//end of modifiable zone..................E/798dd2d6-0324-48e8-9607-a59407ff79b7
    }

    public boolean isCardinalityMany() {
//begin of modifiable zone(JavaCode)......C/cd327dae-deb0-4f75-b65b-92506a3f3cbc

//end of modifiable zone(JavaCode)........E/cd327dae-deb0-4f75-b65b-92506a3f3cbc
//begin of modifiable zone................T/6061ca40-7485-4fb1-8032-7251dc8f6439
        return ((UsedServicePort) this.port).isCardinalityMany();
//end of modifiable zone..................E/6061ca40-7485-4fb1-8032-7251dc8f6439
    }

}
