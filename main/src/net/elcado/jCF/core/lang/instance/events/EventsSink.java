/*
 * Copyright (c). Frédéric Cadier (2014-2018)
 *
 * This software is a computer program whose purpose is to allow component programming in pure java.
 *
 * This software is governed by the CeCILL-C license under French law and abiding by the rules of distribution of free software. You can use, modify and/or redistribute the software under the terms of the CeCILL-C license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy, modify and redistribute granted by the license, users are provided only with a limited warranty and the software's author, the holder of the economic rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, that may mean that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured and, more generally, to use and operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had knowledge of the CeCILL-C license and that you accept its terms.
 */

package net.elcado.jCF.core.lang.instance.events;

import java.util.ArrayList;
import java.util.List;
import net.elcado.jCF.core.lang.component.events.EventsPort;
import net.elcado.jCF.core.lang.instance.Instance;

public class EventsSink extends AbstractEvent {
    protected final List<EventsSource> eventsSources = new ArrayList<>();

    public EventsSink(final Instance instance, final EventsPort port) {
//begin of modifiable zone(JavaSuper).....C/70652914-b0aa-41ff-8618-2bffc1a2b359

//end of modifiable zone(JavaSuper).......E/70652914-b0aa-41ff-8618-2bffc1a2b359
//begin of modifiable zone................T/7b1b1410-1e4f-4360-9ff1-44b65a3ee403
        super(instance, port);
//end of modifiable zone..................E/7b1b1410-1e4f-4360-9ff1-44b65a3ee403
    }

    public List<EventsSource> getEventsSources() {
//begin of modifiable zone(JavaCode)......C/1c957c19-81bf-4de7-9cb4-0592f31de6a4

//end of modifiable zone(JavaCode)........E/1c957c19-81bf-4de7-9cb4-0592f31de6a4
//begin of modifiable zone................T/9d259639-300a-40a8-a96e-74ef29d2f6b6
        return this.eventsSources;
//end of modifiable zone..................E/9d259639-300a-40a8-a96e-74ef29d2f6b6
    }

    public void addEventsSource(final EventsSource value) {
//begin of modifiable zone................T/3bc2cbd4-fb9e-4dd9-ad57-7c6481e18ebe
        this.eventsSources.add(value);
//end of modifiable zone..................E/3bc2cbd4-fb9e-4dd9-ad57-7c6481e18ebe
    }

    public void removeEventsSource(final EventsSource value) {
//begin of modifiable zone................T/af4a0a19-f76c-4f63-a0a3-595ca4079d61
        this.eventsSources.remove(value);
//end of modifiable zone..................E/af4a0a19-f76c-4f63-a0a3-595ca4079d61
    }

}
