/*
 * Copyright (c). Frédéric Cadier (2014-2018)
 *
 * This software is a computer program whose purpose is to allow component programming in pure java.
 *
 * This software is governed by the CeCILL-C license under French law and abiding by the rules of distribution of free software. You can use, modify and/or redistribute the software under the terms of the CeCILL-C license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy, modify and redistribute granted by the license, users are provided only with a limited warranty and the software's author, the holder of the economic rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, that may mean that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured and, more generally, to use and operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had knowledge of the CeCILL-C license and that you accept its terms.
 */

package net.elcado.jCF.core.lang.instance.events;

import java.util.Set;
import javax.lang.model.element.Modifier;
import net.elcado.jCF.core.lang.component.events.EventsPort;
import net.elcado.jCF.core.lang.instance.Instance;
import net.elcado.jCF.utils.reflection.ReflectionUtils;

public abstract class AbstractEvent {
    protected final Instance instance;

    protected final EventsPort port;

    public AbstractEvent(final Instance instance, final EventsPort port) {
//begin of modifiable zone(JavaSuper).....C/739835d6-65b5-402b-84dc-01ec2680d14b

//end of modifiable zone(JavaSuper).......E/739835d6-65b5-402b-84dc-01ec2680d14b
//begin of modifiable zone................T/f62fb2d9-66d2-4d13-92df-9de907adcb99
        this.instance = instance;
        this.port = port;
//end of modifiable zone..................E/f62fb2d9-66d2-4d13-92df-9de907adcb99
    }

    public Class<?> getEventClazz() {
//begin of modifiable zone(JavaCode)......C/088c802c-36d3-46a1-b6d1-8c4d773f5a80

//end of modifiable zone(JavaCode)........E/088c802c-36d3-46a1-b6d1-8c4d773f5a80
//begin of modifiable zone................T/bf283782-143d-4897-87b6-1dedf0a9c12b
        return this.port.getClazz();
//end of modifiable zone..................E/bf283782-143d-4897-87b6-1dedf0a9c12b
    }

    public Set<Modifier> getModifiers() {
//begin of modifiable zone(JavaCode)......C/7760675d-67fc-44b5-ae1e-980e578f9076

//end of modifiable zone(JavaCode)........E/7760675d-67fc-44b5-ae1e-980e578f9076
//begin of modifiable zone................T/b463362e-3361-4464-b837-f42d7eb5594f
        return this.port.getModifiers();
//end of modifiable zone..................E/b463362e-3361-4464-b837-f42d7eb5594f
    }

    public Object getObject() {
//begin of modifiable zone................T/6b0f20c4-74b0-409a-8bc3-552cd0da191d
        Object object = ReflectionUtils.getReflectionUtils().getFieldValue(this.instance.getObject(), this.port.getName());
//end of modifiable zone..................E/6b0f20c4-74b0-409a-8bc3-552cd0da191d
//begin of modifiable zone................T/f83eb50e-d2f6-479d-b17d-823d90923f52
        return object;
//end of modifiable zone..................E/f83eb50e-d2f6-479d-b17d-823d90923f52
    }

    public void setObject(final Object value) {
//begin of modifiable zone................T/8e25d8f0-ad46-493f-9d6d-e8fabd83d22d
        ReflectionUtils.getReflectionUtils().setFieldValue(this.instance.getObject(), this.port.getName(), value);
//end of modifiable zone..................E/8e25d8f0-ad46-493f-9d6d-e8fabd83d22d
    }

}
