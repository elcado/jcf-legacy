/*
 * Copyright (c). Frédéric Cadier (2014-2018)
 *
 * This software is a computer program whose purpose is to allow component programming in pure java.
 *
 * This software is governed by the CeCILL-C license under French law and abiding by the rules of distribution of free software. You can use, modify and/or redistribute the software under the terms of the CeCILL-C license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy, modify and redistribute granted by the license, users are provided only with a limited warranty and the software's author, the holder of the economic rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, that may mean that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured and, more generally, to use and operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had knowledge of the CeCILL-C license and that you accept its terms.
 */

package net.elcado.jCF.log;

import java.util.logging.Level;
import net.elcado.jCF.core.annotations.Component;
import net.elcado.jCF.core.annotations.ImplementedService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * slf4j implementation of {@link LoggingServices}.
 * 
 * @author Frédéric Cadier
 */
@Component
public class SLF4jLogging implements LoggingServices, LoggingParameters {
    @ImplementedService
    public final transient LoggingServices loggingServices = this;

    private final Logger slf4jLogger = LoggerFactory.getLogger(LoggingServices.class);

    private java.util.logging.Logger loggerInstance;

    public SLF4jLogging() {
//begin of modifiable zone(JavaSuper).....C/3b5d8a8a-4c6e-4544-a10b-4308b5d4ebe2

//end of modifiable zone(JavaSuper).......E/3b5d8a8a-4c6e-4544-a10b-4308b5d4ebe2
//begin of modifiable zone................T/bee4df14-af13-4aed-8a88-de0ea2f93f14
        //        this.loggerInstance = LogManager.getLogManager().getLogger(this.slf4jLogger.getName());
        //
        //        this.setLoggingLevel(Level.ALL);
        //
        //        ConsoleHandler loggerHandler = new ConsoleHandler();
        //        loggerHandler.setLevel(Level.ALL);
        //
        //        this.loggerInstance.addHandler(loggerHandler);
//end of modifiable zone..................E/bee4df14-af13-4aed-8a88-de0ea2f93f14
    }

    public Level getLoggingLevel() {
//begin of modifiable zone(JavaCode)......C/6f73419d-042d-4c9a-8cb1-b4896e7c2a8c

//end of modifiable zone(JavaCode)........E/6f73419d-042d-4c9a-8cb1-b4896e7c2a8c
//begin of modifiable zone................T/699a79a5-fa6e-422a-ac8e-4dd6f4759481
        return this.loggerInstance.getLevel();
//end of modifiable zone..................E/699a79a5-fa6e-422a-ac8e-4dd6f4759481
    }

    public void setLoggingLevel(final Level logginLevel) {
//begin of modifiable zone................T/82ea464a-fe41-4c8f-ad1f-32c1a96c75fb
        this.loggerInstance.setLevel(logginLevel);
//end of modifiable zone..................E/82ea464a-fe41-4c8f-ad1f-32c1a96c75fb
    }

    /**
     * ERROR logging level.
     * @param message the message to log.
     */
    public void error(String message) {
//begin of modifiable zone................T/1f889f48-5238-4f84-9801-ba656dc01dfa
        if (this.slf4jLogger.isErrorEnabled()) {
            this.slf4jLogger.error(this.getHeader() + "\n" + message + "\n");
        }
//end of modifiable zone..................E/1f889f48-5238-4f84-9801-ba656dc01dfa
    }

    /**
     * INFO logging level.
     * @param message the message to log.
     */
    public void warn(String message) {
//begin of modifiable zone................T/be3b2e33-3b26-43a4-8f32-38d3db0b1e1d
        if (this.slf4jLogger.isErrorEnabled()) {
            this.slf4jLogger.warn(this.getHeader() + "\n" + message + "\n");
        }
//end of modifiable zone..................E/be3b2e33-3b26-43a4-8f32-38d3db0b1e1d
    }

    /**
     * INFO logging level.
     * @param message the message to log.
     */
    public void info(String message) {
//begin of modifiable zone................T/f049b4b7-6166-4487-bccf-2fc71a14b28c
        if (this.slf4jLogger.isInfoEnabled()) {
            this.slf4jLogger.info(this.getHeader() + "\n" + message + "\n");
        }
//end of modifiable zone..................E/f049b4b7-6166-4487-bccf-2fc71a14b28c
    }

    /**
     * DEBUG logging level.
     * @param message the message to log.
     */
    public void debug(String message) {
//begin of modifiable zone................T/a0f452a0-024b-4ee3-90b3-c550f7388fd1
        if (this.slf4jLogger.isDebugEnabled()) {
            this.slf4jLogger.debug(this.getHeader() + "\n" + message + "\n");
        }
//end of modifiable zone..................E/a0f452a0-024b-4ee3-90b3-c550f7388fd1
    }

    /**
     * INFO logging level.
     * @param message the message to log.
     */
    public void trace(String message) {
//begin of modifiable zone................T/1cfcd8f7-3c10-4b71-a3ba-a15c06fff6fe
        if (this.slf4jLogger.isTraceEnabled()) {
            this.slf4jLogger.trace(this.getHeader() + "\n" + message + "\n");
        }
//end of modifiable zone..................E/1cfcd8f7-3c10-4b71-a3ba-a15c06fff6fe
    }

    private String getHeader() {
//begin of modifiable zone................T/fbe5b774-04da-422c-af16-ec8aa815f7e8
        String header = "[" + String.valueOf(System.currentTimeMillis()) + "] "
                + " " + Thread.currentThread().toString()
                + " " + Thread.currentThread().getStackTrace()[3].toString();
//end of modifiable zone..................E/fbe5b774-04da-422c-af16-ec8aa815f7e8
//begin of modifiable zone................T/bdb26f72-84ae-4c2c-af1b-011ee107d7c3
        return header;
//end of modifiable zone..................E/bdb26f72-84ae-4c2c-af1b-011ee107d7c3
    }

}
