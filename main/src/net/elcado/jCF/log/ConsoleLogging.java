/*
 * Copyright (c). Frédéric Cadier (2014-2018)
 *
 * This software is a computer program whose purpose is to allow component programming in pure java.
 *
 * This software is governed by the CeCILL-C license under French law and abiding by the rules of distribution of free software. You can use, modify and/or redistribute the software under the terms of the CeCILL-C license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy, modify and redistribute granted by the license, users are provided only with a limited warranty and the software's author, the holder of the economic rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, that may mean that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured and, more generally, to use and operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had knowledge of the CeCILL-C license and that you accept its terms.
 */

package net.elcado.jCF.log;

import java.util.logging.Level;
import net.elcado.jCF.core.annotations.Component;
import net.elcado.jCF.core.annotations.ImplementedService;

/**
 * Simple console implementation of {@link LoggingServices}.
 * 
 * @author Frédéric Cadier
 */
@Component
public class ConsoleLogging implements LoggingServices, LoggingParameters {
    @ImplementedService
    public final transient LoggingServices loggingServices = this;

    private Level loggingLevel;

    public ConsoleLogging() {
//begin of modifiable zone(JavaSuper).....C/3d86146c-2a4b-42f1-a476-95fee515a559

//end of modifiable zone(JavaSuper).......E/3d86146c-2a4b-42f1-a476-95fee515a559
//begin of modifiable zone................T/6e5dfa68-9d82-4f70-be5c-d4c3449109de
        this.setLoggingLevel(Level.ALL);
//end of modifiable zone..................E/6e5dfa68-9d82-4f70-be5c-d4c3449109de
    }

    public Level getLoggingLevel() {
//begin of modifiable zone(JavaCode)......C/688befea-2a82-42f5-8d68-a07b990c37e4

//end of modifiable zone(JavaCode)........E/688befea-2a82-42f5-8d68-a07b990c37e4
//begin of modifiable zone................T/272ce358-75cb-48e0-8822-03d4cd76973a
        return this.loggingLevel;
//end of modifiable zone..................E/272ce358-75cb-48e0-8822-03d4cd76973a
    }

    public void setLoggingLevel(final Level logginLevel) {
//begin of modifiable zone................T/7f62dd78-f295-4ffe-9988-6dd2dbd8b5d5
        this.loggingLevel = logginLevel;
//end of modifiable zone..................E/7f62dd78-f295-4ffe-9988-6dd2dbd8b5d5
    }

    /**
     * ERROR logging level.
     * @param message the message to log.
     */
    public void error(String message) {
//begin of modifiable zone................T/e8119515-8204-4044-bf80-38898360283b
        if (this.getLoggingLevel().intValue() <= Level.SEVERE.intValue()) {
            System.err.println("[ERROR] " + this.getHeader() + "\n" + message);
        }
//end of modifiable zone..................E/e8119515-8204-4044-bf80-38898360283b
    }

    /**
     * INFO logging level.
     * @param message the message to log.
     */
    public void warn(String message) {
//begin of modifiable zone................T/0997c817-875a-4b0c-8e41-4dbd97902f3e
        if (this.getLoggingLevel().intValue() <= Level.WARNING.intValue()) {
            System.err.println("[WARNING] " + this.getHeader() + "\n" + message);
        }
//end of modifiable zone..................E/0997c817-875a-4b0c-8e41-4dbd97902f3e
    }

    /**
     * INFO logging level.
     * @param message the message to log.
     */
    public void info(String message) {
//begin of modifiable zone................T/5d776cbc-6a0f-4495-a882-1ad199b6dd7b
        if (this.getLoggingLevel().intValue() <= Level.INFO.intValue()) {
            System.out.println("[INFO] " + this.getHeader() + "\n" + message);
        }
//end of modifiable zone..................E/5d776cbc-6a0f-4495-a882-1ad199b6dd7b
    }

    /**
     * DEBUG logging level.
     * @param message the message to log.
     */
    public void debug(String message) {
//begin of modifiable zone................T/99f22da1-005d-49de-a5a5-fb28d97ee131
        if (this.getLoggingLevel().intValue() <= Level.FINER.intValue()) {
            System.out.println("[DEBUG] " + this.getHeader() + "\n" + message);
        }
//end of modifiable zone..................E/99f22da1-005d-49de-a5a5-fb28d97ee131
    }

    /**
     * INFO logging level.
     * @param message the message to log.
     */
    public void trace(String message) {
//begin of modifiable zone................T/7c49a462-6876-4c8f-b9b2-460e08023cc2
        if (this.getLoggingLevel().intValue() <= Level.FINEST.intValue()) {
            System.out.println("[TRACE] " + this.getHeader() + "\n" + message);
        }
//end of modifiable zone..................E/7c49a462-6876-4c8f-b9b2-460e08023cc2
    }

    private String getHeader() {
//begin of modifiable zone................T/f4113429-7bbf-4f32-b0e9-1b4c933afb4c
        String header = "[" + String.valueOf(System.currentTimeMillis()) + "] "
                + " " + Thread.currentThread().toString()
                + " " + Thread.currentThread().getStackTrace()[3].toString();
//end of modifiable zone..................E/f4113429-7bbf-4f32-b0e9-1b4c933afb4c
//begin of modifiable zone................T/46321e95-ed94-453c-9285-7e8fd2fa1789
        return header;
//end of modifiable zone..................E/46321e95-ed94-453c-9285-7e8fd2fa1789
    }

}
