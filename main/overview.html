<!--
  ~ Copyright (c). Frédéric Cadier (2014-2017)
  ~
  ~ This software is a computer program whose purpose is to allow component programming in pure java.
  ~
  ~ This software is governed by the CeCILL-C license under French law and abiding by the rules of distribution of free software. You can use, modify and/or redistribute the software under the terms of the CeCILL-C license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
  ~
  ~ As a counterpart to the access to the source code and rights to copy, modify and redistribute granted by the license, users are provided only with a limited warranty and the software's author, the holder of the economic rights, and the successive licensors have only limited liability.
  ~
  ~ In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, that may mean that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured and, more generally, to use and operate it in the same conditions as regards security.
  ~
  ~ The fact that you are presently reading this means that you have had knowledge of the CeCILL-C license and that you accept its terms.
  -->

<body>
	The jCF library is, namely, a
	<em>java Component Framework</em>. It allows to develop components
	<sup><a href="#beyong-component">[1]</a></sup> in pure java.
	<h1>jCF component model</h1>
	<div align="center">
		<object width="60%" data="jCF-component.svg" type="image/svg+xml"></object>
	</div>
	<p>
		A jCF component is a java class that more or less follows the <a
			href="https://en.wikipedia.org/wiki/Corba#CORBA_Component_Model_.28CCM.29"
			target="_blank">Corba Component Model (CCM)</a> though different
		annotations on its attributes (see the illustration above).
	<p>Some trivia about a jCF component:
	<ul>
		<li>it provides and uses services;</li>
		<li>it posts and processes events;</li>
		<li>it can be instantiated many times;</li>
		<li>each instance can be independently parameterized.</li>
	</ul>

	<h2>Component and instance</h2>

	jCF framework distinguishes component from their instances.
	<p>
		A component is first to be registered by calling <a
			href="net/elcado/jCF/core/container/ComponentsServices.html#registerComponent-java.lang.Class-">
			container.registerComponent(Class<?> componentType)</a> on a container instance.
	<p>
	Once registered, a component can be instantiated by calling
	<a href="net/elcado/jCF/core/container/InstancesServices.html#deployInstance-net.elcado.jCF.core.lang.instance.InstanceId-java.lang.Class-">
		container.deployInstance(InstanceId parentId, Class<?> componentType)
	</a>
	with a parent's id (see below) and a registered component type. This
		method returns the unique <a
			href="net/elcado/jCF/core/lang/instance/InstanceId.html">InstanceId</a>
		of the new instance which is located depending on the provided
		parent's id:
	<dl>
		<dt>
			<b>parent's id is null</b>
		</dt>
		<dd>
			the built instance is considered as a <em>root</em> of a tree of
			instances. There can be multiple trees of instances in a single
			application. This is the particular case of the jCF container;
		</dd>
		<dt>
			<b>parent's id is <em>not</em> null
			</b>
		</dt>
		<dd>the built instance is located as a child of the given
			instance.</dd>
	</dl>
	All children of a given parent are called
	<em>neighbors</em>, as well as all roots are considered neighbors.
	<p>
		Each instance can be informed of its own <a
			href="net/elcado/jCF/core/lang/instance/InstanceId.html">InstanceId</a>
		by declaring a specific <em>null</em> class attribute annotated with <a
			href="net/elcado/jCF/core/annotations/Id.html">@Id</a> and typed with
		<a href="net/elcado/jCF/core/lang/instance/InstanceId.html">InstanceId</a>.
		After instantiation of the component, this attribute will be injected
		<sup><a href="#beyong-injection">[2]</a></sup> with the id of the
		instance.
	<p>
		Children can also be automatically instantiated and located under a
		given parent if this one declares some composed components in its <a
			href="net/elcado/jCF/core/annotations/Component.html">@Component</a>
		annotation.
	<p>As we'll see below, the tree of instances drastically affects
		the wiring algorithm responsible for the resolution of instances'
		dependencies.
	<p>This tree mechanism allows a very reusable scheme where a
		component (and its children) can be instantiated many times, at
		different points of different trees without any conflict in the
		services injection or events weaving process.
	<h2>Services</h2>

	A service is either provided to, or required from, other instances,
	either to expose some internal functionalities or to benefit from
	others' ones.
	<p>A service is defined by a java interface which describes the
		contract that will be shared between components.
	<dl>
		<dt>
			<b>Implemented service</b>
		</dt>
		<dd>
			A service implementation is an <em>initialized</em> class attribute
			typed with the service's interface and annotated with <a
				href="net/elcado/jCF/core/annotations/ImplementedService.html">@ImplementedService</a>.
		</dd>
		<dt>
			<b>Used service</b>
		</dt>
		<dd>
			A service use is defined by an <em>null</em> class attribute typed
			with the service's interface and annotated with <a
				href="net/elcado/jCF/core/annotations/UsedService.html">@UsedService</a>.
		</dd>
	</dl>
	The jCF framework is able to provide a service's implementation to any
	"user" instance that requires it by injecting the provider's
	implemented attribute into the
	<em>null</em> attribute of the user. The lookup algorithm that returns
	found implementations uses the modifier of the declared implementation:
	<dl>
		<dt>
			<b>a PUBLIC implementation of a root instance</b>
		</dt>
		<dd>can be provided to any other instance, whatever its location
			in any tree;</dd>
		<dt>
			<b>a PUBLIC implementation of a child instance</b>
		</dt>
		<dd>can be provided to a parent, a child and a neighbor;</dd>
		<dt>
			<b>a PROTECTED implementation</b>
		</dt>
		<dd>can be provided to a child and a neighbor;</dd>
		<dt>
			<b>a PRIVATE implementation</b>
		</dt>
		<dd>can only be provided to a child.</dd>
	</dl>
	If a
	<a href="net/elcado/jCF/core/annotations/UsedService.html">@UsedService</a>
	service can not be satisfied, the framework will raise a warning. On
	the other side, if many implementations are found for a used service,
	the injection will raise an error.
	<h3>1-to-many service injection</h3>
	A used service attribute can be declared as a map which key is typed as
	<a href="net/elcado/jCF/core/lang/instance/InstanceId.html">InstanceId</a>.
	It will then be injected with the map (might be empty) of all the found
	implementations toward the id of the instance that provides it.

	<h2>Events</h2>

	In the jCF framework, an event is defined as the exchange of a message
	between two instances: one is the source that fires the event, the
	other is the sink that will
	<em>asynchronously</em> process the event.
	<dl>
		<dt>
			<b>Event source</b>
		</dt>
		<dd>
			An event source is an <em>null</em> class attribute typed with the
			generic <a
				href="net/elcado/jCF/core/lang/instance/events/Source.html">Source</a>
			interface specialized on the data type to be exchanged and annotated
			with <a href="net/elcado/jCF/core/annotations/EventsSource.html">@EventsSource</a>.
		</dd>
		<dt>
			<b>Event sink</b>
		</dt>
		<dd>
			An event sink is an <em>initialized</em> class attribute typed with
			the generic <a
				href="net/elcado/jCF/core/lang/instance/events/Sink.html">Sink</a>
			interface specialized on the data type to be exchanged and annotated
			with <a href="net/elcado/jCF/core/annotations/EventsSink.html">@EventsSink</a>.
		</dd>
	</dl>
	The lookup algorithm that returns found sources uses the modifier of
	the declared source:
	<dl>
		<dt>
			<b>a PUBLIC source of a root instance</b>
		</dt>
		<dd>can be provided to any other instance, whatever its location
			in any tree;</dd>
		<dt>
			<b>a PUBLIC source of a child instance</b>
		</dt>
		<dd>can be provided to a parent, a child and a neighbor;</dd>
		<dt>
			<b>a PROTECTED source</b>
		</dt>
		<dd>can be provided to a child and a neighbor;</dd>
		<dt>
			<b>a PRIVATE source</b>
		</dt>
		<dd>can only be provided to a child.</dd>
	</dl>
	A source don't know how many sinks will handle its events, and the
	handling of events is asynchronous on the sink side. On the other side,
	the sinks don't know who sent the event it receives, unless the
	received message holds the information.

	<h2>Parameters</h2>

	Each instance can be parameterized, or parameterize others, if it
	declares a specific used service with the
	<a href="net/elcado/jCF/core/annotations/ParametersService.html">@ParametersService</a>
	annotation. The annotated class attribute is to be typed with generic
	<a href="net/elcado/jCF/core/lang/instance/parameters/Parameters.html">Parameters</a>
	specialized with the type of parameters the component wants to be aware
	of.
	<p>
		This <a
			href="net/elcado/jCF/core/lang/instance/parameters/Parameters.html">Parameters</a>
		interface offers means to set and get global parameters or to set and
		get parameters for a specific instance identified with its <a
			href="net/elcado/jCF/core/lang/instance/InstanceId.html">InstanceId</a>.
	<h1>Instance's life cycle</h1>

	An instance can follow and trigger actions on its life cycle by
	implementing the
	<a href="net/elcado/jCF/core/lang/component/ComponentState.html">ComponentState</a>
	interface:
	<dl>
		<dt>
			<b>componentStart()</b>
		</dt>
		<dd>
			is called after a call to
			<a href="net/elcado/jCF/core/container/InstancesServices.html#startInstances--">container.startInstances()</a>
			or a call to <a href="net/elcado/jCF/core/container/InstancesServices.html#startInstance-net.elcado.jCF.core.lang.instance.InstanceId-">container.startInstance(InstanceId instanceId)</a>.
			At this point of the life cycle, all dependencies that can be resolved have been resolved;
		</dd>
		<dt>
			<b>componentPause()</b>
		</dt>
		<dd>
			is called after a call to
			<a href="net/elcado/jCF/core/container/InstancesServices.html#pauseInstances--">container.pauseInstances()</a>
			or a call to <a href="net/elcado/jCF/core/container/InstancesServices.html#pauseInstance-net.elcado.jCF.core.lang.instance.InstanceId-">container.pauseInstance(InstanceId instanceId)</a>;
		</dd>
		<dt>
			<b>componentResume()</b>
		</dt>
		<dd>
			is called after a call to
			<a href="net/elcado/jCF/core/container/InstancesServices.html#resumeInstances--">container.resumeInstances()</a>
			or a call to <a href="net/elcado/jCF/core/container/InstancesServices.html#resumeInstance-net.elcado.jCF.core.lang.instance.InstanceId-">container.resumeInstance(InstanceId instanceId)</a>;
		</dd>
		<dt>
			<b>componentStop()</b>
		</dt>
		<dd>
			is called after a call to
			<a href="net/elcado/jCF/core/container/InstancesServices.html#stopInstances--">container.stopInstances()</a>
			or a call to <a href="net/elcado/jCF/core/container/InstancesServices.html#stopInstance-net.elcado.jCF.core.lang.instance.InstanceId-">container.stopInstance(InstanceId instanceId)</a>.
		</dd>
	</dl>

	<h1>Using jCF</h1>

	First of all, a jCF
	<a href="net/elcado/jCF/core/container/Container.html">Container</a> is
	to be initialized through its static
	<a href="net/elcado/jCF/core/container/Container.html#initialize--">initialize</a>
	method which can be passed an existing object instance that will be
	injected and wired as an instance of component:
	<pre>
	&#64;Id
	private final transient InstanceId thisId = null;

	&#64;UsedService
	private final transient ComponentsServices componentsServices = null;

	&#64;UsedService
	private final transient InstancesServices instancesServices = null;

	public void launch {
		Container.<i>initialize</i>(this);
		...
		this.componentsServices.registerComponent(componentType);
		...
		InstanceId anInstanceId = this.instancesServices.deployInstance(this.thisId, componentType);
		...
	}
	</pre>
	
	In the given example, the <a href="net/elcado/jCF/core/container/ComponentsServices.html">ComponentsServices</a>
	and <a href="net/elcado/jCF/core/container/InstancesServices.html">InstancesServices</a> can now be used to register
	components and deploy and start/stop them.

	<h1>License</h1>

	<p id="license">
		The whole project is developed under the <a
			href="http://www.cecill.info/index.en.html">CECILL-C</a> license.
	<hr>
	<p id="beyong-component">
		<sup>[1]</sup> The meaning of "component" is here to be understood in
		the sense described in this <a
			href="https://en.wikipedia.org/wiki/Component-based_software_engineering"
			target="_blank">wikipedia</a> page.
	<p id="#beyong-injection">
		<sup>[2]</sup> Everyone should, at this point, know what inversion of
		control and the dependency injection patterns are... if not, try <a
			href="http://martinfowler.com/bliki/InversionOfControl.html"
			target="_blank">this</a>.
</body>
